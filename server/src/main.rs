mod config;
mod server;
mod service;
mod network;
mod database;

use anyhow::Result;
use tracing::warn;

#[tokio::main]
async fn main() -> Result<()> {
    let filter = match tracing_subscriber::EnvFilter::try_from_env("DEBUG") {
        Ok(v)  => v.add_directive("sqlx::query=debug".parse()?)
            .add_directive("debug".parse()?),
        Err(_) => tracing_subscriber::EnvFilter::from_default_env().add_directive("sqlx::query=off".parse()?)
            .add_directive("nero_server=info".parse()?)
    };

    tracing_subscriber::fmt()
        .with_env_filter(filter)
        .with_thread_names(true)
        .init();

    let args: Vec<_> = std::env::args().collect();
    if args.len() != 2 {
        warn!("Run: nero-server <config-file>");
        return Err(anyhow::Error::msg("Missing config argument"));
    }

    let conf = tokio::fs::read(&args[1]).await
        .expect("Couldn't read config file");
    let conf = config::Config::new(
        std::str::from_utf8(&conf).expect("Config file malformated")
    ).unwrap();

    server::start(conf).await;

    Ok(())
}
