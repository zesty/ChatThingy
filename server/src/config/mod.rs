use std::{error::Error, io::ErrorKind};

use serde::Deserialize;

const DEFAULT_NETWORK_PORT: u16 = 7777;
const CONNECTION_LIMIT: usize   = 5000;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub network: NetworkConfig,
    #[serde(rename = "database")]
    pub db: DatabaseConfig,
    #[serde(default = "default_limits")]
    pub limits: LimitsConfig,
    pub misc: MiscConfig,
}

#[derive(Deserialize, Debug)]
pub struct NetworkConfig {
    #[serde(default = "default_network_port")]
    pub port: u16,
    #[serde(rename = "type")]
    pub ntype: String,
    pub keys: Vec<String>
}

#[derive(Deserialize, Debug)]
pub struct DatabaseConfig {
    pub urls: Vec<String>
}

#[derive(Deserialize, Debug)]
pub struct LimitsConfig {
    #[serde(default = "default_connection_limit")]
    pub connections: usize
}

#[derive(Deserialize, Debug)]
pub struct MiscConfig {
    pub tor_binary_path: String
}

fn default_network_port() -> u16 { DEFAULT_NETWORK_PORT }

fn default_limits() -> LimitsConfig { LimitsConfig { connections: default_connection_limit() } }
fn default_connection_limit() -> usize { CONNECTION_LIMIT }


impl Config {
    pub fn new(conf: &str) -> Result<Config, Box<dyn Error>> {
        let config: Config = match toml::from_str(conf) {
            Ok(v) => v,
            Err(e) => return Err(Box::new(std::io::Error::new(ErrorKind::Other, format!("Error parsing config file: {}", e.message()))))
        };
        Ok(config)
    }
}
