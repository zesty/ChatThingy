use anyhow::Result;

use common::{api, keys::PublicKey};
use rand::RngCore;
use tracing::error;
use borsh::{BorshSerialize, BorshDeserialize};

use crate::{database::user, server::ServerSettings};

pub fn challenge() -> Result<Vec<u8>> {
    let mut rand_bytes = [0_u8; 128]; 
    let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();

    match seed.try_fill_bytes(&mut rand_bytes) {
        Ok(_) => (),
        Err(e) => {
            error!("Cannot generate random bytes for challeng: {}", e.to_string());
            return Err(anyhow::Error::msg(e.to_string()));
        }
    }

    let challenge_msg = api::handshake::Init {
        version: api::VERSION,
        challenge: rand_bytes.to_vec()
    }.try_to_vec()?;

    Ok(challenge_msg)
}

pub struct SecondReturn {
    pub user: UserSettings,
    pub resp: Vec<u8>
}

pub struct UserSettings {
    pub pk: PublicKey,
    pub id: i32,
    pub permission: i32,
    pub join_height: i64
}

pub async fn verify_challenge_response(challenge: &[u8], msg_bin: &[u8], sett: &ServerSettings) -> Result<Result<SecondReturn, Vec<u8>>> {
    let mut user = None;
    let status = match api::handshake::Second::try_from_slice(msg_bin) {
        Ok(msg) => {
            if msg.version < api::MIN_SUPPORTED_VER {
                api::handshake::Status::OLD_VERSION
            } else {
                match msg.pk.verify_sig(challenge, &msg.sig) {
                    Ok(true) => {
                        // Check if user is in database
                        // Or we have new invited user
                        match user::get(&sett.db, &msg.pk.try_to_vec()?).await {
                            Ok(Some(v)) => {
                                if v.left_group {
                                    api::handshake::Status::NOT_MEMBER
                                } else {
                                    user = Some(UserSettings { pk: msg.pk, id: v.id, permission: v.permission, join_height: v.join_height });
                                    match (v.id, v.permission) {
                                        (1, -1) => api::handshake::Status::OK_NEW,
                                        _       => api::handshake::Status::OK
                                    }
                                }
                            },
                            Ok(None) => api::handshake::Status::NOT_MEMBER,
                            Err(e) => {
                                error!("Can't fetch user {} info from database: {}", msg.pk, e);
                                api::handshake::Status::Err
                            }
                        }
                    },
                    Ok(false) | Err(_) => api::handshake::Status::INVALID_SIG
                }
            }
        },
        Err(_) => api::handshake::Status::INVALID_MSG
    };

    let resp_bin = api::handshake::Third {
        status
    }.try_to_vec()?;

    match user {
        Some(user) => Ok(Ok(SecondReturn { user, resp: resp_bin })),
        None       => Ok(Err(resp_bin))
    }
}
