use anyhow::Result;
use borsh::{BorshSerialize, BorshDeserialize};
use common::{api::{self, message::{SendMessageResponse, MessagePayload}}, keys::PublicKey, hash::HashFn};
use tracing::error;

use crate::{server::ServerSettings, database::message};

use super::get_user_id;

pub async fn send(msg_bin: &mut [u8], sett: &ServerSettings, pk: &PublicKey) -> Result<Vec<u8>> {
    match send_inner(msg_bin, sett, pk).await {
        Ok(v)  => Ok(v.try_to_vec()?),
        Err(e) => Err(e)
    }
}

pub async fn send_inner(msg_bin: &mut [u8], sett: &ServerSettings, pk: &PublicKey) -> Result<SendMessageResponse> {
    let mut message = MessagePayload::try_from_slice(msg_bin)?;

    match &mut message {
        MessagePayload::GroupMessage(msg) => {
            msg.timestamp = chrono::offset::Utc::now().timestamp() as i64;
            // We are not really changing anything
            // So size should be identical
            message.serialize(&mut msg_bin.as_mut())?;

            let user_id = get_user_id(sett, pk).await;

            if let Err(e) = message::add_group(&sett.db, user_id, &msg_bin).await {
                error!("Can't save group message sent via {}: {}", pk, e);
                return Ok(SendMessageResponse { status: api::status::Status::ReqErr });
            }
        },
        MessagePayload::DirectMessage(msg) => {
            let dest = msg.destination.try_to_vec()?;

            if !msg.source.eq(pk) {
                // User is trying to send a message under a different identity
                return Ok(SendMessageResponse { status: api::status::Status::SigError });
            }

            // We now need to verify signature
            // We start by calculating hash value
            // of MSG + Destination
            let mut hash = HashFn::new(&msg.hash);
            hash.digest(&msg.message);
            hash.digest(&dest);
            hash.digest(&msg.timestamp.to_be_bytes());

            if !hash.compare() {
                // OOPS!! Something is wrong
                // how come our hash is different!!
                return Ok(SendMessageResponse { status: api::status::Status::SigError });
            }

            drop(hash);

            // Now we check signature
            match pk.verify_sig(msg.hash.get_inner_ref(), &msg.sig) {
                Ok(true)           => (),
                Ok(false) | Err(_) => {
                    return Ok(SendMessageResponse { status: api::status::Status::SigError });
                }
            }

            // Everything is good so far
            
            // We check if user exists and send message to them directly
            //send_directly(msg_bin, sett, pk).await;

            if let Err(e) = message::add(&sett.db, &dest, &msg_bin).await {
                error!("Can't save message sent to {} via {}: {}", msg.destination, msg.source, e);
                return Ok(SendMessageResponse { status: api::status::Status::ReqErr });
            }
        }
    }

    Ok(SendMessageResponse { status: api::status::Status::Ok })
} 

/*async fn send_directly(msg_bin: &[u8], sett: &ServerSettings, pk: &PublicKey) {
    let mut found = None;
    let lock      = sett.connected_users.read().await;
    if let Some(v) = lock.get(pk) {
        found = Some(v.clone());
    }
    drop(lock);

    if let Some(tx) = found {
        tx.send(MessageTransfer { msg: msg_bin.to_vec() }).ok();
    }
}*/
