use anyhow::Result;
use borsh::BorshSerialize;
use common::{keys::PublicKey, api::update::{SetUserUpdate, SetUserUpdateResponse, GetUserUpdate, GetUserUpdateResponse}};
use tracing::{error, debug};

use crate::{server::ServerSettings, database::user};

use super::get_user_id;

pub async fn set_update(sett: &ServerSettings, pk: &PublicKey, req: &SetUserUpdate) -> Result<Vec<u8>> {
    let height = req.height as i32;
    let ret    = match user::set_profile_update(&sett.db, get_user_id(sett, pk).await, height, &req.info).await {
        Ok(_) => SetUserUpdateResponse::Ok,
        Err(e) => {
            error!("Failed to update profile for user {}: {}", pk, e);
            SetUserUpdateResponse::Err
        }
    };

    Ok(ret.try_to_vec()?)
}

pub async fn get_update(sett: &ServerSettings, req: &GetUserUpdate) -> Result<Vec<u8>> {
    // TODO: Can't we check if user in server
    let height = req.height as i32;
    let pk_ser = req.public_key.try_to_vec()?;
    let ret    = match user::get_profile_update(&sett.db, &pk_ser, height).await {
        Ok(v) => GetUserUpdateResponse::Ok(v.content),
        Err(e) => {
            debug!("Failed to retrieve profile update for {}: {}", req.public_key, e);
            GetUserUpdateResponse::InvalidHeight
        }
    };

    Ok(ret.try_to_vec()?)
}
