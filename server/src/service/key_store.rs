use anyhow::Result;
use borsh::BorshSerialize;
use common::{keys::PublicKey, api::key_store::GetEphPkResponse};

use crate::{server::ServerSettings, database::key_store};

use super::get_user_id;

pub async fn get_keys_left_count(sett: &ServerSettings, pk: &PublicKey) -> Result<usize> {
    let count = key_store::count_keys_left(&sett.db, get_user_id(sett, pk).await).await?;

    Ok(count)
}

pub async fn store_keys(sett: &ServerSettings, pk: &PublicKey, keys: Vec<(Vec<u8>, Vec<u8>)>) -> Result<()> {
    key_store::insert(&sett.db, get_user_id(sett, pk).await, &keys).await
}

pub async fn get_key(sett: &ServerSettings, pk: &PublicKey) -> GetEphPkResponse {
    if let Ok(ser_pk) = pk.try_to_vec() {
        if let Ok(key) = key_store::pop_key(&sett.db, &ser_pk).await {
            return match key {
                Some(v) => GetEphPkResponse::Ok(v.0, v.1),
                None    => GetEphPkResponse::OutOfKeys
            };
        }
    }
    
    GetEphPkResponse::NotInServer
}
