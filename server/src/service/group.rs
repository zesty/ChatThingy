use anyhow::Result;

use borsh::BorshSerialize;
use common::{keys::PublicKey, api::group::{InitializeStatus, AddMember, AddMemberStatus, LeaveGroupStatus}};

use tracing::{warn, error, info};

use crate::{server::ServerSettings, database::{user, group}};

use super::{get_user, get_user_id};

pub async fn init(sett: &ServerSettings, pk: &PublicKey) -> Result<Vec<u8>> {
    let user = get_user(sett, pk).await;

    let resp = match (user.id, user.permission) {
        (1, -1) => {
            match user::update_permission(&sett.db, user.id, 100).await {
                Ok(_) => {
                    info!("Admin {} successfully init group", pk);
                    if let Some(user_mut) = sett.conn_user_sett.write().await.get_mut(pk) {
                        user_mut.permission = 100;
                    }
                    InitializeStatus::Ok
                },
                Err(e) => {
                    error!("Couldn't init group: {}", e);
                    InitializeStatus::Error
                }
            }
        },
        (1, _)  => {
            warn!("Initial admin {} is trying to reinit group", pk);
            InitializeStatus::Denied
        },
        _       => {
            warn!("User {} is trying to init group", pk);
            InitializeStatus::Denied
        }
    };

    Ok(resp.try_to_vec()?)
}

pub async fn add_member(sett: &ServerSettings, pk: &PublicKey, req: &AddMember) -> Result<Vec<u8>> {
    let user = get_user(sett, pk).await;

    let resp = {
        // TODO: More elaborate permission system
        if user.permission != 100 {
            AddMemberStatus::PermissionDenied
        } else {
            match group::get_last_height(&sett.db).await {
                Ok(v) => {
                    let height = match v {
                        Some(v) => v,
                        None    => 0
                    };
                    match req.public_key.try_to_vec() {
                        Ok(ser_pk) => match user::create(&sett.db, &ser_pk, 0, height).await {
                            Ok(_) => AddMemberStatus::Ok,
                            // TODO: Better way to check if user exists
                            Err(_) => AddMemberStatus::Error
                        },
                        Err(e) => {
                            error!("Couldn't serialize pk of user {}: {}", pk, e);
                            AddMemberStatus::Error
                        }
                    }
                },
                Err(_) => AddMemberStatus::Error
            }
        }
    };


    Ok(resp.try_to_vec()?)
}

pub async fn remove_member(sett: &ServerSettings, pk: &PublicKey) -> Result<Vec<u8>> {
    let user_id = get_user_id(sett, pk).await;
    let status  = match user::update_left_group(&sett.db, user_id, true).await {
        Ok(_) => LeaveGroupStatus::Ok,
        Err(_) => LeaveGroupStatus::Error
    };

    Ok(status.try_to_vec()?)
}
