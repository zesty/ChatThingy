use common::keys::PublicKey;

use crate::server::{ServerSettings, UserSett};

pub mod handshake;
pub mod message;
pub mod key_store;
pub mod group;
pub mod profile;

pub async fn get_user_id(sett: &ServerSettings, pk: &PublicKey) -> i32 {
    sett.conn_user_sett.read().await
        .get(pk)
        .expect("Should be able to get user id")
        .id
}

pub async fn get_user(sett: &ServerSettings, pk: &PublicKey) -> UserSett {
    sett.conn_user_sett.read().await
        .get(pk)
        .expect("Should be able to get user")
        .clone()
}
