use super::Database;
use anyhow::Result;

#[derive(sqlx::FromRow)]
pub struct GroupHeight {
    pub id: i64,
}

pub async fn get_last_height(con: &Database) -> Result<Option<i64>> {
    let rez = sqlx::query_as::<_, GroupHeight>(r#"select id from group_message order by id desc limit 1"#)
        .fetch_one(con.read())
        .await;
    match rez {
        Ok(v) => Ok(Some(v.id)),
        Err(sqlx::Error::RowNotFound) => Ok(None),
        Err(e) => Err(anyhow::Error::new(e))
    }
}
