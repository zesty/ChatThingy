use super::Database;
use anyhow::Result;

#[derive(sqlx::FromRow)]
pub struct User {
    pub id: i32,
    pub permission: i32,
    pub join_height: i64,
    pub left_group: bool
}

#[derive(sqlx::FromRow)]
pub struct ProfileUpdate {
    pub content: Vec<u8>,
}

pub async fn create(con: &Database, pubkey: &[u8], permission: i32, join_height: i64) -> Result<()> {
    if sqlx::query(r#"insert into "user" (public_key, permission, join_height, left_group) values ($1, $2, $3, false)
                      on conflict(public_key) do update set permission = excluded.permission, join_height = excluded.join_height, left_group = false"#)
        .bind(pubkey)
        .bind(permission)
        .bind(join_height)
        .execute(con.write())
        .await?
        .rows_affected() == 0 {
         return Err(anyhow::Error::msg("adding user to database failed"));
    }
    Ok(())
}

pub async fn update_left_group(con: &Database, user_id: i32, left_group: bool) -> Result<()> {
    let mut tx = con.write().begin().await?;

    sqlx::query(r#"update "user" set left_group = $1 where id = $2"#)
        .bind(left_group)
        .bind(user_id)
        .execute(&mut tx)
        .await?;

    sqlx::query(r#"delete from message where "user" = $1"#)
        .bind(user_id)
        .execute(&mut tx)
        .await?;

    tx.commit().await?;
    Ok(())
}

pub async fn update_permission(con: &Database, user_id: i32, permission: i32) -> Result<()> {
    if sqlx::query(r#"update "user" set permission = $1 where id = $2"#)
        .bind(permission)
        .bind(user_id)
        .execute(con.write())
        .await?
        .rows_affected() == 0 {
         return Err(anyhow::Error::msg("updating user permission failed"));
    }
    Ok(())
}

pub async fn get(con: &Database, pubkey: &[u8]) -> Result<Option<User>> {
    let rez = sqlx::query_as::<_, User>(r#"select id, permission, join_height, left_group from "user" where public_key = $1"#)
        .bind(pubkey)
        .fetch_one(con.read())
        .await;

    Ok(match rez {
        Ok(v) => Some(v),
        Err(sqlx::Error::RowNotFound) => None,
        Err(e) => return Err(anyhow::Error::new(e))
    })
}

pub async fn set_profile_update(con: &Database, user_id: i32, height: i32, content: &[u8]) -> Result<()> {
    if sqlx::query(r#"insert into user_update values ($1, $2, $3) on conflict ("user") do update set height = excluded.height, content = excluded.content"#)
        .bind(user_id)
        .bind(height)
        .bind(content)
        .execute(con.write())
        .await?
        .rows_affected() == 0 {
         return Err(anyhow::Error::msg("updating user profile status failed"));
    }
    Ok(())
}

pub async fn get_profile_update(con: &Database, pubkey: &[u8], height: i32) -> Result<ProfileUpdate> {
    let rez = sqlx::query_as::<_, ProfileUpdate>(r#"select content from user_update, "user" where public_key = $1 and "user".id = user_update.user and height = $2"#)
        .bind(pubkey)
        .bind(height)
        .fetch_one(con.read())
        .await?;
    Ok(rez)
}
