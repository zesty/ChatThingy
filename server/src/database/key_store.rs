use super::Database;
use anyhow::Result;

#[derive(sqlx::FromRow)]
struct EphKeysCount {
    pub count: i64,
}

#[derive(sqlx::FromRow)]
struct EphKey {
    pub eph_pk: Vec<u8>,
    pub sig: Vec<u8>
}

pub async fn count_keys_left(con: &Database, user_id: i32) -> Result<usize> {
    let rez = sqlx::query_as::<_, EphKeysCount>(r#"select count(eph_pk) as "count" from ephemeral_keys where "user" = $1"#)
        .bind(user_id)
        .fetch_one(con.read())
        .await?;
    Ok(rez.count as usize)
}

pub async fn insert(con: &Database, user_id: i32, keys: &[(Vec<u8>, Vec<u8>)]) -> Result<()> {
    let mut tx = con.write().begin().await?;

    for key in keys {
        sqlx::query(r#"insert into ephemeral_keys values ($1, $2, $3)"#)
            .bind(user_id)
            .bind(&key.0)
            .bind(&key.1)
            .execute(&mut tx)
            .await?;
    }
    tx.commit().await?;

    Ok(())
}

pub async fn pop_key(con: &Database, pk: &[u8]) -> Result<Option<(Vec<u8>, Vec<u8>)>> {
    let rez = sqlx::query_as::<_, EphKey>(r#"
            with deleted as (
              delete from ephemeral_keys
              where ctid in (select ctid from ephemeral_keys where "user" = (select id from "user" where public_key = $1) limit 1)
              returning eph_pk, sig
            )
            select eph_pk, sig from deleted;
            "#)
        .bind(pk)
        .fetch_one(con.read())
        .await;
    match rez {
        Ok(v) => Ok(Some((v.eph_pk, v.sig))),
        Err(sqlx::Error::RowNotFound) => Ok(None),
        Err(e) => Err(anyhow::Error::new(e))
    }
}
