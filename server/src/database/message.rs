use crate::server::{ServerSettings, MessageTransfer, GroupMessageTransfer};

use super::Database;
use borsh::BorshDeserialize;
use common::keys::PublicKey;
use sqlx::postgres::PgListener;
use base64::decode;
use async_stream::stream;
use futures::{ Stream, TryStreamExt };
use tokio::sync::{mpsc, broadcast};
use anyhow::Result;

#[derive(sqlx::FromRow, Debug)]
pub struct Message {
    pub message: Vec<u8>,
    pub id: i64
}

pub async fn add(con: &Database, dest: &[u8], message: &[u8]) -> Result<()> {
    if sqlx::query(r#"insert into message ("user", message) values ((select id from "user" where public_key = $1), $2)"#)
        .bind(dest)
        .bind(message)
        .execute(con.write())
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("Error adding message to database"));
    }
    Ok(())
}

pub async fn add_group(con: &Database, source_user_id: i32, message: &[u8]) -> Result<()> {
    if sqlx::query(r#"insert into group_message ("user", message) values ($1, $2)"#)
        .bind(source_user_id)
        .bind(message)
        .execute(con.write())
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("Error adding message to database"));
    }
    Ok(())
}

pub async fn poll_feed(sett: &ServerSettings) -> Result<()> {
    let mut listener = PgListener::connect_with(&sett.db.clone().read()).await?;
    listener.listen("dm").await?;
    listener.listen("gm").await?;
    loop {
        if let Some(notification) = listener.try_recv().await? {
            match notification.channel() {
                "dm" => {
                    let spl = notification.payload().split(',').collect::<Vec<&str>>();
                    if spl.len() != 2 {
                        continue;
                    }
                    let last = match spl[1].parse::<i64>() {
                        Ok(v) => v,
                        Err(_) => continue
                    };

                    if let Ok(v) = decode(spl[0]) {
                        if let Ok(pk) = PublicKey::try_from_slice(&v) {
                            if let Some(tx) = sett.connected_users.read().await.get(&pk) {
                                tx.clone().send(MessageTransfer { last }).await.ok();
                            }
                        }
                    }
                },
                "gm" => {
                    let spl = notification.payload().split(',').collect::<Vec<&str>>();
                    if spl.len() != 2 {
                        continue;
                    }
                    
                    let id = match spl[0].parse::<i32>() {
                        Ok(v) => v,
                        Err(_) => continue
                    };

                    let last = match spl[1].parse::<i64>() {
                        Ok(v) => v,
                        Err(_) => continue
                    };

                    sett.group_msg_notification.send(GroupMessageTransfer { last, id }).ok();
                },
                _ => ()
            }
        }
    }
}

#[derive(Debug, PartialEq)]
enum PolledMessageType {
    Direct,
    Group,
    Both,
    None
}

pub async fn poll_messages<'a>(con: &'a Database, mut rx: mpsc::Receiver<MessageTransfer>,
                               mut group_rx: broadcast::Receiver<GroupMessageTransfer>, user_id: i32,
                               last: &'a mut i64,
                               last_group: &'a mut i64) -> impl Stream<Item = Message> + 'a + Unpin {
    Box::pin(stream! {
        let mut available = PolledMessageType::Both;
        loop {
            if available == PolledMessageType::Direct || available == PolledMessageType::Both {
                let mut rez = sqlx::query_as::<_, Message>(r#"select id, message from message where
                                                            "user" = $1 and id > $2 order by id asc"#)
                    .bind(user_id)
                    .bind(*last)
                    .fetch(con.read());
                loop {
                    let next = rez.try_next().await;
                    if next.is_err() {
                        break;
                    }
                    match next.unwrap() {
                        Some(data) => {
                            *last = data.id;
                            yield data
                        },
                        None => break
                    }
                }
            }
            if available == PolledMessageType::Group || available == PolledMessageType::Both {
                let mut rez = sqlx::query_as::<_, Message>(r#"select id, message from group_message where
                                                           "user" != $1 and id > $2 order by id asc"#)
                    .bind(user_id)
                    .bind(*last_group)
                    .fetch(con.read());
                loop {
                    let next = rez.try_next().await;
                    if next.is_err() {
                        break;
                    }
                    match next.unwrap() {
                        Some(data) => {
                            *last_group = data.id;
                            yield data
                        },
                        None => break
                    }
                }
            }
            available = PolledMessageType::None;
            
            tokio::select! {
                r = rx.recv() => {
                    match r {
                        Some(notification) => {
                            if *last < notification.last {
                                available = PolledMessageType::Direct;
                                continue;
                            }
                        },
                        None => break
                    }
                },
                r = group_rx.recv() => {
                    match r {
                        Ok(notification) => {
                            if notification.id != user_id && *last_group < notification.last {
                                available = PolledMessageType::Group;
                                continue;
                            }
                        },
                        Err(broadcast::error::RecvError::Lagged(_)) => continue,
                        Err(_) => break
                    }
                }
            }
        }
    })
}
