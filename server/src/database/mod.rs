pub mod user;
pub mod group;
pub mod message;
pub mod key_store;

use sqlx::{Postgres, Pool};
use std::error::Error;

use crate::config::Config;

#[derive(Debug, Clone)]
pub struct Database {
    pub con: Pool<Postgres>
}

impl Database {
    pub async fn new(conf: &Config) -> Result<Database, Box<dyn Error + Send + Sync>> {
        let pool = sqlx::PgPool::connect(&conf.db.urls[0]).await?;
        Ok(Database { con: pool })
    }
    
    pub fn read(&self) -> &Pool<Postgres> {
        &self.con
    }

    pub fn write(&self) -> &Pool<Postgres> {
        &self.con
    }
}
