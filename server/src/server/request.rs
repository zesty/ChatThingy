use std::sync::Arc;
use borsh::BorshDeserialize;
use tokio::{io::{AsyncReadExt, AsyncWriteExt}, sync::mpsc};
use tokio_yamux::StreamHandle;
use anyhow::Result;
use tracing::info;
use futures::StreamExt;

use common::{keys::PublicKey, api::{RequestType, message::MessageNotification, group::AddMember, update::{MAX_UPDATE_SIZE, SetUserUpdate, GetUserUpdate}}};

use crate::{service::{self, get_user_id, group, profile, get_user}, database::message::Message};

use super::ServerSettings;

async fn ping(stream: &mut StreamHandle) -> Result<()> {
    loop {
        stream.read_u8().await?;
        stream.write_u8(1u8).await?;
    };
}

async fn send_message(stream: &mut StreamHandle, sett: &ServerSettings, pk: &PublicKey) -> Result<()> {
    loop {
        let size = stream.read_u32().await?;

        let mut buf = vec![0u8; size as usize];
        stream.read_exact(&mut buf).await?;

        let resp = service::message::send(&mut buf, sett, pk).await?;

        stream.write_u32(resp.len() as u32).await?;
        stream.write_all(resp.as_slice()).await?;
    };
}

async fn message_listener(stream: &mut StreamHandle, sett: &ServerSettings, pk: &PublicKey) {
    let mut notif;
    if let Ok(v) = get_notification_last(stream).await {
        notif = v;
    } else {
        let mut lock = sett.connected_users.write().await;
        lock.remove(pk);
        drop(lock);
        return;
    }

    // We won't serve messages older than when user joined
    let join_height       = get_user(sett, pk).await.join_height;
    let mut last_group_id = if notif.last_group_id < join_height { join_height } else { notif.last_group_id };

    'OUTER: loop {
        // We're using a bounded channel with 1 capacity here because we only want to knew if there
        // are new messages for the user
        let (tx, rx) = mpsc::channel(1);
        let mut lock = sett.connected_users.write().await;
        lock.insert(pk.clone(), tx);
        drop(lock);

        let group_rx = sett.group_msg_notification.subscribe();
        // Now we start listening for new message
        let mut msg_stream = crate::database::message::poll_messages(
            &sett.db, rx, group_rx,
            get_user_id(sett, pk).await,
            &mut notif.last_id,
            &mut last_group_id).await;
        
        loop {
            match msg_stream.next().await {
                Some(msg) => {
                    if write_message(stream, &msg).await.is_err() {
                        break 'OUTER;
                    }
                },
                None => {
                    // Connection is dropped with user/Database error?
                    break 'OUTER;
                }
            }
        }
    }
}

async fn get_notification_last(stream: &mut StreamHandle) -> Result<MessageNotification> {
    let size    = stream.read_u32().await?;
    let mut buf = vec![0u8; size as usize];
    stream.read_exact(&mut buf).await?;
    let notif   = MessageNotification::try_from_slice(&buf)?;

    Ok(notif)
}

async fn write_message(stream: &mut StreamHandle, msg: &Message) -> Result<()> {
    stream.write_i64(msg.id).await?;
    stream.write_u32(msg.message.len() as u32).await?;
    stream.write_all(&msg.message).await?;

    Ok(())
}

async fn init_group(stream: &mut StreamHandle, sett: &ServerSettings, pk: &PublicKey) -> Result<()> {
    let status = group::init(sett, pk).await?;
    stream.write_u32(status.len() as u32).await?;
    stream.write_all(&status).await?;
    Ok(())
}

async fn add_member(stream: &mut StreamHandle, sett: &ServerSettings, pk: &PublicKey) -> Result<()> {
    let size    = stream.read_u32().await?;
    let mut buf = vec![0u8; size as usize];
    stream.read_exact(&mut buf).await?;
    let req     = AddMember::try_from_slice(&buf)?;
    drop(buf);

    let status = group::add_member(sett, pk, &req).await?;
    stream.write_u32(status.len() as u32).await?;
    stream.write_all(&status).await?;
    Ok(())
}

async fn leave_group(stream: &mut StreamHandle, sett: &ServerSettings, pk: &PublicKey) -> Result<()> {
    let status = group::remove_member(sett, pk).await?;
    stream.write_u32(status.len() as u32).await?;
    stream.write_all(&status).await?;
    Ok(())
}

async fn set_update_info(stream: &mut StreamHandle, sett: &ServerSettings, pk: &PublicKey) -> Result<()> {
    let size = stream.read_u32().await? as usize;
    if size > MAX_UPDATE_SIZE {
        stream.shutdown().await?;
        return Err(anyhow::Error::msg(format!("update profile request from {} exceeding MAX_UPDATE_SIZE", pk)));
    }
    let mut buf = vec![0u8; size];
    stream.read_exact(&mut buf).await?;
    let req     = SetUserUpdate::try_from_slice(&buf)?;
    drop(buf);

    let status = profile::set_update(sett, pk, &req).await?;
    stream.write_u32(status.len() as u32).await?;
    stream.write_all(&status).await?;
    Ok(())
}

async fn get_update_info(stream: &mut StreamHandle, sett: &ServerSettings) -> Result<()> {
    let size = stream.read_u32().await? as usize;
    let mut buf = vec![0u8; size];
    stream.read_exact(&mut buf).await?;
    let req     = GetUserUpdate::try_from_slice(&buf)?;
    drop(buf);

    let status = profile::get_update(sett, &req).await?;
    stream.write_u32(status.len() as u32).await?;
    stream.write_all(&status).await?;
    Ok(())
}

pub async fn handle(stream: &mut StreamHandle, sett: Arc<ServerSettings>, pk: PublicKey) -> Result<()> {
    let req_type = std::convert::TryInto::<RequestType>::try_into(stream.read_u8().await?)?;

    info!("New request {:?} from {}", req_type, pk);
    match req_type {
        RequestType::Ping => ping(stream).await?,
        RequestType::MessageListener => message_listener(stream, &sett, &pk).await,
        RequestType::SendMessage => send_message(stream, &sett, &pk).await?,
        RequestType::GetEphPk => super::key_store::get_key(stream, &sett).await?,
        RequestType::InitGroup => init_group(stream, &sett, &pk).await?,
        RequestType::AddMember => add_member(stream, &sett, &pk).await?,
        RequestType::UpdateInfo => set_update_info(stream, &sett, &pk).await?,
        RequestType::GetUpdateInfo => get_update_info(stream, &sett).await?,
        RequestType::LeaveGroup => leave_group(stream, &sett, &pk).await?,
        RequestType::Unknewn => (),
    }

    Ok(())
}
