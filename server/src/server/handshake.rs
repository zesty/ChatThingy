use borsh::BorshSerialize;
use tokio::{io::{AsyncWriteExt, AsyncReadExt}, net::TcpStream};
use anyhow::Result;

use common::api;

use crate::service::{self, handshake::UserSettings};

use super::ServerSettings;

pub async fn init(stream: &mut TcpStream, sett: &ServerSettings) -> Result<UserSettings> {
    // I.
    // We send a challenge first
    let challenge = service::handshake::challenge()?;
    let msg       = api::handshake::Init {
        version: api::VERSION,
        challenge: challenge.clone()
    }.try_to_vec()?;

    stream.write_u32(msg.len() as u32).await?;
    stream.write_all(msg.as_slice()).await?;

    // II.
    // Next we receive challenge and public key with a signature of challenge
    let resp_len = stream.read_u32().await?;
    if resp_len > api::handshake::HANDSHAKE_MESSAGE_SIZE_MAX || resp_len == 0 {
        return Err(anyhow::Error::msg("Handshake response message invalid size"));
    }

    let mut resp = vec![0u8; resp_len as usize];
    stream.read_exact(&mut resp).await?;

    // Then we verify if signature is valid
    let user;
    match service::handshake::verify_challenge_response(challenge.as_slice(), &resp, sett).await? {
        // III.
        Ok(v) => {
            stream.write_u32(v.resp.len() as u32).await?;
            stream.write_all(&v.resp).await?;
            user = v.user;
        },
        Err(v) => {
            stream.write_u32(v.len() as u32).await?;
            stream.write_all(&v).await?;
            return Err(anyhow::Error::msg("Handshake second phase failed"));
        }
    }

    Ok(user)
}
