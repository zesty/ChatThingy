mod handshake;
mod request;
mod key_store;

use common::keys::PublicKey;
use tokio::{
    net::{TcpListener, TcpStream},
    sync::{
        Semaphore, RwLock,
        mpsc,
        broadcast
    },
    io::AsyncWriteExt
};
use torut::onion::TorSecretKeyV3;
use zeroize::Zeroize;
use std::{
    net::{SocketAddr, IpAddr, Ipv4Addr},
    sync::Arc
};
use tracing::{error, info, debug};
use anyhow::Result;
use hashbrown::HashMap;
use futures::prelude::*;

use crate::{config::Config, network::{tor::Tor, Network}, database::{Database, message::poll_feed}};

/// A type holding all info that may be needed by a task
pub struct ServerSettings {
    /// Normal config
    pub conf: Config,
    /// Database connection
    pub db: Database,
    /// Connected users to this server
    pub connected_users: RwLock<HashMap<PublicKey, mpsc::Sender<MessageTransfer>>>,
    /// User settings
    pub conn_user_sett: RwLock<HashMap<PublicKey, UserSett>>,
    /// Group message notification
    pub group_msg_notification: broadcast::Sender<GroupMessageTransfer>
}

#[derive(Clone)]
pub struct UserSett {
    pub id: i32,
    pub permission: i32,
    pub join_height: i64
}

#[derive(Debug, Clone)]
pub struct GroupMessageTransfer {
    pub id: i32,
    pub last: i64
}

#[derive(Debug, Clone)]
pub struct MessageTransfer {
    pub last: i64
}

async fn handle_connection(mut stream: TcpStream, sett: Arc<ServerSettings>)
 -> Result<()> {

    // First we do initial handshake
    let user = match handshake::init(&mut stream, &sett).await {
        Ok(v) => v,
        Err(e) => {
            stream.shutdown().await?;
            return Err(e);
        }
    };

    info!("User {} successfully authentified", user.pk);

    let user_pk  = user.pk.clone();
    let mut lock = sett.conn_user_sett.write().await;
    lock.insert(user.pk, UserSett { id: user.id, permission: user.permission, join_height: user.join_height });
    drop(lock);

    // Now we multiplex connection
    let mut yam_con = tokio_yamux::Session::new_server(stream, tokio_yamux::config::Config::default());

    // Spawn a task that checks if user key store is low to be refilled
    let sett_clone    = sett.clone();
    let user_pk_clone = user_pk.clone();
    let control       = yam_con.control();
    tokio::spawn(async move {
        if let Err(e) = key_store::check_keys_left(control, &sett_clone, &user_pk_clone).await {
            debug!("Couldn't refill keys for {}: {}", user_pk_clone, e);
        }
    });

    while let Some(Ok(mut stream)) = yam_con.next().await {
        let sett_clone    = sett.clone();
        let user_pk_clone = user_pk.clone();
        tokio::spawn(async move {
            if let Err(e) = request::handle(&mut stream, sett_clone, user_pk_clone.clone()).await {
                debug!("Stream for {} closed: {}", user_pk_clone, e);
            }
            stream.shutdown().await.ok();
        });
    }

    yam_con.control().close().await;
    // Before we leave, we drop user reader
    let mut lock = sett.connected_users.write().await;
    lock.remove(&user_pk);
    drop(lock);

    // And his settings too
    let mut lock = sett.conn_user_sett.write().await;
    lock.remove(&user_pk);
    drop(lock);

    
    Ok(())
}

async fn accept_connection(listener: TcpListener, sett: Arc<ServerSettings>) {
    let sem = Arc::new(Semaphore::new(sett.conf.limits.connections));

    loop {
        tokio::select! {
            _ = tokio::signal::ctrl_c() => {
                drop(listener);
                break;
            },
            s = listener.accept() => {
                match s {
                    Ok((socket, addr)) => { 
                        let sem_clone  = sem.clone();
                        let sett_clone = sett.clone();
                        tokio::spawn(async move {
                            let aq = sem_clone.try_acquire();
                            if let Ok(_guard) = aq {
                                if let Err(e) = handle_connection(socket, sett_clone).await {
                                    debug!("Error occured with user {} socket: {}", addr, e);
                                }
                            };
                        } );
                    }
                    Err(e) => error!("An error occured while accepting a connection: {}", e),
                }
            }
        }
    }
}

async fn bind_to_network(config: &mut Config) -> (TcpListener, Box<dyn Network>) {
    // Binding to the hidden service according to type of network
    match config.network.ntype.as_str() {
        "tor" => {
            let listener = match TcpListener::bind(SocketAddr::new(IpAddr::V4(Ipv4Addr::new( 127, 0, 0, 1 )), 0)).await {
                Ok(v) => v,
                Err(e) => panic!("Unable to bind to port: {}", e)
            };

            let local_port = listener.local_addr()
                .expect("Can't get server bind address")
                .port();

            let mut tor = Box::new(Tor::new(config.misc.tor_binary_path.as_str()).await);

            for enc_key in config.network.keys.as_slice() {
                let mut enc_key = format!("\"{}\"", enc_key);
                match serde_json::from_slice::<TorSecretKeyV3>(enc_key.as_bytes()) {
                    Ok(key) => {
                        enc_key.zeroize();
                        let domain = tor.add_key_with_port(key.as_bytes(), local_port, config.network.port).await;
                        info!("Listening on: {}:{}", domain, config.network.port);
                    },
                    Err(_) => {
                        enc_key.zeroize();
                        panic!("Can't parse a key of the network");
                    }
                }
            }
            config.network.keys.zeroize();
            (listener, tor)
        },
        /*"lokinet" => {
            // TODO
        }*/
        #[cfg(debug_assertions)]
        "testnet" => {
            let listener = match TcpListener::bind(SocketAddr::new(IpAddr::V4(Ipv4Addr::new( 127, 0, 0, 1 )), config.network.port)).await {
                Ok(v) => v,
                Err(e) => panic!("Unable to bind to port: {}", e)
            };
            info!("Listening on: 127.0.0.1:{}", config.network.port);
            (listener, Box::new(super::network::TestNet {}))
        },
        _ => panic!("Unknewn/Unsupported network '{}' specified in config", config.network.ntype)
    }
}

pub async fn start(mut config: Config) {
    let (listener, mut net) = bind_to_network(&mut config).await;
    let (tx, _)             = broadcast::channel(100);
    let sett                = Arc::new(ServerSettings {
        db: Database::new(&config).await.unwrap(),
        conf: config,
        connected_users: RwLock::new(HashMap::new()),
        conn_user_sett: RwLock::new(HashMap::new()),
        group_msg_notification: tx
    });

    // Spawning message listener
    // That notifies message notification tasks
    // of users when there is message to read
    let sett_clone = sett.clone();
    tokio::spawn(async move {
        loop {
            poll_feed(&sett_clone).await.ok();
        }
    });

    accept_connection(listener, sett).await;
    net.close().await;
}
