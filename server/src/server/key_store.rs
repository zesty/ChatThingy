use borsh::{BorshSerialize, BorshDeserialize};
use tokio::io::{AsyncWriteExt, AsyncReadExt};
use tokio_yamux::{Control, StreamHandle};
use anyhow::Result;

use common::{keys::PublicKey, api::{self, ServerRequestType, key_store::RunOutOfEphPkNotification}};
use tracing::info;

use crate::service::key_store;

use super::ServerSettings;


pub async fn check_keys_left(mut control: Control, sett: &ServerSettings, pk: &PublicKey) -> Result<()> {
    let left = key_store::get_keys_left_count(sett, pk).await?;
    if left > api::key_store::KEYS_REFILL_THRESHOLD {
        // Nothing to do
        return Ok(());
    }

    let mut stream = match control.open_stream().await {
        Ok(v) => v,
        Err(e) => return Err(anyhow::Error::from(e))
    };

    stream.write_u8(ServerRequestType::RunOutOfEphPkNotification as u8).await?;

    let req = RunOutOfEphPkNotification { number: (api::key_store::KEYS_COUNT - left) as u8 }.try_to_vec()?;
    stream.write_u32(req.len() as u32).await?;
    stream.write_all(&req).await?;

    let len = stream.read_u32().await?;

    if len > api::key_store::KEYS_REFILL_RESPONSE_MAX_SIZE {
        return Err(anyhow::Error::msg("keys refill response bigger than KEYS_REFILL_RESPONSE_MAX_SIZE"));
    }

    let mut buf = vec![0u8; len as usize];
    stream.read_exact(&mut buf).await?;
    let resp    = api::key_store::AddMoreEphPk::try_from_slice(&buf)?;

    key_store::store_keys(sett, pk, resp.list).await?;

    info!("User {} ephemeral keys restocked", pk);

    Ok(())
}

pub async fn get_key(stream: &mut StreamHandle, sett: &ServerSettings) -> Result<()> {
    let len = stream.read_u32().await? as usize;

    if len > api::message::MESSAGE_RAW_SIZE_MAX {
        return Err(anyhow::Error::msg("got GetEphPk with request bigger than MESSAGE_RAW_SIZE_MAX"));
    }

    let mut buf = vec![0u8; len];
    stream.read_exact(&mut buf).await?;
    let req     = api::key_store::GetEphPkRequest::try_from_slice(&buf)?;
    
    let resp    = key_store::get_key(sett, &req.user).await.try_to_vec()?;

    stream.write_u32(resp.len() as u32).await?;
    stream.write_all(&resp).await?;


    Ok(())
}
