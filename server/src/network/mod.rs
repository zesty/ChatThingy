pub mod tor;

#[async_trait::async_trait]
pub trait Network {
    async fn add_key_with_port(&mut self, key: &[u8], local_port: u16, public_port: u16) -> String;
    async fn start(&mut self);
    async fn close(&mut self);
}

/// A debug network that uses localhost
#[cfg(debug_assertions)]
pub struct TestNet {}


#[cfg(debug_assertions)]
#[async_trait::async_trait]
impl Network for TestNet {
    async fn add_key_with_port(&mut self, _key: &[u8], _local_port: u16, _public_port: u16) -> String { "".to_string() }
    async fn start(&mut self) {}
    async fn close(&mut self) {}
}
