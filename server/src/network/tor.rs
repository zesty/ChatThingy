use std::net::{SocketAddr, IpAddr, Ipv4Addr};
use std::pin::Pin;

use futures::Future;
use torut::utils::{run_tor, AutoKillChild};
use torut::control::{UnauthenticatedConn, TorAuthMethod, TorAuthData, AuthenticatedConn, ConnError, AsyncEvent};
use tokio::net::{TcpStream, TcpListener};
use tempfile::NamedTempFile;

use super::Network;

type WayTooMuch = Box<dyn Fn(AsyncEvent) -> Pin<Box<dyn Future<Output = Result<(), ConnError>> + Send>> + Send + Sync>;

pub struct Tor {
    ac: AuthenticatedConn<TcpStream, WayTooMuch>,
    _child: AutoKillChild
}

impl Tor {
    pub async fn new(tor_binary_path: &str) -> Self {
        let file = NamedTempFile::new()
            .expect("Couldn't create temporary file for tor config");

        tokio::fs::write(file.path(), "SocksPort 0\nDataDirectory /tmp/fd/\n").await
            .expect("Couldn't write to temporary tor config file");

        let listener = TcpListener::bind("127.0.0.1:0").await
            .expect("Can't bind to any port for tor control port?");
        let port = listener.local_addr()
            .expect("Can't get local address of tor control port")
            .port();
        drop(listener);

        let child = run_tor(tor_binary_path, [
            "--ControlPort", &port.to_string(),
            "-f", file.path().to_str().unwrap()
        ].iter()).expect("Starting tor failed");
        let _child = AutoKillChild::new(child);

        let s = TcpStream::connect(&format!("127.0.0.1:{}", port)).await.unwrap();
        let mut utc = UnauthenticatedConn::new(s);
        let proto_info = utc.load_protocol_info().await.unwrap();

        assert!(proto_info.auth_methods.contains(&TorAuthMethod::Null), "Null authentication is not allowed");
        utc.authenticate(&TorAuthData::Null).await.unwrap();
        let mut ac = utc.into_authenticated().await;

        let f: WayTooMuch = Box::new(|_| {
            Box::pin(async move { Ok(()) })
        });
        
        ac.set_async_event_handler(Some(f));
        ac.take_ownership().await.unwrap();

        file.close().ok();

        Tor { ac, _child }
    }
}

#[async_trait::async_trait]
impl Network for Tor {
    async fn add_key_with_port(&mut self, key: &[u8], local_port: u16, public_port: u16) -> String {
        let mut raw: [u8; 64] = [0u8; 64];
        raw.copy_from_slice(key);
        let key = torut::onion::TorSecretKeyV3::from_bytes(&mut raw);

        self.ac.add_onion_v3(&key, false, false, false, None, &mut [
            (public_port, SocketAddr::new(IpAddr::from(Ipv4Addr::new(127, 0, 0, 1)), local_port)),
        ].iter())
            .await
            .unwrap();

        key.public().get_onion_address().to_string()
    }

    async fn start(&mut self) {}

    async fn close(&mut self) {}
}
