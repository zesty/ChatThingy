
create table "user" (
    id serial primary key,
    public_key bytea not null unique,
    permission integer not null,
    join_height bigint not null,
    left_group boolean not null,
    constraint valid_permission
        check (permission >= -1 and permission <= 100)
);

create table user_update (
    "user" int primary key,
    height int not null,
    content bytea not null,
    constraint fk_muser foreign key("user") references "user" (id) on delete cascade
);

create table ephemeral_keys (
    "user" int not null,
    eph_pk bytea not null,
    sig bytea not null,
    constraint fk_muser foreign key("user") references "user" (id) on delete cascade
);

create table message_counter (
    "user" int primary key, 
    last_msg bigint not null,
    constraint fk_lmuser foreign key("user") references "user" (id) on delete cascade
);

create table message (
    "user" int not null,
    id bigint not null default 1,
    message bytea not null,
    constraint fk_muser foreign key("user") references "user" (id)
);

create table group_message (
    "user" int not null,
    id bigserial primary key,
    message bytea not null,
    constraint fk_muser foreign key("user") references "user" (id)
);

create function next_message(user_id int) 
    returns bigint
as
$$
    insert into message_counter ("user", last_msg) 
    values (user_id, 1)
    on conflict ("user") 
    do update 
        set last_msg = message_counter.last_msg + 1
    returning last_msg;
$$
language sql
volatile;

create function increment_message()
    returns trigger
as
$$
begin
    new.id := next_message(new.user);
    return new;
end;
$$
language plpgsql;

create trigger message_increment
    before insert on message
    for each row
    execute procedure increment_message();

create or replace function new_message_notification() returns trigger as $$
    declare
    pk bytea;
    begin
        pk := (select public_key from "user" where id = NEW.user);
        perform pg_notify('dm', format('%s,%s', encode(pk, 'base64'), NEW.id::text));
        return null;
    end;
$$ language plpgsql;

create or replace function new_group_message_notification() returns trigger as $$
    begin
        perform pg_notify('gm', format('%s,%s', NEW.user::text, NEW.id::text));
        return null;
    end;
$$ language plpgsql;

create or replace trigger trigger_new_message_notification
  after insert
  on message
  for each row
  execute procedure new_message_notification();

create or replace trigger trigger_new_group_message_notification
  after insert
  on group_message
  for each row
  execute procedure new_group_message_notification();

--- adding administrator
--- Only 1 administrator must be present in the start
--- their initial permission should be -1
insert into "user" (public_key, permission, join_height, left_group) values (decode('', 'base64'), -1, 0, false);
