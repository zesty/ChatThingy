use borsh::{BorshSerialize, BorshDeserialize};
use common::keys::PublicKey;

pub static AVATAR_SIZE_TOO_BIG: usize   = 29500;
pub static PROFILE_NAME_TOO_LONG: usize = 30;

/// Blob Message
#[repr(u8)]
#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub enum MessageBlob {
    /// New contact request, but it must be using a valid invite code we generated before
    NewContactRequest(NewContactRequestBlob),
    /// Direct message
    Message(CipherMessageBlob),
    /// Welcome message when we join group
    Welcome(WelcomeBlob),
    /// A message sent to one the members to be removed from the group
    RemoveMe(RemoveMeBlob)
}

#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct RemoveMeBlob {
    pub proposal: Vec<u8>
}

/// Sent to new member of group
#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct WelcomeBlob {
    /// Mls welcome message
    pub welcome: Vec<u8>
}

#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct NewContactRequestBlob {
    /// Sender public key for key exchange
    pub eph_dh_pk: Vec<u8>,
    /// Receiver ephemeral key we got from server key store
    pub gen_dh_pk: Vec<u8>,
    /// Sender first ephemeral key for the send chain
    pub ratchet_eph_pk: Vec<u8>
}

#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct CipherMessageBlob {
    /// Nonce of the header
    pub header_nonce: [u8; 24],
    /// Header of message
    pub header: Vec<u8>,
    /// Nonce of the message itself
    pub nonce: [u8; 24],
    /// Some associated data with message
    pub ad: Vec<u8>,
    /// The encrypted message itself, which translates to PlainMessage after decrypting it
    pub msg: Vec<u8>
}

#[repr(u8)]
#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub enum PlainTextBlob {
    Message(PlainMessage),
    ProfileUpdate(ProfileUpdate)
}

#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct ProfileUpdate {
    pub height: u32,
    pub secret: Vec<u8>,
    pub nonce: Vec<u8>
}

#[repr(u8)]
#[derive(BorshSerialize, BorshDeserialize, Debug, Clone)]
pub enum PlainMessage {
    /// Normal message
    Text { msg: String },
}

/// Message representation
/// Sent to user
#[repr(u8)]
#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub enum Message {
    Plain(PlainMessage),
    /// A new contact notification
    /// We are the sender so we cannot initiate conversation
    NewPendingContactSource,
    /// A new contact notification
    /// Receiver and the first that can initiate conversation
    NewPendingContactReceiver,
    /// Emitted when a user leaves the group
    ContactLeftGroup,
    /// New member added to group
    NewMemberAdded(PublicKey),
    /// We can't decrypt message
    DecryptionError,
    /// Message got corrupted in the process
    Unreadable
}

/// Profile info
#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub struct ProfileInfoBlob {
    pub name: String,
    pub avatar: Vec<u8>
}
