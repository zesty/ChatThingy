pub(crate) mod tor;

use anyhow::Result;
use tokio::io::{AsyncWrite, AsyncRead};

pub trait StreamSocketU: Send + Sync + AsyncRead + AsyncWrite + Unpin {}
pub type StreamSocket = Box<dyn StreamSocketU>;
//pub type StreamSocketBuffered = BufStream<Box<dyn StreamSocketU>>;

pub enum NetworkType {
    Tor,
    #[cfg(debug_assertions)]
    TestNet
}

#[derive(Debug, PartialEq)]
pub enum NetworkStatus {
    Starting,
    Ready,
    Shutdown
}

#[async_trait::async_trait]
pub trait Network {
    async fn connect_stream(&self, addr: &str) -> Result<StreamSocket>;
    async fn status(&mut self) -> NetworkStatus;
    async fn stop(&mut self);
    async fn start(&mut self);
}

impl StreamSocketU for tokio::net::TcpStream {}

/// Network for debug purposes
#[cfg(debug_assertions)]
pub struct TestNet {}

#[cfg(debug_assertions)]
#[async_trait::async_trait]
impl Network for TestNet {
    async fn connect_stream(&self, addr: &str) -> Result<StreamSocket> {
        match tokio::net::TcpStream::connect(addr).await {
            Ok(v) => Ok(Box::new(v)),
            Err(e) => Err(anyhow::Error::new(e))
        }
    }
    async fn status(&mut self) -> NetworkStatus {
        NetworkStatus::Ready
    }
    async fn stop(&mut self) {}
    async fn start(&mut self) {}
}
