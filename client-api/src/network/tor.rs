use std::pin::Pin;
use anyhow::Result;
use futures::Future;
use tokio_socks::tcp::Socks5Stream;
use torut::utils::{run_tor, AutoKillChild};
use torut::control::{UnauthenticatedConn, TorAuthMethod, TorAuthData, AuthenticatedConn, ConnError, AsyncEvent, TorSignal};
use tokio::net::{TcpStream, TcpListener};
use tempfile::NamedTempFile;

use super::{Network, StreamSocket, StreamSocketU, NetworkStatus};

type WayTooMuch = Box<dyn Fn(AsyncEvent) -> Pin<Box<dyn Future<Output = Result<(), ConnError>> + Send>> + Send + Sync>;

pub struct Tor {
    ac: AuthenticatedConn<TcpStream, WayTooMuch>,
    _child: AutoKillChild,
    socks_addr: String,
    directory: String,
    shutdown: bool
}

impl Tor {
    async fn get_free_port() -> u16 {
        let listener = TcpListener::bind("127.0.0.1:0").await
            .expect("Can't bind to any port for tor?");
        listener.local_addr()
            .expect("Can't get local address of tor")
            .port()
    }

    pub async fn new(dir: &str) -> Self {
        let file = NamedTempFile::new()
            .expect("Couldn't create temporary file for tor config");

        let log_file = format!("{}/tor.log", dir);

        tokio::fs::remove_file(&log_file).await.ok();

        let port = Tor::get_free_port().await;

        tokio::fs::write(file.path(), format!("SocksPort {}\nDataDirectory {}/tor\n", port, dir)).await
            .expect("Couldn't write to temporary tor config file");

        let control_port = Tor::get_free_port().await;

        let child = run_tor("/bin/tor", [
            "--ControlPort", &control_port.to_string(),
            "-f", file.path().to_str().unwrap(),
            "--Log", &format!("notice file {}", log_file)
        ].iter()).expect("Starting tor failed");
        let _child = AutoKillChild::new(child);

        let s = TcpStream::connect(&format!("127.0.0.1:{}", control_port)).await.unwrap();
        let mut utc = UnauthenticatedConn::new(s);
        let proto_info = utc.load_protocol_info().await.unwrap();

        assert!(proto_info.auth_methods.contains(&TorAuthMethod::Null), "Null authentication is not allowed");
        utc.authenticate(&TorAuthData::Null).await.unwrap();
        let mut ac = utc.into_authenticated().await;

        let f: WayTooMuch = Box::new(|_| {
            Box::pin(async move { Ok(()) })
        });
        
        ac.set_async_event_handler(Some(f));
        ac.take_ownership().await.unwrap();

        let addr = format!("127.0.0.1:{}", port);

        file.close().ok();

        Tor { ac, _child, socks_addr: addr, directory: dir.to_owned(), shutdown: false }
    }
}

impl StreamSocketU for Socks5Stream<TcpStream> {}

#[async_trait::async_trait]
impl Network for Tor {
    async fn connect_stream(&self, addr: &str) -> Result<StreamSocket> {
        let conn = Socks5Stream::connect(self.socks_addr.as_str(), addr).await?;

        Ok(Box::new(conn))
    }

    async fn status(&mut self) -> NetworkStatus {
        if !self.shutdown {
            match self.ac.get_info_unquote("status/circuit-established").await.unwrap().as_str() {
                "1" => NetworkStatus::Ready,
                "0" => NetworkStatus::Starting,
                _ => panic!("Tor sent an unknewn status for status/circuit-established")
            }
        } else {
            NetworkStatus::Shutdown
        }
    }

    async fn stop(&mut self) {
        self.ac.send_signal(TorSignal::Shutdown).await.unwrap();
        self._child.wait().ok();
        self.shutdown = true;
    }

    async fn start(&mut self) {
        if self.shutdown {
            let tor     = Tor::new(&self.directory).await;
            self.ac     = tor.ac;
            self._child = tor._child;
            self.socks_addr = tor.socks_addr;
            self.shutdown   = false;
        }
    }
}
