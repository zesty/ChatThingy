use std::{sync::Arc, ops::Range};

use common::keys::{KeyPair, PublicKey};
use rand::seq::SliceRandom;
use tokio::{sync::{RwLock, mpsc}, task::JoinHandle};
use divide_range::RangeDivisions;
use tracing::info;

use crate::{network::{NetworkType, Network}, client::{Client, blocking_task::MessageBlockingTask, state::ClientState, Activity, JoinStatus, ResumePoint}};


pub struct Context {
    ctx: Arc<InnerContext>
}

pub(crate) struct InnerContext {
    pub net: RwLock<Box<dyn Network + Send + Sync>>,
    pub kp: Arc<KeyPair>,
    pub workers: Vec<Worker>
}

pub(crate) struct Worker {
    pub range: Range<u8>,
    pub handle: mpsc::UnboundedSender<MessageBlockingTask>,
    pub worker: JoinHandle<()>
}

/// Nero Context
impl Context {
    /// Create a new context
    /// Depending on what network user wants
    pub async fn new(kp: Arc<KeyPair>, net_type: NetworkType, dir: &str) -> Context {
        let net: Box<dyn Network + Send + Sync> = match net_type {
            NetworkType::Tor => Box::new(crate::network::tor::Tor::new(dir).await),
            #[cfg(debug_assertions)]
            NetworkType::TestNet => Box::new(crate::network::TestNet {})
        };

        // Spawning workers that will handle
        // the hard work (ie. encryption/description ... etc)
        let mut workers = Vec::new();
        let cpus        = num_cpus::get();
        let mut 
            range_split = (0u8..255u8).divide_evenly_into(cpus);

        for _ in 0..cpus {
            let kp_clone = kp.clone(); 
            let (tx, rx) = mpsc::unbounded_channel();
            workers.push(Worker {
                range: range_split.next().unwrap(),
                worker: tokio::task::spawn_blocking(move || {
                    crate::client::blocking_task::blocking_task(rx, kp_clone);
                }),
                handle: tx
            });
        }

        Context {
            ctx: Arc::new(InnerContext {
                net: RwLock::new(net),
                kp,
                workers
            })
        }
    }

    /// Get keypair
    pub fn keypair(&self) -> &KeyPair {
        &self.ctx.kp
    }

    /// Create a client for a group
    /// Must specify the master server of the group
    /// Client may add other servers to list after discovering them
    /// IMPORTANT!!!
    /// A valid client state must be supplied to persist
    /// ratchet states
    pub fn instance_client(&self, master_server: &str, last: ResumePoint, state: ClientState) -> (Client, mpsc::UnboundedReceiver<Activity>) {
        Client::new(self.ctx.clone(), master_server.to_owned(), last, state)
    }

    /// Join a new group
    pub async fn join_group(&self, master_server: &str) -> JoinStatus {
        Client::join(self.ctx.clone(), master_server.to_owned()).await
    }

    /// Return a reference to the network
    pub fn network(&self) -> &RwLock<Box<dyn Network + Send + Sync>> {
        &self.ctx.net
    }
}

impl Drop for Context {
    fn drop(&mut self) {
        info!("Dropping context background workers");
        for task in self.ctx.workers.as_slice() {
            task.worker.abort();
        }
    }
}

impl InnerContext {
    /// Choose a blocking task in uniform manner for read operation
    /// That means message sent by a user are always read by same thread
    pub fn pick_worker(&self, pk: &PublicKey) -> &Worker {
        // We pick first byte of key
        // As criteria of worker picking
        let crit = pk.first_byte();

        for worker in &self.workers {
            if worker.range.contains(&crit) {
                return worker;
            }
        }

        // Need this because 255u8 is not included
        return self.workers.last()
            .expect("Can't run on a machine with 0 cores");
    }

    pub fn pick_random_worker(&self) -> &Worker {
        let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();
        return self.workers.choose(&mut seed)
            .expect("Can't run on a machine with 0 cores");
    }
}
