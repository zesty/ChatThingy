use anyhow::Result;
use borsh::{BorshDeserialize, BorshSerialize};
use futures::executor::block_on;
use openmls::prelude::*;
use tokio::{sync::oneshot, io::{AsyncReadExt, AsyncWriteExt}};
use tracing::{info, error};
use std::sync::Arc;

use common::{api::{self, RequestType, group::{AddMemberStatus, LeaveGroupStatus}}, keys::{KeyPair, PublicKey}};

use crate::{Client, ClientState, group_identity::{create_identity, get_pk}, MessageActivity, ChannelType};

use super::{
    InnerClient, 
    blocking_task::{MessageBlockingTaskType, MessageBlockingTaskCreateGroup, schedule_blocking_task, MessageBlockingTaskAddMember, GroupMessageBlockingTaskWrite, MessageLeaveGroupBlockingTask},
    message::{send_inner, SendMessage, Message}
};

pub fn blocking_create_group(kp: &KeyPair, state: &ClientState, primary_server: &str) -> Result<()> {
    let key_package = create_identity(kp, state.as_ref())?;

    let group_config: MlsGroupConfig = MlsGroupConfig::builder()
        .padding_size(100)
        .sender_ratchet_configuration(SenderRatchetConfiguration::new(
            100,   // out_of_order_tolerance
            2000, // maximum_forward_distance
        ))
        .use_ratchet_tree_extension(true)
        .build();

    let mut group = MlsGroup::new(
        state.as_ref(),
        &group_config,
        GroupId::from_slice(primary_server.as_bytes()),
        key_package
            .hash_ref(state.crypto_provider())?
            .as_slice(),
    )?;

    let mut buf = Vec::new();
    group.save(&mut buf)?;

    block_on(state.save_group_state(&buf))?;

    Ok(())
}

pub(crate) async fn new_group(inner: Arc<InnerClient>) -> Result<()> {
    let (tx, rx) = oneshot::channel();
    schedule_blocking_task(&inner, &inner.ctx.kp.public_key(),
        MessageBlockingTaskType::CreateGroup(MessageBlockingTaskCreateGroup {
            master_server: inner.master_server.clone(),
            tx
        })
    ).await;

    let mut stream = Client::init_channel(&inner, RequestType::InitGroup).await?;

    rx.await
        .expect("Should be able to get blocking create group task result");
    
    let ret = {
        let size    = stream.read_u32().await?;
        let mut buf = vec![0u8; size as usize];
        stream.read_exact(&mut buf).await?;

        let status = api::group::InitializeStatus::try_from_slice(&buf)?;

        match status {
            api::group::InitializeStatus::Ok => info!("Group successfully created"),
            e => error!("Server group creation failed, reason {:?}:", e)
        }

        Ok(())
    };
    stream.shutdown().await?;

    ret
}

#[derive(Debug)]
pub enum AddMember {
    Ok,
    InvalidKey,
    MemberExists,
    ServerUnreachable,
    Err(String)
}

pub(crate) async fn add_member_server(inner: &InnerClient, new_member_pk: PublicKey) -> Result<AddMemberStatus> {
    let mut stream = Client::init_channel(&inner, RequestType::AddMember).await?;

    let req = api::group::AddMember { public_key: new_member_pk }.try_to_vec()?;

    stream.write_u32(req.len() as u32).await?;
    stream.write_all(&req).await?;

    let size = stream.read_u32().await?;
    let mut buf = vec![0u8; size as usize];
    stream.read_exact(&mut buf).await?;

    let resp = AddMemberStatus::try_from_slice(&buf)?;

    stream.shutdown().await?;

    Ok(resp)
}

pub(crate) async fn add_member(inner: Arc<InnerClient>, key_package: KeyPackage) -> AddMember {
    let pk = match get_pk(key_package.credential()) {
        Ok(v) => v,
        Err(_) => return AddMember::InvalidKey
    };

    // We first send new member notification to server
    let resp = match add_member_server(&inner, pk.clone()).await {
        Ok(v) => v,
        Err(e) => {
            error!("Couldn't add new member: {}", e);
            return AddMember::ServerUnreachable;
        }
    };
    match resp {
        AddMemberStatus::Ok => (),
        AddMemberStatus::MemberExists => return AddMember::MemberExists,
        AddMemberStatus::PermissionDenied => return AddMember::Err("Permission denied".to_string()),
        AddMemberStatus::Error => return AddMember::Err("Error from server".to_string())
    }

    // Now that our request is successfull to server
    // We notify all other members we added a new member
    let (tx, rx) = oneshot::channel();
    let ret = send_inner(&inner, &inner.ctx.kp.public_key(),
        MessageBlockingTaskType::AddMember(MessageBlockingTaskAddMember {
            dest: pk.clone(),
            key_package,
            send_message_tx: inner.msg_sender.clone(),
            tx
        }),
        true,
        rx
    ).await;

    // Aditionally we send our profile update too if there is one
    let inner_clone = inner.clone();
    tokio::spawn(async move {
        match inner_clone.state.clone().get_profile_update().await {
            Ok(Some(profile_update)) => {
                let (tx, rx) = oneshot::channel();
                send_inner(&inner_clone, &inner_clone.ctx.kp.public_key(),
                    MessageBlockingTaskType::GroupWrite(GroupMessageBlockingTaskWrite {
                        msg: crate::types::PlainTextBlob::ProfileUpdate(profile_update),
                        tx
                    }),
                    true,
                    rx
                ).await.ok();
            },
            _ => ()
        }
    });

    match ret {
        Ok(SendMessage::Ok | SendMessage::NotSent) => (),
        Ok(SendMessage::Err(e)) => return AddMember::Err(e.to_string()),
        Err(e) => return AddMember::Err(e.to_string())
    };

    inner.activity.clone().send(super::Activity::User(super::contact::UserActivity::Add(pk.clone()))).ok();

    inner.activity.clone().send(super::Activity::Message(MessageActivity::Text(Message {
        content: crate::types::Message::NewMemberAdded(pk),
        id: 0,
        timestamp: chrono::offset::Utc::now().timestamp() as i64,
        source: inner.ctx.kp.public_key(),
        ch_type: ChannelType::Group
    }))).ok();

    AddMember::Ok
}

#[derive(Debug, Clone, PartialEq)]
pub enum LeaveGroup {
    Ok,
    ServerUnreachable,
    Err(String)
}

pub(crate) async fn leave_server(inner: &InnerClient) -> Result<LeaveGroupStatus> {
    let mut stream = Client::init_channel(&inner, RequestType::LeaveGroup).await?;

    let size = stream.read_u32().await?;
    let mut buf = vec![0u8; size as usize];
    stream.read_exact(&mut buf).await?;

    let resp = LeaveGroupStatus::try_from_slice(&buf)?;

    stream.shutdown().await?;

    Ok(resp)
}

pub(crate) async fn leave(inner: &InnerClient) -> LeaveGroup {
    // We first send a message
    let (tx, rx) = oneshot::channel();
    let ret = send_inner(&inner, &inner.ctx.kp.public_key(),
        MessageBlockingTaskType::LeaveGroup(MessageLeaveGroupBlockingTask {
            tx
        }),
        false,
        rx
    ).await;

    match ret {
        Ok(SendMessage::Ok) => (),
        Ok(_) => return LeaveGroup::ServerUnreachable,
        Err(e) => return LeaveGroup::Err(e.to_string())
    }

    match leave_server(inner).await {
        Ok(LeaveGroupStatus::Ok) => LeaveGroup::Ok,
        Ok(LeaveGroupStatus::Error) => LeaveGroup::ServerUnreachable,
        Err(e) => {
            error!("Leave group failed: {}", e);
            LeaveGroup::ServerUnreachable
        }
    }
}
