use anyhow::Result;
use borsh::{BorshSerialize, BorshDeserialize};
use tokio::io::{AsyncReadExt, AsyncWriteExt};

use crate::{network::StreamSocket, context::InnerContext};

use common::api;

#[allow(non_camel_case_types)]
pub enum HandshakeStatus {
    Ok,
    OK_NEW,
    NOT_MEMBER
}

pub(crate) async fn init(stream: &mut StreamSocket, ctx: &InnerContext) -> Result<HandshakeStatus> {
    // I. We first start by receiving challenge
    let size = stream.read_u32().await?;
    if size > api::handshake::HANDSHAKE_MESSAGE_SIZE_MAX || size == 0 {
        return Err(anyhow::Error::msg("Handshake response message invalid size"));
    }

    let mut buf = vec![0u8; size as usize];
    stream.read_exact(&mut buf).await?;
    let first   = api::handshake::Init::try_from_slice(&buf)?;

    if first.version < api::MIN_SUPPORTED_VER {
        return Err(anyhow::Error::msg(format!("Server has older version ({}) than minimum supported ({})", first.version, api::MIN_SUPPORTED_VER)));
    }


    // II. Now we send challenge sig
    let sig = ctx.kp.sign(&first.challenge);
    
    drop(first);
    drop(buf);

    let msg = api::handshake::Second {
        version: api::VERSION,
        sig,
        pk: ctx.kp.public_key()
    }.try_to_vec()?;
    
    stream.write_u32(msg.len() as u32).await?;
    stream.write_all(&msg).await?;

    // III. Verify we did a successful handshake
    let size = stream.read_u32().await?;
    if size > api::handshake::HANDSHAKE_MESSAGE_SIZE_MAX || size == 0 {
        return Err(anyhow::Error::msg("Handshake response message invalid size"));
    }

    let mut buf = vec![0u8; size as usize];

    stream.read_exact(&mut buf).await?;

    let msg = api::handshake::Third::try_from_slice(&buf)?;

    Ok(match msg.status {
        // Normal access to server
        api::handshake::Status::OK         => HandshakeStatus::Ok,
        // We are admin of the group
        api::handshake::Status::OK_NEW     => HandshakeStatus::OK_NEW,
        api::handshake::Status::NOT_MEMBER => HandshakeStatus::NOT_MEMBER,
        e => return Err(anyhow::Error::msg(format!("Handshake failed, reason: {:?}", e)))
    })
}
