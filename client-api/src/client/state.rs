use anyhow::Result;
use openmls::prelude::OpenMlsCryptoProvider;
use openmls_traits::key_store::{FromKeyStoreValue, OpenMlsKeyStore, ToKeyStoreValue};
use openmls_rust_crypto::RustCrypto;
use zeroize::Zeroizing;
use std::sync::Arc;

use common::keys::PublicKey;

use dratchet::RatchetEncHeader;

use crate::types::ProfileUpdate;

pub type ClientState = Arc<Box<dyn ClientStateTrait + Send + Sync>>;

#[async_trait::async_trait]
pub trait ClientStateTrait {
    /// Get latest state of a ratchet
    async fn get_state(&self, dest: &PublicKey) -> Result<RatchetEncHeader>;
    /// Save state of the ratchet
    async fn save_state(&self, dest: &PublicKey, ratchet: RatchetEncHeader) -> Result<()>;

    /// Load a single ephemeral key
    async fn get_eph_sk(&self, eph_pk: &[u8; 32]) -> Result<Zeroizing<[u8; 32]>>;
    /// Save ephemeral keys for key exchange
    async fn save_eph_sk_list(&self, eph_sk_list: &[([u8; 32], Zeroizing<[u8; 32]>)]) -> Result<()>;


    /// Load group state
    async fn get_group_state(&self) -> Result<Zeroizing<Vec<u8>>>;
    /// Save group state
    async fn save_group_state(&self, state: &[u8]) -> Result<()>;

    fn crypto_provider<'a>(&'a self) -> &'a RustCrypto;

    fn store_group_key(&self, k: &[u8], v: &[u8]) -> Result<()>;
    fn read_group_key(&self, k: &[u8]) -> Result<Vec<u8>>;
    fn delete_group_key(&self, k: &[u8]) -> Result<()>;

    /// Store pending message
    async fn save_pending_message(&self, msg: Vec<u8>) -> Result<()>;
    /// Get next pending message
    async fn get_pending_message(&self) -> Result<(u32, Vec<u8>)>;
    /// Delete pending message by index
    async fn delete_pending_message(&self, index: u32) -> Result<()>;

    /// Get last update height for a user
    async fn last_update_height(&self, pk: &PublicKey) -> Result<u32>;
    /// Get last update height for profile
    /// The result of this method must be uniform
    /// between groups if a synchrnised profile update is wanted
    async fn last_profile_update_height(&self) -> Result<u32>;

    /// Store pending task update
    async fn save_pending_task(&self, task: Vec<u8>) -> Result<()>;
    /// Get next pending task update
    async fn get_pending_task(&self) -> Result<(u32, Vec<u8>)>;
    /// Delete pending task update by index
    async fn delete_pending_task(&self, index: u32) -> Result<()>;

    /// Get current profile update
    async fn get_profile_update(&self) -> Result<Option<ProfileUpdate>>;
    /// Save profile update
    async fn save_profile_update(&self, update: &ProfileUpdate) -> Result<()>;
}

impl OpenMlsCryptoProvider for Box<dyn ClientStateTrait + Send + Sync> {
    type CryptoProvider = RustCrypto;
    type RandProvider = RustCrypto;
    type KeyStoreProvider = Self;

    fn crypto(&self) -> &Self::CryptoProvider {
        self.crypto_provider()
    }

    fn rand(&self) -> &Self::RandProvider {
        self.crypto_provider()
    }

    fn key_store(&self) -> &Self::KeyStoreProvider {
        self
    }
}


impl OpenMlsKeyStore for Box<dyn ClientStateTrait + Send + Sync> {
    type Error = String;

    fn store<V: ToKeyStoreValue>(&self, k: &[u8], v: &V) -> Result<(), Self::Error> {
        let value = match v.to_key_store_value() {
            Ok(v) => v,
            Err(_) => return Err("Couldn't serialize key value".to_string())
        };

        match self.store_group_key(k, &value) {
            Ok(_)  => Ok(()),
            Err(e) => Err(e.to_string())
        }
    }

    fn read<V: FromKeyStoreValue>(&self, k: &[u8]) -> Option<V> {
        match self.read_group_key(k) {
            Ok(v) => V::from_key_store_value(&v).ok(),
            Err(_) => None
        }
    }

    fn delete(&self, k: &[u8]) -> Result<(), Self::Error> {
        match self.delete_group_key(k) {
            Ok(_)  => Ok(()),
            Err(e) => Err(e.to_string())
        }
    }
}
