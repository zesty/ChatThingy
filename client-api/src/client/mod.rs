mod handshake;
pub(crate) mod message;
pub mod state;
pub(crate) mod contact;
mod key_store;
pub(crate) mod group;
pub(crate) mod blocking_task;
pub(crate) mod profile;

use common::{keys::PublicKey, api::{RequestType, ServerRequestType}};
use futures::executor::block_on;
use openmls::prelude::KeyPackage;
use tokio_yamux::{StreamHandle, Control};
use std::{sync::Arc, time::Duration};
use tokio::{sync::{mpsc, oneshot, RwLock}, task::JoinHandle, io::{AsyncWriteExt, AsyncReadExt}};
use tracing::{error, debug, info};
use anyhow::Result;
use futures::prelude::*;


use crate::{context::InnerContext, types::PlainMessage};

use self::{state::ClientState, message::{MessageActivity, SendMessage, SendMessageTask}, contact::{AddContact, UserActivity, ContactActivity}, group::{AddMember, LeaveGroup}, profile::{UpdateTask, ProfileUpdateStatus}};

static CONNECT_TIMEOUT: Duration        = Duration::from_secs(15);
static MSG_NOTIFICATION_RETRY: Duration = Duration::from_secs(2);
static PING_INTERVAL: Duration          = Duration::from_secs(10);

pub struct ResumePoint {
    pub direct: i64,
    pub group: i64
}

pub enum JoinStatus {
    Ok,
    NotInvited,
    ServerUnreachable
}

#[derive(Debug)]
pub enum Activity {
    Message(MessageActivity),
    User(UserActivity),
    Contact(ContactActivity)
}

/// Client implementation of Nero's protocol for an instance
pub struct Client {
    inner: Arc<InnerClient>
}

pub(crate) struct InnerClient {
    ctx: Arc<InnerContext>,
    tx: mpsc::UnboundedSender<NewStream>,
    activity: mpsc::UnboundedSender<Activity>,
    msg_sender: mpsc::UnboundedSender<SendMessageTask>,
    update_sender: mpsc::UnboundedSender<UpdateTask>,
    tasks: RwLock<Vec<JoinHandle<()>>>,
    state: ClientState,
    last_seen: RwLock<i64>,
    //servers: Vec<String>,
    master_server: String
}

enum StreamReturn {
    NotReady,
    Stream(StreamHandle)
}

struct NewStream {
    tx: oneshot::Sender<StreamReturn>
}

impl Clone for Client {
    fn clone(&self) -> Self {
        Client { inner: self.inner.clone() }
    }
}

impl Client {
    /// Initiating a new connection to server
    pub(crate) fn new(ctx: Arc<InnerContext>, master_server: String,
                      last: ResumePoint, state: ClientState) -> (Client, mpsc::UnboundedReceiver<Activity>) {
        let (tx, rx)   = mpsc::unbounded_channel();
        let (tx1, rx1) = mpsc::unbounded_channel();
        let (tx2, rx2) = mpsc::unbounded_channel();
        let (tx3, rx3) = mpsc::unbounded_channel();
        let inner      = Arc::new(InnerClient {
            ctx,
            tx,
            activity: tx1.clone(),
            msg_sender: tx2.clone(),
            update_sender: tx3,
            last_seen: RwLock::new(0),
            tasks: RwLock::new(Vec::new()),
            state,
            //servers: vec![master_server.clone()],
            master_server
        });

        // Initializing connector to server
        let client_clone = inner.clone();
        let init_task    = tokio::spawn(async move {
            Client::start(client_clone, rx).await;
        });

        // Initializing ping task
        let client_clone = inner.clone();
        let ping_task    = tokio::spawn(async move {
            Client::ping(client_clone).await;
        });

        // Initializing reader
        let client_clone = inner.clone();
        let read_task    = tokio::spawn(async move {
            message::read(&client_clone, tx1, last).await;
        });

        // Initializing Writer
        let client_clone = inner.clone();
        let write_task   = tokio::spawn(async move {
            message::write(&client_clone, rx2, tx2).await;
        });

        // Initializing Profile update handler
        let client_clone = inner.clone();
        let update_task  = tokio::spawn(async move {
            profile::handler(&client_clone, rx3).await;
        });

        let mut lock = block_on(inner.tasks.write());
        lock.push(init_task);
        lock.push(ping_task);
        lock.push(read_task);
        lock.push(write_task);
        lock.push(update_task);
        drop(lock);

        (Client { inner }, rx1)
    }

    /// Joining new group
    pub(crate) async fn join(ctx: Arc<InnerContext>, master_server: String) -> JoinStatus {
        let mut conn = match tokio::time::timeout(Duration::from_secs(20), ctx.net.read().await.connect_stream(&master_server)).await {
            Ok(Ok(v))  => v,
            _ => return JoinStatus::ServerUnreachable
        };

        let init = match handshake::init(&mut conn, &ctx).await {
            Ok(v)  => v,
            Err(_) => {
                return JoinStatus::ServerUnreachable;
            }
        };
        
        match init {
            handshake::HandshakeStatus::NOT_MEMBER => JoinStatus::NotInvited,
            _ => JoinStatus::Ok
        }
    }

    /// Get last seend timestamp
    pub async fn last_seen(&self) -> i64 {
        *self.inner.last_seen.read().await
    }

    /// Send a new message
    pub async fn send_message(&self, dest: &PublicKey, msg: PlainMessage) -> SendMessage {
        message::send(&self.inner, Some(dest), msg).await
    }

    /// Send group message
    pub async fn send_group_message(&self, msg: PlainMessage) -> SendMessage {
        message::send(&self.inner, None, msg).await
    }

    /// Add new contact
    pub async fn add_contact(&self, pk: &PublicKey) -> AddContact {
        contact::add(&self.inner, pk).await
    }

    /// Add new member to group
    pub async fn add_member(&self, key_package: KeyPackage) -> AddMember {
        group::add_member(self.inner.clone(), key_package).await
    }

    /// Update profile info
    pub async fn update_profile(&self, name: &str, avatar: Vec<u8>) -> ProfileUpdateStatus {
        profile::update_info(&self.inner, name, avatar).await
    }

    /// Leave group
    /// Client object is no longer usable if [`LeaveGroup::Ok`] is returned
    pub async fn leave_group(&self) -> LeaveGroup {
        let status = group::leave(&self.inner).await;

        // If we leave group
        // We must stop all tasks
        match status {
            LeaveGroup::Ok => {
                for task in self.inner.tasks.read().await.as_slice() {
                    task.abort();
                }
            },
            _ => ()
        }

        status
    }

    /// Start events for instance
    async fn start(inner: Arc<InnerClient>, mut rx: mpsc::UnboundedReceiver<NewStream>) {
        let mut conn_yamux = None as Option<Control>;

        loop {
            let ret = match rx.recv().await {
                Some(v) => v,
                None    => return
            };
            if ret.tx.is_closed() {
                continue;
            }
            
            if let Some(mut yam) = conn_yamux {
                if let Ok(stream) = yam.open_stream().await {
                    ret.tx.send(StreamReturn::Stream(stream)).ok();
                    conn_yamux = Some(yam);
                    continue;
                }
                conn_yamux = None;
            }
            ret.tx.send(StreamReturn::NotReady).ok();
            conn_yamux = match tokio::time::timeout(CONNECT_TIMEOUT, Client::create_connection(inner.clone())).await {
                Ok(v) => v,
                Err(_) => continue
            };
        };
    }

    async fn create_connection(inner: Arc<InnerClient>) -> Option<Control> {
        let mut conn = match inner.ctx.net.read().await.connect_stream(&inner.master_server).await {
            Ok(v)  => v,
            Err(e) => {
                debug!("Couldn't connect to server {}: {}", inner.master_server, e);
                return None
            },
        };

        let init = match handshake::init(&mut conn, &inner.ctx).await {
            Ok(v)  => v,
            Err(e) => {
                error!("Server {}: {}", inner.master_server, e);
                return None;
            }
        };
        
        let mut session = tokio_yamux::Session::new_client(conn, tokio_yamux::config::Config::default());
        let control     = session.control();

        match init {
            handshake::HandshakeStatus::Ok => (),
            handshake::HandshakeStatus::OK_NEW => {
                let inner_clone = inner.clone();
                tokio::spawn(async move {
                    group::new_group(inner_clone).await.ok();
                });
            },
            handshake::HandshakeStatus::NOT_MEMBER => {
                // TODO: WE got kicked!!
            }
        }

        let inner_clone = inner.clone();
        inner.tasks.write().await.push(tokio::spawn(async move {
            loop {
                match session.next().await {
                    Some(Ok(mut stream)) => {
                        let inner_cl = inner_clone.clone();
                        tokio::spawn(async move {
                            Client::handle_server_request(&inner_cl, &mut stream).await.ok();
                        });
                    },
                    Some(Err(_)) => {
                        break;
                    }
                    None => {
                        break;
                    }
                }
            }
        }));

        Some(control)
    }

    async fn handle_server_request(inner: &InnerClient, stream: &mut StreamHandle) -> Result<()> {
        let req_type = std::convert::TryInto::<ServerRequestType>::try_into(stream.read_u8().await?)?;

        match req_type {
            ServerRequestType::RunOutOfEphPkNotification => {
                if let Err(e) = key_store::key_refill(inner, stream).await {
                    error!("Couldn't refill server keystore with ephemeral keys: {}", e);
                }
            },
            ServerRequestType::Unknewn => ()
        }

        Ok(())
    }

    async fn init_channel(inner: &InnerClient, req_type: RequestType) -> Result<StreamHandle> {
        let ret = tokio::time::timeout(CONNECT_TIMEOUT, async move {
            let (tx, rx) = oneshot::channel();

            if inner.tx.clone().send(NewStream { tx }).is_err() {
                return Err(anyhow::Error::msg("Stream channel is closed"));
            }
            
            match rx.await? {
                StreamReturn::NotReady => Err(anyhow::Error::msg("Connection not ready yet")),
                StreamReturn::Stream(mut stream) => {
                    stream.write_u8(req_type as u8).await?;

                    Ok(stream)
                }
            }
        }).await;

        match ret {
            Ok(Ok(v)) => Ok(v),
            Ok(Err(e)) => Err(e),
            Err(e) => Err(anyhow::Error::new(e))
        }
    }

    async fn ping(inner: Arc<InnerClient>) {
        // https://github.com/libp2p/rust-yamux/issues/99
        // Manually is it? :(
        loop {
            match Client::init_channel(&inner, RequestType::Ping).await {
                Ok(mut stream) => {
                    loop {
                        match tokio::time::timeout(PING_INTERVAL, Client::ping_inner(&mut stream)).await {
                            Ok(Ok(_)) => (),
                            _ => {
                                break;
                            }
                        }

                        *inner.last_seen.write().await = chrono::offset::Utc::now().timestamp() as i64;
                        tokio::time::sleep(PING_INTERVAL).await;
                    };
                },
                Err(_) => ()
            }
        };
    }

    async fn ping_inner(stream: &mut StreamHandle) -> Result<()> {
        stream.write_u8(0u8).await?;
        stream.read_u8().await?;

        Ok(())
    }
}

impl Drop for InnerClient {
    fn drop(&mut self) {
        info!("Dropping group {} background workers", self.master_server);
        for task in block_on(self.tasks.read()).as_slice() {
            task.abort();
        }
    }
}
