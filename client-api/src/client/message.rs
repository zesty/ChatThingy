use anyhow::Result;
use borsh::{BorshSerialize, BorshDeserialize};
use dratchet::{RatchetError, RatchetEncHeader};
use futures::executor::block_on;
use openmls::prelude::*;
use tokio::{sync::{mpsc, oneshot}, io::{AsyncWriteExt, AsyncReadExt}};
use tokio_yamux::StreamHandle;
use tracing::{error, warn};

use common::{api, keys::{KeyPair, PublicKey}, api::{message::{MessagePayload, MessageNotification, DirectMessage, GroupMessage}, RequestType, key_store::AddMoreEphPk}, hash::{Digest, Hash, Sha256, HashFn}};
use zeroize::{Zeroize, Zeroizing};

use crate::{types::{PlainMessage, MessageBlob, CipherMessageBlob, NewContactRequestBlob, WelcomeBlob, PlainTextBlob, RemoveMeBlob}, Client, group_identity::get_pk};

use super::{
    state::ClientState, InnerClient, MSG_NOTIFICATION_RETRY,
    blocking_task::{
        MessageBlockingTaskType, MessageBlockingTaskRead,
        MessageBlockingTaskWrite, schedule_blocking_task,
        GroupMessageBlockingTaskWrite
    },
    Activity, contact::UserActivity, profile::UpdateTask, ResumePoint
};

#[derive(Debug)]
pub enum MessageActivity {
    Text(Message)
}

/// Type of channel
#[derive(Debug, Clone, PartialEq)]
pub enum ChannelType {
    Direct,
    Group
}

/// A message we receive
#[derive(Debug)]
pub struct Message {
    /// Content of the message
    pub content: crate::types::Message,
    /// Sender public key
    pub source: PublicKey,
    /// Message id (different between direct and group messages)
    pub id: i64,
    /// Time UTC
    pub timestamp: i64,
    /// Channel type (either direct message or group)
    pub ch_type: ChannelType
}

/// New contact request
#[derive(Debug)]
pub struct ContactRequest {
    /// Public key for key exchange
    pub dh_pk: Vec<u8>,
    /// First ephemeral key for the send chain
    pub ratchet_eph_pk: Vec<u8>,
    /// Ratchet Itself
    pub ratchet: RatchetEncHeader
}

pub(crate) async fn read(inner: &InnerClient, tx: mpsc::UnboundedSender<Activity>, last: ResumePoint) {
    loop {
        if let Ok(mut stream) = Client::init_channel(&inner, RequestType::MessageListener).await {
            // Now we send latest timestamp we stopped at
            let msg_notify = MessageNotification { last_id: last.direct, last_group_id: last.group }.try_to_vec()
                .expect("Should be able to serialize message notification request");

            let mut buf = vec![0u8; api::message::MESSAGE_RAW_SIZE_MAX];
            if stream.write_u32(msg_notify.len() as u32).await.is_ok()
                && stream.write_all(&msg_notify).await.is_ok() {
                // We start reading messages
                while let Ok((msg_size, id)) = read_server(&mut stream, &mut buf).await {
                    match MessagePayload::try_from_slice(&buf[..msg_size]) {
                        Ok(msg) => {
                            let dest = match &msg {
                                MessagePayload::DirectMessage(message) => message.destination.clone(),
                                MessagePayload::GroupMessage(_) => inner.ctx.kp.public_key().clone()
                            };
                            schedule_blocking_task(
                                inner, &dest,
                                MessageBlockingTaskType::Read(MessageBlockingTaskRead {
                                    id,
                                    msg,
                                    send_message_tx: inner.msg_sender.clone(),
                                    profile_update_tx: inner.update_sender.clone(),
                                    tx: tx.clone()
                                })
                            ).await;
                        },
                        Err(e)  => {
                            // Can't deserialize message, it's lost forever
                            // Hope it's nothing important
                            error!("Couldn't serialize message from server: {}", e);
                        }
                    }
                }
            }
        }
        
        tokio::time::sleep(MSG_NOTIFICATION_RETRY).await;
    }
}

async fn read_server(stream: &mut StreamHandle, buf: &mut [u8]) -> Result<(usize, i64)> {
    let id   = stream.read_i64().await?;
    let size = stream.read_u32().await? as usize;
    
    if size > api::message::MESSAGE_RAW_SIZE_MAX {
        return Err(anyhow::Error::msg("Message size is bigger than MESSAGE_RAW_SIZE_MAX"));
    }

    stream.read_exact(&mut buf[..size]).await?;

    Ok((size, id))
}

pub(crate) fn blocking_gen_keys(kp: &KeyPair, state: ClientState, len: usize) -> Result<Vec<u8>> {
    let mut pk_sig = Vec::with_capacity(len);
    let mut pk_sec = Vec::with_capacity(len);

    for _ in 0..len {
        let dh_kp = crate::key_exchange::gen_dh_kp();
        
        pk_sig.push((dh_kp.1.as_bytes().to_vec(), kp.sign(dh_kp.1.as_bytes())));
        pk_sec.push((dh_kp.1.to_bytes(), Zeroizing::new(dh_kp.0.to_bytes())));
    }

    block_on(state.save_eph_sk_list(&pk_sec)).ok();

    Ok(AddMoreEphPk { list: pk_sig }.try_to_vec()?)
}

fn blocking_group_read(kp: &KeyPair, state: ClientState, msg_bin: &GroupMessage, task: &MessageBlockingTaskRead) -> Result<()> {
    let group = block_on(state.get_group_state())?;
    let mut
        group = MlsGroup::load(group.as_slice())?;

    let msg_ser = MlsMessageIn::try_from_bytes(&msg_bin.message)?;
    let msg     = group.parse_message(msg_ser, state.as_ref())?;
    let pk      = match msg.sender() {
        Sender::Member(hash_ref) => {
            if let Some(key_package) = group.member(hash_ref) {
                get_pk(key_package.credential())?
            } else {
                return Err(anyhow::Error::msg("Couldn't get sender's public key from group message"));
            }
        },
        _ => {
            return Err(anyhow::Error::msg("Couldn't get sender's public key: sender should be a member"));
        }
    };

    let msg     = group.process_unverified_message(msg, None, state.as_ref())?;
    let plain   = match msg {
        ProcessedMessage::ApplicationMessage(v) => PlainTextBlob::try_from_slice(&v.into_bytes())?,
        ProcessedMessage::StagedCommitMessage(v) => {
            // We have a new member
            // TODO: DO more checks?
            let mut creds = Vec::new();
            for i in v.add_proposals() {
                creds.push(i.add_proposal().key_package().credential().clone());
            }

            let mut rem_hashs = Vec::new();
            for i in v.remove_proposals() {
                rem_hashs.push(i.remove_proposal().removed().clone());
            }

            for hash in &rem_hashs {
                if let Some(key_package) = group.member(hash) {
                    let user_pk = get_pk(key_package.credential())?;
                    task.tx.send(super::Activity::Message(MessageActivity::Text(Message {
                        content: crate::types::Message::ContactLeftGroup,
                        id: task.id,
                        timestamp: msg_bin.timestamp,
                        source: user_pk,
                        ch_type: ChannelType::Group
                    }))).ok();
                }
            }

            group.merge_staged_commit(*v)?;

            let my_pk = kp.public_key();
            for cred in creds {
                let add_pk = match get_pk(&cred) {
                    Ok(v) => v,
                    Err(e) => {
                        error!("Can't add contact from server group {}", e);
                        continue;
                    }
                };
                // Skipping ourselves
                if my_pk.eq(&add_pk) {
                    continue;
                }
                task.tx.send(Activity::User(UserActivity::Add(add_pk.clone()))).ok();

                task.tx.send(super::Activity::Message(MessageActivity::Text(Message {
                    content: crate::types::Message::NewMemberAdded(add_pk),
                    id: task.id,
                    timestamp: msg_bin.timestamp,
                    source: pk.clone(),
                    ch_type: ChannelType::Group
                }))).ok();
            }

            save_group_state(&state, &mut group);
            
            match block_on(state.get_profile_update()) {
                Ok(Some(profile_update)) => {
                    match blocking_write(kp, state, BlockingWrite::GroupMessage(PlainTextBlob::ProfileUpdate(profile_update)), kp.public_key()) {
                        Ok(v) => {
                            let (tx, _) = oneshot::channel();
                            task.send_message_tx.send(SendMessageTask { msg: v, resp: tx, store: true }).ok();
                        },
                        Err(_) => ()
                    }
                },
                _ => ()
            }
            return Ok(());
        },
        ProcessedMessage::ProposalMessage(_) => return Ok(())
    };

    match plain {
        PlainTextBlob::Message(plain) => {
            task.tx.send(Activity::Message(MessageActivity::Text(Message { id: task.id, content: crate::types::Message::Plain(plain), source: pk, timestamp: msg_bin.timestamp, ch_type: ChannelType::Group })))
                .expect("Should be able to send message");
        }
        update @ _ => {
            task.profile_update_tx.send(UpdateTask { update, pk }).ok();
        }
    }

    save_group_state(&state, &mut group);

    Ok(())
}

pub(crate) fn blocking_read(kp: &KeyPair, state: ClientState, task: &MessageBlockingTaskRead) -> Result<()> {
    let message = match &task.msg {
        MessagePayload::DirectMessage(m) => m,
        MessagePayload::GroupMessage(m) => return blocking_group_read(kp, state, m, task)
    };

    // We start if message is destined to us
    if !kp.public_key().eq(&message.destination) {
        // Wrong destination
        return Ok(())
    }

    let mut hash = HashFn::new(&message.hash);
    hash.digest(&message.message);
    hash.digest(&message.destination.try_to_vec()?);
    hash.digest(&message.timestamp.to_be_bytes());

    if !hash.compare() {
        // Something is wrong with hash
        return Ok(());
    }

    // We verify signature
    match message.source.verify_sig(message.hash.get_inner_ref(), &message.sig) {
        Ok(true)           => (),
        Ok(false) | Err(_) => return Ok(())
    }

    let payload = match MessageBlob::try_from_slice(&message.message) {
        Ok(v) => v,
        Err(_) => {
            error!("Error deserializing incoming message from {}", message.source);
            return Ok(());
        }
    };

    match payload {
        MessageBlob::Message(msg) => {
            let ratchet = match block_on(state.get_state(&message.source)) {
                Ok(v)  => v,
                Err(e) => {
                    error!("Got message from unknewn contact {}: {}", message.source, e);
                    return Ok(());
                }
            };

            read_msg_cipher(ratchet, state, msg, &message.source, message.timestamp, task);
        },
        MessageBlob::NewContactRequest(msg) => {
            if msg.gen_dh_pk.len() != 32 {
                error!("Got wrong ephemeral key from {}", message.source);
                return Ok(());
            }
            let eph_pk: [u8; 32] = msg.gen_dh_pk.try_into().expect("Should be able to cast eph key to array");
            let eph_sk           = match block_on(state.get_eph_sk(&eph_pk)) {
                Ok(v) => v,
                Err(e) => {
                    error!("Couldn't fetch secret ephemeral key: {}", e);
                    return Ok(());
                }
            };

            let shared_secret = match crate::key_exchange::kdf_rk_hk(eph_sk.as_slice(), &msg.eph_dh_pk) {
                Ok(v) => v,
                Err(e) => {
                    error!("{}", e);
                    return Ok(());
                }
            };
            
            let bob_pk: [u8; 32] = match msg.ratchet_eph_pk.try_into() {
                Ok(v)  => v,
                Err(_) => {
                    error!("New contact response sent from {} has wrong ratchet eph pk size", message.source);
                    return Ok(());
                }
            };
            let ratchet = RatchetEncHeader::init_alice(shared_secret.0, x25519_dalek::PublicKey::from(bob_pk), shared_secret.1, shared_secret.2);

            if let Err(e) = block_on(state.save_state(&message.source, ratchet)) {
                error!("Couldn't save ratchet for {}: {}", message.source, e);
            }

            task.tx.send(super::Activity::Contact(super::contact::ContactActivity::Add(message.source.clone(), false))).ok();
            task.tx.send(super::Activity::Message(MessageActivity::Text(Message {
                content: crate::types::Message::NewPendingContactReceiver,
                id: task.id,
                timestamp: message.timestamp,
                source: message.source.clone(),
                ch_type: ChannelType::Direct
            }))).ok();
        },
        MessageBlob::Welcome(welcome) => {
            let welcome = match Welcome::tls_deserialize(&mut welcome.welcome.as_slice()) {
                Ok(v) => v,
                Err(_) => {
                    error!("Received invalid welcome message from {}", message.source);
                    return Ok(());
                }
            };
            
            // Checking if we are already member in group
            match block_on(state.get_group_state()) {
                Ok(_)  => {
                    warn!("Got a welcome message from {} when we are already in group", message.source);
                    return Ok(());
                },
                Err(_) => (),
            };

            let mut group = match MlsGroup::new_from_welcome(
                state.as_ref(),
                &MlsGroupConfig::default(),
                welcome,
                // we are using [`RatchetTreeExtension`]
                None
             ) {
                Ok(v) => v,
                Err(e) => {
                    error!("Couldn't join group: {}", e);
                    return Ok(());
                }
             };
             
            save_group_members(&kp, &mut group, task.tx.clone());
            save_group_state(&state, &mut group);
        },
        MessageBlob::RemoveMe(remove) => {
            let group = block_on(state.get_group_state())?;
            let mut
                group = MlsGroup::load(group.as_slice())?;

            let msg_ser = MlsMessageIn::try_from_bytes(&remove.proposal)?;
            let msg     = group.parse_message(msg_ser, state.as_ref())?;
            let pk      = match msg.sender() {
                Sender::Member(hash_ref) => {
                    if let Some(key_package) = group.member(hash_ref) {
                        get_pk(key_package.credential())?
                    } else {
                        return Err(anyhow::Error::msg("Couldn't get sender's public key from group message"));
                    }
                },
                _ => {
                    return Err(anyhow::Error::msg("Couldn't get sender's public key: sender should be a member"));
                }
            };

            if !pk.eq(&message.source) {
                return Err(anyhow::Error::msg(format!("Contact {} is sending forged remove proposal", message.source)));
            }

            let msg = group.process_unverified_message(msg, None, state.as_ref())?;
            match msg {
                ProcessedMessage::ProposalMessage(prop) => {
                    let mut valid = None;
                    match prop.proposal() {
                        Proposal::Remove(rem) => {
                            if let Some(member) = group.member(rem.removed()) {
                                if let Ok(pk1) = get_pk(member.credential()) {
                                    if pk1.eq(&pk) {
                                        valid = Some(rem.removed());
                                    }
                                }
                            }
                        },
                        _ => ()
                    }
                    if let Some(refer) = valid {
                        let (msg, _) = group.remove_members(state.as_ref(), &[refer.to_owned()])?;
                        group.merge_pending_commit()?;

                        // TODO: Add verification
                        let (tx, _) = oneshot::channel();

                        task.send_message_tx.send(SendMessageTask {
                            msg: MessagePayload::GroupMessage(api::message::GroupMessage { message: msg.tls_serialize_detached()?, timestamp: 0 }).try_to_vec()?,
                            resp: tx,
                            store: true
                        }).ok();

                        save_group_state(&state, &mut group);

                        task.tx.send(super::Activity::Message(MessageActivity::Text(Message {
                            content: crate::types::Message::ContactLeftGroup,
                            id: task.id,
                            timestamp: message.timestamp,
                            source: pk,
                            ch_type: ChannelType::Group
                        }))).ok();
                    }
                },
                _ => return Err(anyhow::Error::msg(format!("Contact {} is sending RemoveMe with invalid ProcessedMessage", message.source)))
            }
        }
    }

    Ok(())
}

fn save_group_members(kp: &KeyPair, group: &mut MlsGroup, tx: mpsc::UnboundedSender<Activity>) {
    let my_pk = kp.public_key();
    for member in group.members() {
        let pk = match get_pk(member.credential()) {
            Ok(v) => v,
            Err(e) => {
                error!("Can't add contact from server group {}", e);
                continue;
            }
        };
        // Skipping ourselves
        if my_pk.eq(&pk) {
            continue;
        }
        tx.send(Activity::User(UserActivity::Add(pk))).ok();
    }
}

fn save_group_state(state: &ClientState, group: &mut MlsGroup) {
    let mut group_bin = Vec::new();
    if let Err(e) = group.save(&mut group_bin) {
        error!("Couldn't save group state: {}", e);
        group_bin.zeroize();
        return;
    }
    block_on(state.save_group_state(&group_bin)).ok();
    group_bin.zeroize();
}

fn read_msg_cipher(mut ratchet: RatchetEncHeader, state: ClientState, msg: CipherMessageBlob,
                   source: &PublicKey, timestamp: i64, task: &MessageBlockingTaskRead) {
    let enc_header = (msg.header, msg.header_nonce);
    let ret        = ratchet.ratchet_decrypt(&enc_header, &msg.msg, &msg.nonce, &msg.ad);

    let message = Message {
        content: match ret {
            Ok(v) => {
                if let Err(e) = block_on(state.save_state(source, ratchet)) {
                    error!("Couldn't save ratchet for {}: {}", source, e);
                }

                match PlainTextBlob::try_from_slice(&v) {
                    Ok(v) => match v {
                        PlainTextBlob::Message(msg) => crate::types::Message::Plain(msg),
                        update @ _ => {
                            task.profile_update_tx.send(UpdateTask { update, pk: source.clone() }).ok();
                            return;
                        }
                    },
                    Err(e) => {
                        error!("Error serializing message: {:?}", e);
                        crate::types::Message::DecryptionError
                    }
                }
            },
            Err(e) => {
                error!("Error decrypting message: {:?}", e);
                return
            }
        },
        id: task.id,
        timestamp,
        source: source.clone(),
        ch_type: ChannelType::Direct
    };

    task.tx.send(Activity::Message(MessageActivity::Text(message)))
        .expect("Should be able to send message");
}

pub(crate) enum BlockingWrite {
    Message(PlainTextBlob),
    ContactRequest(Vec<u8>),
    AddMember(KeyPackage, mpsc::UnboundedSender<SendMessageTask>),
    GroupMessage(PlainTextBlob),
    LeaveGroup
}

pub(crate) fn blocking_write(kp: &KeyPair, state: ClientState, msg_bin: BlockingWrite, mut dest: PublicKey) -> Result<Vec<u8>> {

    let blob = match msg_bin {
        BlockingWrite::Message(msg) => {
            let msg_bin     = msg.try_to_vec()?;
            let mut ratchet = match block_on(state.get_state(&dest)) {
                Ok(v) => v,
                Err(e) => return Err(e)
            };

             match ratchet.ratchet_encrypt(&msg_bin, b"") {
                Ok(v) => {
                    let ret = MessageBlob::Message(CipherMessageBlob {
                        header_nonce: v.0.1,
                        header: v.0.0,
                        nonce: v.2,
                        msg: v.1,
                        ad: b"".to_vec()
                    }).try_to_vec()?;

                    match block_on(state.save_state(&dest, ratchet)) {
                        Ok(_) => (),
                        Err(e) => return Err(e)
                    }

                    ret
                },
                Err(e) => {
                    match e {
                        RatchetError::FirstReplyFromPeerNeeded => {
                            return Err(anyhow::Error::msg("You can't send message before contact sends a message"));
                        },
                        e => {
                            return Err(anyhow::Error::msg(format!("Ratchet error with destination {:?}: {:?}", dest, e)));
                        }
                    }
                }
            }
        },
        BlockingWrite::ContactRequest(eph_dh_pk) => {
            let dh_kp         = crate::key_exchange::gen_dh_kp();
            let mut dh_secret = dh_kp.0.to_bytes();
            let shared_secret = crate::key_exchange::kdf_rk_hk(&dh_secret, &eph_dh_pk)?;
            dh_secret.zeroize();

            let (ratchet, pk) = RatchetEncHeader::init_bob(shared_secret.0, shared_secret.1, shared_secret.2);

            if let Err(e) = block_on(state.save_state(&dest, ratchet)) {
                error!("Couldn't save ratchet for {}: {}", dest, e);
            }

            MessageBlob::NewContactRequest(NewContactRequestBlob {
                eph_dh_pk: dh_kp.1.as_bytes().to_vec(),
                gen_dh_pk: eph_dh_pk,
                ratchet_eph_pk: pk.as_bytes().to_vec()
            }).try_to_vec()?
        },
        BlockingWrite::AddMember(key_package, staged_commit_tx) => {
            let group = block_on(state.get_group_state())?;
            let mut
                group = MlsGroup::load(group.as_slice())?;

            let (msg_out, welcome) = group.add_members(state.as_ref(), &[key_package])?;

            // TODO: Can we eliminate this oneshot
            let (tx, _) = oneshot::channel();
            staged_commit_tx.send(SendMessageTask {
                msg: MessagePayload::GroupMessage(GroupMessage { message: msg_out.tls_serialize_detached()?, timestamp: 0 }).try_to_vec()?,
                resp: tx,
                store: true
            })?;

            let mut welcome_bin = Vec::new();
            welcome.tls_serialize(&mut welcome_bin)?;

            let ret = MessageBlob::Welcome(WelcomeBlob { welcome: welcome_bin }).try_to_vec()?;

            group.merge_pending_commit()?;
            save_group_state(&state, &mut group);

            ret
        },
        BlockingWrite::GroupMessage(msg) => {
            let msg   = msg.try_to_vec()?;

            let group = block_on(state.get_group_state())?;
            let mut
                group = MlsGroup::load(group.as_slice())?;
            let msg   = group.create_message(state.as_ref(), &msg)?;

            save_group_state(&state, &mut group);

            let mut msg_bin = Vec::new();
            msg.tls_serialize(&mut msg_bin)?;

            return Ok(MessagePayload::GroupMessage(api::message::GroupMessage { message: msg_bin, timestamp: 0 }).try_to_vec()?)
        },
        BlockingWrite::LeaveGroup => {
            let group = block_on(state.get_group_state())?;
            let mut
                group = MlsGroup::load(group.as_slice())?;


            let membs = group.members();
            // TODO: Check if we are alone
            dest      = get_pk(membs[0].credential())?;

            let msg   = group.leave_group(state.as_ref())?;

            let mut msg_bin = Vec::new();
            msg.tls_serialize(&mut msg_bin)?;

            MessageBlob::RemoveMe(RemoveMeBlob { proposal: msg_bin }).try_to_vec()?
        }
    };

    let timestamp = chrono::offset::Utc::now().timestamp() as i64;

    let mut hashfn = Sha256::new();
    hashfn.input(&blob);
    hashfn.input(&dest.try_to_vec()?);
    hashfn.input(&timestamp.to_be_bytes());

    let mut hash = [0u8; 32];
    hashfn.result(&mut hash);

    let ret = MessagePayload::DirectMessage(DirectMessage {
        source: kp.public_key(),
        destination: dest.clone(),
        timestamp,
        sig: kp.sign(&hash),
        hash: Hash::Sha256(hash),
        message: blob
    }).try_to_vec()?;

    Ok(ret)
}

pub async fn write_to_server(stream: &mut StreamHandle, msg: &[u8]) -> Result<SendMessage> {
    stream.write_u32(msg.len() as u32).await?;
    stream.write_all(msg).await?;

    let len     = stream.read_u32().await?;
    let mut buf = vec![0u8; len as usize];
    stream.read_exact(&mut buf).await?;
    let resp    = api::message::SendMessageResponse::try_from_slice(&buf)?;

    if resp.status == api::status::Status::Ok {
        Ok(SendMessage::Ok)
    } else {
        Ok(SendMessage::Err(format!("Error response from server: {:?}", resp.status)))
    }
}

#[derive(Debug, Clone)]
pub enum SendMessage {
    Ok,
    NotSent,
    Err(String)
}

pub(crate) async fn send(inner: &InnerClient, dest: Option<&PublicKey>, msg: PlainMessage) -> SendMessage {
    let (tx, rx)    = oneshot::channel();
    let (dest, msg) = match dest {
        Some(dest) => (dest.clone(), MessageBlockingTaskType::Write(MessageBlockingTaskWrite {
            dest: dest.clone(),
            msg: PlainTextBlob::Message(msg),
            tx
        })),
        None => (inner.ctx.kp.public_key(), MessageBlockingTaskType::GroupWrite(GroupMessageBlockingTaskWrite {
            msg: PlainTextBlob::Message(msg),
            tx
        }))
    };

    let ret = send_inner(inner, &dest, msg, true, rx).await;

    match ret {
        Ok(ret) => ret,
        Err(e) => {
            error!("{}", e);
            SendMessage::Err(e.to_string())
        }
    }
}

pub(crate) async fn send_inner(inner: &InnerClient, dest: &PublicKey, msg: MessageBlockingTaskType,
                               store: bool, rx: oneshot::Receiver<Result<Vec<u8>>>) -> Result<SendMessage> {
    schedule_blocking_task(inner, dest, msg).await;

    match rx.await? {
        Ok(msg) => {
            let (tx, rx) = oneshot::channel();
            inner.msg_sender.clone().send(SendMessageTask { msg, resp: tx, store }).ok();

            let resp = rx.await?;

            Ok(resp)
        },
        Err(e) => {
            error!("{}", e);
            Ok(SendMessage::Err(e.to_string()))
        }
    }
}

#[derive(Debug)]
pub(crate) struct SendMessageTask {
    msg: Vec<u8>,
    resp: oneshot::Sender<SendMessage>,
    store: bool
}

pub(crate) async fn write(inner: &InnerClient, mut rx: mpsc::UnboundedReceiver<SendMessageTask>, tx: mpsc::UnboundedSender<SendMessageTask>) {
    // We first spawn a task to write pending messages
    let state = inner.state.clone();
    inner.tasks.write().await.push(tokio::spawn(async move {
        loop {
            while let Ok((index, msg)) = state.get_pending_message().await {
                let (tx1, rx) = oneshot::channel();
                tx.send(SendMessageTask { msg, resp: tx1, store: false }).ok();
                match rx.await {
                    Ok(SendMessage::Ok | SendMessage::Err(_)) => {
                        if let Err(e) = state.delete_pending_message(index).await {
                            error!("Error deleting pending message with index {}: {}", index, e);
                        }
                    },
                    _ => {
                        // Anything can happen here
                        // we just retry hoping the error disappears
                        tokio::time::sleep(MSG_NOTIFICATION_RETRY).await;
                    }
                };
            }
            tokio::time::sleep(MSG_NOTIFICATION_RETRY).await;
        };
    }));

    let mut stream = Client::init_channel(&inner, RequestType::SendMessage).await.ok();
    loop {
        let task = match rx.recv().await {
            Some(v) => v,
            None    => return
        };
        match stream.take() {
            Some(mut s) => {
                match write_to_server(&mut s, &task.msg).await {
                    Ok(v) => {
                        task.resp.send(v).ok();
                        stream = Some(s);
                    },
                    Err(_) => {
                        task.resp.send(SendMessage::NotSent).ok();
                        if task.store {
                            if let Err(e) = inner.state.save_pending_message(task.msg).await {
                                // Something terribly happened
                                error!("Can't save pending message: {}", e);
                            }
                        }
                        stream = Client::init_channel(&inner, RequestType::SendMessage).await.ok();
                    }
                }
            },
            None => {
                // We are not ready yet
                // We shall save this
                if task.store {
                    if let Err(e) = inner.state.save_pending_message(task.msg).await {
                        // Something terribly happened
                        // TODO: Find a way to show this?
                        error!("Can't save pending message: {}", e);
                    }
                }
                task.resp.send(SendMessage::NotSent).ok();
                stream = Client::init_channel(&inner, RequestType::SendMessage).await.ok();
            }
        }
    };
}
