use borsh::{BorshSerialize, BorshDeserialize};
use tokio::{sync::oneshot, io::{AsyncWriteExt, AsyncReadExt}};
use tokio_yamux::StreamHandle;
use tracing::error;
use anyhow::Result;

use common::{
    keys::PublicKey, api::{self, RequestType}
};

use crate::{Client, types::ProfileInfoBlob, ChannelType, MessageActivity};

use super::{InnerClient, message::{send_inner, Message}, blocking_task::{MessageBlockingTaskType, MessageBlockingTaskContactRequest}};

#[derive(Debug)]
pub enum ContactActivity {
    Add(PublicKey, bool),
    Remove
}

#[derive(Debug)]
pub enum UserActivity {
    Add(PublicKey),
    Update(PublicKey, u32, ProfileInfoBlob),
    Remove
}

#[derive(Debug)]
pub enum AddContact {
    Ok,
    ServerUnreachable,
    InvalidKey,
    ContactExists,
    NotInServer,
    TryLater,
    Err(String)
}

async fn get_eph_pk(stream: &mut StreamHandle, pk: &PublicKey) -> Result<api::key_store::GetEphPkResponse> {
    let req = api::key_store::GetEphPkRequest {
        user: pk.clone()
    }.try_to_vec()?;

    stream.write_u32(req.len() as u32).await?;
    stream.write_all(&req).await?;

    let size     = stream.read_u32().await?;
    let mut resp = vec![0u8; size as usize];
    stream.read_exact(&mut resp).await?;
    
    Ok(api::key_store::GetEphPkResponse::try_from_slice(&resp)?)
}

pub(crate) async fn add(inner: &InnerClient, dest: &PublicKey) -> AddContact {
    if inner.ctx.kp.public_key().eq(dest) {
        return AddContact::InvalidKey;
    }
    // We first start by retrieving an ephemaral public key for key exchange from server
    // of our contact we want to add
    let mut stream;

    stream = match Client::init_channel(&inner, RequestType::GetEphPk).await {
        Ok(v) => v,
        Err(_) => return AddContact::ServerUnreachable,
    };

    let ret = {
        let eph_dh_pk = match get_eph_pk(&mut stream, dest).await {
            Ok(api::key_store::GetEphPkResponse::Ok(key, sig)) => {
                match dest.verify_sig(&key, &sig) {
                    Ok(true) => key,
                    Ok(false) | Err(_) => return AddContact::Err("Invalid key signature sent from server".to_string())
                }
            },
            Ok(api::key_store::GetEphPkResponse::NotInServer) => return AddContact::NotInServer,
            Ok(api::key_store::GetEphPkResponse::OutOfKeys) => return AddContact::TryLater,
            Err(_) => return AddContact::ServerUnreachable,
        };

        if eph_dh_pk.len() != 32 {
            error!("Got wrong key size for key exchange with {}", dest);
            return AddContact::Err("Invalid ephemeral key sent by server".to_string());
        }

        let (tx, rx) = oneshot::channel();
        let ret = send_inner(inner, dest,
            MessageBlockingTaskType::ContactRequest(MessageBlockingTaskContactRequest {
                dest: dest.clone(),
                eph_dh_pk,
                tx
            }),
            true,
            rx
        ).await;

        match ret {
            Ok(super::message::SendMessage::Ok | super::message::SendMessage::NotSent) => {
                AddContact::Ok
            },
            Ok(super::message::SendMessage::Err(e)) => {
                error!("{}", e);
                AddContact::Err(e.to_string())
            }
            Err(e) => {
                error!("{}", e);
                AddContact::Err(e.to_string())
            }
        }
    };

    let tx = inner.activity.clone();
    tx.send(super::Activity::Contact(super::contact::ContactActivity::Add(dest.clone(), true))).ok();
    tx.send(super::Activity::Message(MessageActivity::Text(Message {
        content: crate::types::Message::NewPendingContactSource,
        id: -1,
        timestamp: chrono::offset::Utc::now().timestamp() as i64,
        source: dest.clone(),
        ch_type: ChannelType::Direct
    }))).ok();

    stream.shutdown().await.ok();

    ret
}
