use std::sync::Arc;
use anyhow::Result;
use common::{api::message::MessagePayload, keys::{PublicKey, KeyPair}};
use openmls::prelude::KeyPackage;
use rand::{rngs::OsRng, RngCore};
use tokio::sync::{mpsc, oneshot};
use tracing::{info, error};
use zeroize::Zeroizing;
use chacha20poly1305::{XChaCha20Poly1305, XNonce};
use chacha20poly1305::aead::{Aead, KeyInit};

use crate::{ClientState, types::PlainTextBlob};

use super::{message::{BlockingWrite, SendMessageTask}, Activity, InnerClient, profile::UpdateTask};

pub(crate) struct MessageBlockingTask {
    ratchet: ClientState,
    msg_type: MessageBlockingTaskType
}

pub(crate) enum MessageBlockingTaskType {
    Read(MessageBlockingTaskRead),
    Write(MessageBlockingTaskWrite),
    GroupWrite(GroupMessageBlockingTaskWrite),
    ContactRequest(MessageBlockingTaskContactRequest),
    EphKeysGen(MessageBlockingTaskEphKeyGen),
    CreateGroup(MessageBlockingTaskCreateGroup),
    AddMember(MessageBlockingTaskAddMember),
    LeaveGroup(MessageLeaveGroupBlockingTask),
    Encrypt(BlockingTaskEncrypt),
    Decrypt(BlockingTaskDecrypt),
}

pub(crate) struct BlockingTaskDecrypt {
    pub msg: Vec<u8>,
    pub secret: Zeroizing<[u8; 32]>,
    pub nonce: [u8; 24],
    pub tx: oneshot::Sender<Result<Vec<u8>>>
}

pub(crate) struct BlockingTaskEncrypt {
    pub msg: Vec<u8>,
    pub secret: Zeroizing<[u8; 32]>,
    pub tx: oneshot::Sender<Result<([u8; 24], Vec<u8>)>>
}

pub(crate) struct MessageLeaveGroupBlockingTask {
    pub tx: oneshot::Sender<Result<Vec<u8>>>
}

pub(crate) struct GroupMessageBlockingTaskWrite {
    pub msg: PlainTextBlob,
    pub tx: oneshot::Sender<Result<Vec<u8>>>
}

pub(crate) struct MessageBlockingTaskCreateGroup {
    pub master_server: String,
    pub tx: oneshot::Sender<()>
}

pub(crate) struct MessageBlockingTaskRead {
    pub id: i64,
    pub msg: MessagePayload,
    pub send_message_tx: mpsc::UnboundedSender<SendMessageTask>,
    pub profile_update_tx: mpsc::UnboundedSender<UpdateTask>,
    pub tx: mpsc::UnboundedSender<Activity>
}

pub(crate) struct MessageBlockingTaskWrite {
    pub dest: PublicKey,
    pub msg: PlainTextBlob,
    pub tx: oneshot::Sender<Result<Vec<u8>>>
}

pub(crate) struct MessageBlockingTaskContactRequest {
    pub dest: PublicKey,
    pub eph_dh_pk: Vec<u8>,
    pub tx: oneshot::Sender<Result<Vec<u8>>>
}

pub(crate) struct MessageBlockingTaskEphKeyGen {
    pub len: usize,
    pub tx: oneshot::Sender<Result<Vec<u8>>>
}

pub(crate) struct MessageBlockingTaskAddMember {
    pub dest: PublicKey,
    pub key_package: KeyPackage,
    pub send_message_tx: mpsc::UnboundedSender<SendMessageTask>,
    pub tx: oneshot::Sender<Result<Vec<u8>>>
}

pub(crate) fn blocking_task(mut rx: mpsc::UnboundedReceiver<MessageBlockingTask>, kp: Arc<KeyPair>) {
    loop {
        let ret = match rx.blocking_recv() {
            Some(v) => v,
            None => {
                info!("Blocking worker task exited");
                return;
            }
        };

        match ret.msg_type {
            MessageBlockingTaskType::Read(task) => {
                if let Err(e) = super::message::blocking_read(&kp, ret.ratchet, &task) {
                    error!("Reading message failed: {}", e);
                }
            },
            MessageBlockingTaskType::Write(task) => {
                let ret = super::message::blocking_write(&kp, ret.ratchet, BlockingWrite::Message(task.msg), task.dest);
                task.tx.send(ret).ok();
            },
            MessageBlockingTaskType::ContactRequest(task) => {
                let ret = super::message::blocking_write(&kp, ret.ratchet, BlockingWrite::ContactRequest(task.eph_dh_pk), task.dest);
                task.tx.send(ret).ok();
            },
            MessageBlockingTaskType::EphKeysGen(task) => {
                let ret = super::message::blocking_gen_keys(&kp, ret.ratchet, task.len);
                task.tx.send(ret).ok();
            }
            MessageBlockingTaskType::CreateGroup(task) => {
                task.tx.send(match super::group::blocking_create_group(&kp, &ret.ratchet, &task.master_server) {
                    Ok(_)  => (),
                    Err(e) => {
                        error!("Couldn't create group: {}", e);
                    }
                }).ok();
            },
            MessageBlockingTaskType::AddMember(task) => {
                let ret = super::message::blocking_write(&kp, ret.ratchet, BlockingWrite::AddMember(task.key_package, task.send_message_tx), task.dest);
                task.tx.send(ret).ok();
            },
            MessageBlockingTaskType::LeaveGroup(task) => {
                let ret = super::message::blocking_write(&kp, ret.ratchet, BlockingWrite::LeaveGroup, kp.public_key());
                task.tx.send(ret).ok();
            },
            MessageBlockingTaskType::GroupWrite(task) => {
                let ret = super::message::blocking_write(&kp, ret.ratchet, BlockingWrite::GroupMessage(task.msg), kp.public_key());
                task.tx.send(ret).ok();
            },
            MessageBlockingTaskType::Encrypt(task) => {
                let ret = encrypt(&task);
                task.tx.send(ret).ok();
            },
            MessageBlockingTaskType::Decrypt(task) => {
                let ret = decrypt(&task);
                task.tx.send(ret).ok();
            }
        }
    };
}

fn decrypt(task: &BlockingTaskDecrypt) -> Result<Vec<u8>> {
    let cipher  = XChaCha20Poly1305::new(task.secret.as_ref().into());
    let dec_msg = match cipher.decrypt(&XNonce::from(task.nonce), task.msg.as_slice()) {
        Ok(v) => v,
        Err(e) => return Err(anyhow::Error::msg(e))
    };

    Ok(dec_msg)
}

fn encrypt(task: &BlockingTaskEncrypt) -> Result<([u8; 24], Vec<u8>)> {
    let mut nonce = [0u8; 24];
    OsRng.fill_bytes(&mut nonce);

    let cipher  = XChaCha20Poly1305::new(task.secret.as_ref().into());
    let enc_msg = match cipher.encrypt(&nonce.into(), task.msg.as_slice()) {
        Ok(v) => v,
        Err(e) => return Err(anyhow::Error::msg(e))
    };
    
    Ok((nonce, enc_msg))
}

pub(crate) async fn schedule_blocking_task(inner: &InnerClient, dest: &PublicKey, task: MessageBlockingTaskType) {
    let worker = inner.ctx.pick_worker(dest).handle.clone();
    
    if worker.send(MessageBlockingTask {
        msg_type: task,
        ratchet: inner.state.clone(),
    }).is_err() {
        panic!("Should be able to schedule a blocking task");
    } 
}

pub(crate) async fn schedule_blocking_task_random(inner: &InnerClient, task: MessageBlockingTaskType) {
    let worker = inner.ctx.pick_random_worker().handle.clone();
    
    if worker.send(MessageBlockingTask {
        msg_type: task,
        ratchet: inner.state.clone(),
    }).is_err() {
        panic!("Should be able to schedule a blocking task");
    } 
}
