use anyhow::Result;
use borsh::BorshDeserialize;
use tokio::{io::{AsyncReadExt, AsyncWriteExt}, sync::oneshot};
use tokio_yamux::StreamHandle;

use common::api;

use super::{InnerClient, blocking_task::{schedule_blocking_task, MessageBlockingTaskType, MessageBlockingTaskEphKeyGen}};


pub(crate) async fn key_refill(inner: &InnerClient, stream: &mut StreamHandle) -> Result<()> {
    let len     = stream.read_u32().await?;
    let mut buf = vec![0u8; len as usize];

    stream.read_exact(&mut buf).await?;

    let req = api::key_store::RunOutOfEphPkNotification::try_from_slice(&buf)?;

    let (tx, rx) = oneshot::channel();
    schedule_blocking_task(inner, &inner.ctx.kp.public_key(),
        MessageBlockingTaskType::EphKeysGen(MessageBlockingTaskEphKeyGen {
            len: req.number as usize,
            tx
        })
    ).await;

    match rx.await
        .expect("Should be able to get blocking task result") {
        Ok(resp) => {
            stream.write_u32(resp.len() as u32).await?;
            stream.write_all(&resp).await?;
        },
        Err(e) => return Err(e)
    }

    Ok(())
}
