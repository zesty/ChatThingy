use common::{api::{RequestType, update::{GetUserUpdateResponse, MAX_UPDATE_SIZE, SetUserUpdate, SetUserUpdateResponse}}, keys::PublicKey};
use tokio::{sync::{mpsc, oneshot}, io::{AsyncWriteExt, AsyncReadExt}};
use tokio_yamux::StreamHandle;
use tracing::{error, info};
use borsh::{BorshSerialize, BorshDeserialize};
use anyhow::Result;
use zeroize::Zeroizing;
use rand::RngCore;

use crate::{types::{PlainTextBlob, ProfileInfoBlob, ProfileUpdate, PROFILE_NAME_TOO_LONG, AVATAR_SIZE_TOO_BIG}, Client, ClientState};
use common::api::update::GetUserUpdate;

use super::{
    InnerClient,
    blocking_task::{
        schedule_blocking_task_random,
        MessageBlockingTaskType, BlockingTaskDecrypt,
        BlockingTaskEncrypt, GroupMessageBlockingTaskWrite
    },
    CONNECT_TIMEOUT,
    message::send_inner
};

#[derive(BorshSerialize, BorshDeserialize)]
pub(crate) struct UpdateTask {
    pub update: PlainTextBlob,
    pub pk: PublicKey
}

pub(crate) async fn handler(inner: &InnerClient, mut rx: mpsc::UnboundedReceiver<UpdateTask>) {
    let state = inner.state.clone();
    loop {
        while let Ok((index, msg)) = state.get_pending_task().await {
            match UpdateTask::try_from_slice(&msg) {
                Ok(up) => {
                    if update(&inner, &state, &up).await {
                        state.delete_pending_task(index).await.ok();
                    }
                },
                Err(e) => {
                    error!("Couldn't deserialize update: {}", e);
                    state.delete_pending_task(index).await.ok();
                }
            }
        }

        loop {
            let task = match rx.recv().await {
                Some(v) => v,
                None => return
            };

            if !update(&inner, &state, &task).await {
                state.save_pending_task(task.try_to_vec().expect("Should be able to save tasks")).await.ok();
            }
        };
    }
}

async fn read_update_from_server(stream: &mut StreamHandle, pk: &PublicKey, height: u32) -> Result<GetUserUpdateResponse> {
    let req = GetUserUpdate {
        height,
        public_key: pk.clone()
    }.try_to_vec()?;

    stream.write_u32(req.len() as u32).await?;
    stream.write_all(&req).await?;

    let size = stream.read_u32().await? as usize;

    if size > MAX_UPDATE_SIZE {
        return Err(anyhow::Error::msg(format!("update info size with height {} surpasses MAX_UPDATE_SIZE", height)));
    }
    
    let mut buf = vec![0u8; size];
    stream.read_exact(&mut buf).await?;

    let resp = GetUserUpdateResponse::try_from_slice(&buf)?;

    Ok(resp)
}

async fn update(inner: &InnerClient, state: &ClientState, task: &UpdateTask) -> bool {
    match &task.update {
        PlainTextBlob::ProfileUpdate(u) => {
            if state.last_update_height(&task.pk).await.unwrap_or(u32::MAX) > u.height {
                return true;
            } else if u.secret.len() != 32 || u.nonce.len() != 24 {
                info!("Skipping invalid profile update from {}", task.pk);
                return false;
            }

            let mut stream = match Client::init_channel(&inner, RequestType::GetUpdateInfo).await {
                Ok(v) => v,
                Err(_) => {
                    tokio::time::sleep(CONNECT_TIMEOUT).await;
                    return false;
                },
            };

            match read_update_from_server(&mut stream, &task.pk, u.height).await {
                Ok(resp) => match resp {
                    GetUserUpdateResponse::Ok(info) => {
                        let (tx, rx) = oneshot::channel();
                        schedule_blocking_task_random(&inner,
                            MessageBlockingTaskType::Decrypt(BlockingTaskDecrypt {
                                msg: info,
                                secret: Zeroizing::new(u.secret.as_slice().try_into().expect("Secret should be 32 bytes")),
                                nonce: u.nonce.as_slice().try_into().expect("Nonce should be 24 bytes"),
                                tx
                            })
                        ).await;

                        let resp = rx.await
                            .expect("Should receive decryption result");

                        match resp {
                            Ok(v) => {
                                match ProfileInfoBlob::try_from_slice(&v) {
                                    Ok(info) => {
                                        stream.shutdown().await.ok();
                                        inner.activity.clone().send(crate::Activity::User(crate::UserActivity::Update(task.pk.clone(), u.height, info))).ok();
                                    },
                                    Err(_) => {
                                        stream.shutdown().await.ok();
                                        return true;
                                    }
                                }
                            },
                            Err(e) => {
                                error!("Can't decrypt profile info for {} with height {}: {}", task.pk, u.height, e);
                                stream.shutdown().await.ok();
                                return true;
                            }
                        }
                    },
                    _ => ()
                },
                Err(e) => {
                    error!("Can't update info for {}: {}", task.pk, e);
                    stream.shutdown().await.ok();
                    return false;
                }
            }
        }
        _ => ()
    }

    return true;
}

pub enum ProfileUpdateStatus {
    Ok(u32),
    ServerUnreachable,
    NameTooLong,
    AvatarSizeTooBig,
    Failed,
    Skip
}

pub(crate) async fn update_info(inner: &InnerClient, name: &str, avatar: Vec<u8>) -> ProfileUpdateStatus {
    if name.len() > PROFILE_NAME_TOO_LONG {
        return ProfileUpdateStatus::NameTooLong;
    } else if avatar.len() > AVATAR_SIZE_TOO_BIG {
        return ProfileUpdateStatus::AvatarSizeTooBig;
    }

    let state = inner.state.clone();

    let mut secret = [0u8; 32];
    let mut seed: rand::rngs::StdRng = rand::SeedableRng::from_entropy();
    seed.try_fill_bytes(&mut secret)
        .expect("Can't generate secret key for profile update");
    
    let msg = ProfileInfoBlob {
        name: name.to_string(),
        avatar
    };
    let msg = match msg.try_to_vec() {
        Ok(v) => v,
        Err(_) => return ProfileUpdateStatus::Failed
    };

    let (tx, rx) = oneshot::channel();
    schedule_blocking_task_random(&inner,
        MessageBlockingTaskType::Encrypt(BlockingTaskEncrypt {
            msg,
            secret: Zeroizing::new(secret.clone()),
            tx
        })
    ).await;

    let new_height = match state.last_profile_update_height().await {
        Ok(v) => v,
        Err(e) => {
            error!("Couln't fetch last profile update: {}", e);
            return ProfileUpdateStatus::Failed;
        }
    };

    let resp = match rx.await
        .expect("Should receive encryption result") {
        Ok(v) => v,
        Err(e) => {
            error!("Failed to encrypt profile update: {}", e);
            return ProfileUpdateStatus::Failed;
        }
    };
    let update = PlainTextBlob::ProfileUpdate(ProfileUpdate {
        height: new_height,
        secret: secret.to_vec(),
        nonce: resp.0.to_vec()
    });

    let mut stream = match Client::init_channel(&inner, RequestType::UpdateInfo).await {
        Ok(v) => v,
        Err(_) => {
            return ProfileUpdateStatus::ServerUnreachable;
        },
    };

    let profile_update = SetUserUpdate { height: new_height, info: resp.1 };
    let profile_update = match profile_update.try_to_vec() {
        Ok(v) => v,
        Err(_) => return ProfileUpdateStatus::Failed
    };

    match write_update_to_server(&mut stream, &profile_update).await {
        Ok(SetUserUpdateResponse::Ok) => (),
        Ok(SetUserUpdateResponse::Err) => {
            stream.shutdown().await.ok();
            return ProfileUpdateStatus::Failed;
        }
        _ => {
            stream.shutdown().await.ok();
            return ProfileUpdateStatus::ServerUnreachable;
        }
    }
    stream.shutdown().await.ok();

    let (tx, rx) = oneshot::channel();
    let ret = send_inner(inner, &inner.ctx.kp.public_key(),
        MessageBlockingTaskType::GroupWrite(GroupMessageBlockingTaskWrite {
            msg: update,
            tx
        }),
        true,
        rx
    ).await;

    match ret {
        Ok(super::message::SendMessage::Ok | super::message::SendMessage::NotSent) => {
            state.save_profile_update(&ProfileUpdate {
                height: new_height,
                secret: secret.to_vec(),
                nonce: resp.0.to_vec()
            }).await.ok();
            ProfileUpdateStatus::Ok(new_height)
        },
        Ok(super::message::SendMessage::Err(e)) => {
            error!("{}", e);
            ProfileUpdateStatus::Skip
        }
        Err(e) => {
            error!("{}", e);
            ProfileUpdateStatus::Skip
        }
    }
}

async fn write_update_to_server(stream: &mut StreamHandle, update: &[u8]) -> Result<SetUserUpdateResponse> {
    stream.write_u32(update.len() as u32).await?;
    stream.write_all(&update).await?;

    let size = stream.read_u32().await? as usize;
    
    let mut buf = vec![0u8; size];
    stream.read_exact(&mut buf).await?;

    let resp = SetUserUpdateResponse::try_from_slice(&buf)?;

    Ok(resp)
}
