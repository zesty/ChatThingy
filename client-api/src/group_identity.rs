use anyhow::Result;
use common::keys::{KeyPair, PublicKey};
use openmls::prelude::*;

pub(crate) fn get_pk(cred: &Credential) -> Result<PublicKey> {
    match cred.signature_scheme() {
        SignatureScheme::ED25519 => Ok(PublicKey::ED25519(cred.signature_key()
                                                          .as_slice().try_into()?)),
        _ => Err(anyhow::Error::msg("Unknewn signature scheme"))
    }
}

fn generate_credential_bundle(
    identity: Vec<u8>,
    kp: &KeyPair,
    backend: &impl OpenMlsCryptoProvider,
) -> Result<Credential, CredentialError> {
    let kp = match kp {
        KeyPair::ED25519(v) => {
            // https://github.com/openmls/openmls/blob/3248d1e2a91a79c6e7140487cb85e1bb379de274/openmls_rust_crypto/src/provider.rs#L219
            // Even thought the api asks for private and public key separated
            // In this case private key is composed of both
            let k  = v.to_bytes();
            let pk = k[32..].to_vec();
            // full key here because we need it to sign...
            let sk_pk = k.into();
            SignatureKeypair::from_bytes(SignatureScheme::ED25519, sk_pk, pk)
        }
    };

    let credential_bundle = CredentialBundle::from_parts(identity, kp);
    let credential_id = credential_bundle.credential()
        .signature_key()
        .tls_serialize_detached()
        .expect("Error serializing signature key.");
    // Store the credential bundle into the key store so OpenMLS has access
    // to it.
    backend
        .key_store()
        .store(&credential_id, &credential_bundle)
        .expect("An unexpected error occurred.");
    Ok(credential_bundle.into_parts().0)
}

fn generate_key_package_bundle(
    ciphersuites: &[Ciphersuite],
    credential: &Credential,
    backend: &impl OpenMlsCryptoProvider,
) -> Result<KeyPackage, KeyPackageBundleNewError> {
    // Fetch the credential bundle from the key store
    let credential_id = credential
        .signature_key()
        .tls_serialize_detached()
        .expect("Error serializing signature key.");
    let credential_bundle = backend
        .key_store()
        .read(&credential_id)
        .expect("An unexpected error occurred.");

    // Create the key package bundle
    let key_package_bundle =
        KeyPackageBundle::new(ciphersuites, &credential_bundle, backend, vec![])?;

    // Store it in the key store
    let key_package_id = key_package_bundle.key_package()
            .hash_ref(backend.crypto())
            .expect("Could not hash KeyPackage.");
    backend
        .key_store()
        .store(key_package_id.value(), &key_package_bundle)
        .expect("An unexpected error occurred.");
    Ok(key_package_bundle.into_parts().0)
}

pub fn create_identity(kp: &KeyPair, state: &impl OpenMlsCryptoProvider) -> Result<KeyPackage> {
    // TODO: Better way to handle these?
    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_CHACHA20POLY1305_SHA256_Ed25519;

    let credential  = generate_credential_bundle(
        kp.public_key().export().into(),
        kp,
        state,
    )?;

    let key_package = generate_key_package_bundle(&[ciphersuite], &credential, state)?;

    Ok(key_package)
}
