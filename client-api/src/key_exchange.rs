use x25519_dalek::{x25519, PublicKey, StaticSecret};
use blake2::{Blake2b512, Digest};
use zeroize::Zeroize;
use rand::rngs::OsRng;
use anyhow::Result;

pub fn kdf_rk_hk(sk: &[u8], pk: &[u8]) -> Result<([u8; 32], [u8; 32], [u8; 32])> {
    let sk: [u8; 32] = match sk.try_into() {
        Ok(v) => v,
        Err(_) => return Err(anyhow::Error::msg("kdf_rk_hk secret key derivation failed"))
    };
    let pk: [u8; 32] = match pk.try_into() {
        Ok(v) => v,
        Err(_) => return Err(anyhow::Error::msg("kdf_rk_hk publib key derivation failed"))
    };

    let shared_secret = x25519(sk, pk);

    let mut kdf = Blake2b512::new();
    kdf.update(&shared_secret);
    let mut hsecret = kdf.finalize(); 
    let mut secret: [u8; 32] = [0_u8; 32];
    secret.copy_from_slice(&hsecret[..32]);
    let mut header_secret: [u8; 32] = [0_u8; 32];
    header_secret.copy_from_slice(&hsecret[32..]);
    
    hsecret.zeroize();
    

    // For now same secret used for hk and nhk
    Ok((secret, header_secret, header_secret))
}

pub fn gen_dh_kp() -> (StaticSecret, PublicKey) {
    let secret = StaticSecret::new(&mut OsRng);
    let public = PublicKey::from(&secret);

    (secret, public)
}
