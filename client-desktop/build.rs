
fn main() {
    println!("cargo:rerun-if-changed=src/app/cc-crop-area.*");
    let lib = pkg_config::Config::new().probe("gtk4").unwrap();

    cc::Build::new()
        .includes(lib.include_paths)
        .file("src/app/cc-crop-area.c")
        .compile("cc-crop-area.a");

    glib_build_tools::compile_resources(
        &["data"],
        "data/resources.gresource.xml",
        "compiled.gresource",
    );
}
