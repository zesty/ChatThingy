use gtk::traits::GtkWindowExt;


pub fn present(window: &gtk::Window) {
    let about = libadwaita::AboutWindow::builder()
            .application_name("Nero")
            .developer_name("zesty")
            .license_type(gtk::License::Gpl30)
            .developers(vec!["zesty"])
            .copyright("Copyright © 2022-2023 zesty")
            .issue_url("https://codeberg.org/zesty/Nero/issues")
            .website("https://codeberg.org/zesty/Nero/")
            .transient_for(window)
            .build();
    about.present();
}
