mod imp;
mod profile;
mod group;
mod conversation;
mod contact;
mod member;
mod message;
mod channel;
mod channel_notification;
mod room;
mod avatar;
mod avatar_crop;

use client_core::TaskRequest;
use gtk::subclass::prelude::ObjectSubclassIsExt;
use gtk::glib::clone;
use gtk::prelude::*;
use gtk::{gio, glib};
use libadwaita::prelude::*;
use std::rc::Rc;

use self::imp::Actions;
glib::wrapper! {
    pub struct AppWindow(ObjectSubclass<imp::AppWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, @implements gio::ActionMap, gio::ActionGroup;
}

impl AppWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(app: &P, tx_handle: &TaskRequest, actions: Actions) -> Self {
        let new_self: Self = glib::Object::builder().property("application", app).build();
        let imp = new_self.imp();
        imp.tx_handle.set(tx_handle.clone()).expect("failed to pass task handle to Window");

        group::hook_actions(&new_self, &actions);

        // Profile load
        profile::update_content(&new_self);
        // We initially need to load groups
        group::update_content(&new_self);

        new_self
    }

    pub fn init_actions(application: &gtk::Application) -> Actions {
        let actions = Actions {
            network_start: gtk::gio::SimpleAction::new("network_start", None),
            network_shutdown: gtk::gio::SimpleAction::new("network_shutdown", None),

            group_rejoin: gtk::gio::SimpleAction::new("group_rejoin", None),
            group_remove: gtk::gio::SimpleAction::new("group_remove", None),
            group_leave: gtk::gio::SimpleAction::new("group_leave", None)
        };

        application.add_action(&actions.network_start);
        application.add_action(&actions.network_shutdown);

        application.add_action(&actions.group_rejoin);
        application.add_action(&actions.group_remove);
        application.add_action(&actions.group_leave);

        actions
    }
    

    fn init_label(&self) {
        let s   = self;

        profile::init(&self);
        group::init(&self);
        member::init(&self);
        contact::init(&self);
        channel::init(&self);
        conversation::init(&self);

        // Group updater
        glib::timeout_add_seconds_local(2, clone!(@weak s => @default-return Continue(true), move || {
            group::update_content(&s);
            Continue(true)
        }));
    }
}

pub async fn error_action<W: IsA<gtk::Window>>(window: Rc<W>, error: String) {
    let close_resp  = "cancel";
    let info_dialog = libadwaita::MessageDialog::builder()
        .transient_for(&*window)
        .modal(true)
        .close_response(close_resp)
        .heading("Error happened")
        .body(&error)
        .build();

    info_dialog.add_response(close_resp, "Cancel");
    info_dialog.present();
}

pub fn show_notification(window: &AppWindow, msg: &str) {
    let toast = libadwaita::Toast::new(msg);
    toast.set_priority(libadwaita::ToastPriority::High);
    toast.set_timeout(3);
    window.imp().toast_overlay.add_toast(toast);
}
