use gtk::glib;

mod imp {
    use std::cell::OnceCell;

    use gtk::glib;
    use gtk::subclass::prelude::*;

    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(string = r#"
using Gtk 4.0;
using Adw 1;

template $CropDialog : Window {
  modal: true;
  default-width: 400;
  default-height: 400;

  [titlebar]
  HeaderBar {
    show-title-buttons: false;

    [start]
    Button cancel {
      label: _("_Cancel");
      use-underline: true;
      receives-default: true;
    }
    title-widget: 
    Adw.WindowTitle title_widget {
      title: "Avatar";
    };

    [end]
    Button done {
      label: _("_Done");
      use-underline: true;
      styles [
        "suggested-action",
      ]
    }
  }
}
    "#)]
    pub struct CropDialog {
        #[template_child]
        pub cancel: TemplateChild<gtk::Button>,
        #[template_child]
        pub done: TemplateChild<gtk::Button>,
        pub crop_area: OnceCell<gtk::Widget>
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CropDialog {
        const NAME: &'static str = "CropDialog";
        type Type = super::CropDialog;
        type ParentType = gtk::Window;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for CropDialog {}
    impl WidgetImpl for CropDialog {}
    impl WindowImpl for CropDialog {}
    impl ApplicationImpl for CropDialog {}
}

glib::wrapper! {
    pub struct CropDialog(ObjectSubclass<imp::CropDialog>)
        @extends gtk::Widget, gtk::Window, gtk::Application;
}

impl Default for CropDialog {
    fn default() -> Self {
        Self::new()
    }
}

impl CropDialog {
    pub fn new() -> Self {
        glib::Object::new()
    }
}
