use gtk::subclass::prelude::*;
use gtk::glib;

mod imp {
    use gtk::glib;
    use gtk::subclass::prelude::*;

    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(string = r#"
using Gtk 4.0;
using Adw 1;

template $RoomRow: Gtk.Box {
  hexpand: true;
  valign: start;
  margin-start: 5;
  margin-end: 5;
  margin-top: 10;
  margin-bottom: 10;
  orientation: horizontal;
  Gtk.Image {
    icon-name: "lang-include-symbolic";
  }
  Gtk.Label name {
    margin-start: 5;
    hexpand: true;
    halign: start;
    ellipsize: end;
    margin-bottom: 4;
  }
}
    "#)]
    pub struct RoomRow {
        #[template_child]
        pub name: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for RoomRow {
        const NAME: &'static str = "RoomRow";
        type Type = super::RoomRow;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for RoomRow {}
    impl WidgetImpl for RoomRow {}
    impl BoxImpl for RoomRow {}
}

glib::wrapper! {
    pub struct RoomRow(ObjectSubclass<imp::RoomRow>)
        @extends gtk::Widget, gtk::Box;
}

impl Default for RoomRow {
    fn default() -> Self {
        Self::new()
    }
}

impl RoomRow {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn set_name(&self, name: &str) {
        let imp = self.imp();
        imp.name.set_text(name);
    }
}
