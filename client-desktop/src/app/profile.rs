use std::rc::Rc;

use super::avatar::AvatarPaintable;
use super::avatar_crop::CropDialog;
use super::conversation::update_contact_info;
use super::{AppWindow, show_notification};
use client_core::{task::profile, UpdateProfileInfoStatus};
use gtk::ffi::GtkWidget;
use gtk::gdk::Paintable;
use gtk::gdk::ffi::GdkPaintable;
use gtk::gdk_pixbuf::ffi::GdkPixbuf;
use gtk::glib::translate::ToGlibPtr;
use gtk::subclass::prelude::ObjectSubclassIsExt;
use gtk::glib::{clone, MainContext};
use gtk::glib;
use gtk::prelude::*;
use libadwaita::traits::ActionRowExt;

extern {
    fn cc_crop_area_new() -> *mut GtkWidget;
    fn cc_crop_area_set_paintable(area: *mut GtkWidget, paintable: *mut GdkPaintable);
    fn cc_crop_area_create_pixbuf(area: *mut GtkWidget) -> *mut GdkPixbuf;
    fn cc_crop_area_set_min_size(area: *mut GtkWidget, width: std::ffi::c_int, height: std::ffi::c_int);
}

pub fn init(window: &AppWindow) {
    let imp = window.imp();

    imp.profile_button.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        window.imp().main_stack.set_visible_child_name("profile");
    }));

    imp.profile_cancel.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        let imp  = window.imp();
        let edit = imp.profile_edit_status.replace(false);
        if edit {
            imp.profile_edit.set_visible(true);
            imp.profile_edit_ok.set_visible(false);
            imp.profile_edit_ok.set_sensitive(true);
            imp.profile_key_package.set_visible(true);
            imp.profile_identity.set_visible(true);
            imp.profile_id_edit.set_visible(false);
            imp.profile_id.set_visible(true);
            imp.profile_id_edit.set_text("");
        } else {
            imp.main_stack.set_visible_child_name("main");
        }
        // We fallback to info before edit if we cancel
        reset_before_edit(&window);
    }));

    imp.profile_id_edit.connect_text_notify(clone!(@weak window => @default-return (), move |_| {
        let imp = window.imp();
        if imp.profile_id_edit.text().len() != 0 {
            imp.profile_edit_ok.set_sensitive(true);
        } else {
            imp.profile_edit_ok.set_sensitive(false);
        }
    }));

    imp.profile_edit.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        let imp  = window.imp();
        imp.profile_edit_status.replace(true);

        imp.profile_edit.set_visible(false);
        imp.profile_edit_ok.set_visible(true);
        if imp.profile_id_edit.text().len() != 0 {
            imp.profile_edit_ok.set_sensitive(true);
        } else {
            imp.profile_edit_ok.set_sensitive(false);
        }
        imp.profile_key_package.set_visible(false);
        imp.profile_identity.set_visible(false);
        imp.profile_id_edit.set_visible(true);
        imp.profile_id.set_visible(false);

        if let Some(v) = imp.profile_id.subtitle() {
            if v.len() != 0 {
                imp.profile_id_edit.set_text(&v);
            }
        }
    }));

    imp.profile_edit_ok.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        update_action(&window);
    }));

    imp.profile_avatar_edit.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        let imp  = window.imp();
        if imp.profile_edit_status.borrow().eq(&true) {
            avatar_picker(&window);
        }
    }));

    imp.profile_key_package.connect_activated(clone!(@weak window => @default-return (), move |_| {
        copy_key_package(&window);
    }));

    imp.profile_identity.connect_activated(clone!(@weak window => @default-return (), move |_| {
        copy_identity(&window);
    }));
}

fn avatar_picker_crop(window: &AppWindow, image: &gtk::gio::File) {
    let texture = match gtk::gdk::Texture::from_file(image) {
        Ok(v) => v,
        Err(_) => {
            gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), format!("'{}' is not a valid image", image.path().unwrap().as_path().to_str().unwrap())));
            return;
        }
    };
    let image   = gtk::gdk::Paintable::from(texture);
    let img_ptr = image.to_glib_full();
    
    let crop_area: gtk::Widget = {
        unsafe {
            let c_ptr: *mut gtk::ffi::GtkWidget = cc_crop_area_new();

            cc_crop_area_set_paintable(c_ptr, img_ptr);
            cc_crop_area_set_min_size(c_ptr, 48, 48);
            glib::translate::from_glib_full(c_ptr)
        }
    };

    let crop_dialog = CropDialog::new();
    let imp = crop_dialog.imp();

    imp.cancel.connect_clicked(clone!(@weak crop_dialog, @weak crop_area => @default-return (), move |_| {
        crop_dialog.destroy();
    }));

    imp.done.connect_clicked(clone!(@weak window, @weak crop_dialog, @weak crop_area => @default-return (), move |_| {
        let imp = window.imp();
        let mut buf: gtk::gdk_pixbuf::Pixbuf;
        unsafe {
            let ptr = cc_crop_area_create_pixbuf(crop_area.to_glib_full());
            buf     = glib::translate::from_glib_full(ptr);
        }

        if buf.width() > 256 {
            buf = buf.scale_simple(256, 256, gtk::gdk_pixbuf::InterpType::Bilinear)
                .expect("Should be able to resize image")
        };
        
        let p         = gtk::gdk::Paintable::from(gtk::gdk::Texture::for_pixbuf(&buf));
        let paintable = AvatarPaintable::new();
        paintable.update(p);
        imp.profile_avatar_main.set_custom_image(Some(&paintable));

        crop_dialog.destroy();
    }));

    crop_dialog.imp().crop_area.set(crop_area.clone()).ok();
    crop_dialog.set_child(Some(&crop_area));
    crop_dialog.set_transient_for(Some(window));
    crop_dialog.present();
}

fn avatar_picker(window: &AppWindow) {
    let filters = gtk::gio::ListStore::new(glib::types::Type::OBJECT);
    let filter = gtk::FileFilter::new();
    filter.add_mime_type("image/*");
    filter.set_name(Some("Image"));
    filters.append(&filter);

    let dialog = gtk::FileDialog::builder()
        .title("Open File")
        .accept_label("Open")
        .modal(true)
        .filters(&filters)
        .build();

    dialog.open(Some(window), None as Option<&gtk::gio::Cancellable>, clone!(@weak window => @default-return (), move |r| {
        if let Ok(file) = r {
            avatar_picker_crop(&window, &file);
        }
    }));
}

pub fn reset_before_edit(window: &AppWindow) {
    let imp = window.imp();
    match imp.profile_avatar.custom_image() {
        Some(p) => imp.profile_avatar_main.set_custom_image(Some(&p)),
        None => {
            imp.profile_avatar_main.set_custom_image(None as Option<&Paintable>);
            match imp.profile_id.subtitle() {
                Some(v) => {
                    if v.len() == 0 {
                        imp.profile_avatar_main.set_text(Some(&imp.profile_identity.subtitle().unwrap()));
                    } else {
                        imp.profile_avatar_main.set_text(Some(&v));
                    }
                },
                _ => {
                    imp.profile_avatar_main.set_text(Some(&imp.profile_identity.subtitle().unwrap()));
                }
            }
        }
    }
}

fn copy_identity(window: &AppWindow) {
    let clipboard = window.clipboard();
    clipboard.set_text(&window.imp().profile_identity.subtitle().unwrap().to_string());

    show_notification(&window, "Identity copied to clipboard");
}

fn copy_key_package(window: &AppWindow) {
    let clipboard = window.clipboard();
    clipboard.set_text(&window.imp().profile_key_package.subtitle().unwrap().to_string());

    show_notification(&window, "Key package copied to clipboard");
}

pub fn update_action(window: &AppWindow) {
    let imp         = window.imp();
    let task_handle = imp.tx_handle.get().unwrap().clone();
    let name        = imp.profile_id_edit.text().to_string();
    let avatar      = match imp.profile_avatar_main.custom_image() {
        Some(v) => {
            let paintable = v.downcast::<AvatarPaintable>().expect("Should be an AvatarPaintable");
            let pixbuf    = gtk::gdk::pixbuf_get_from_texture(&paintable.texture())
                .expect("Should be able to create pixbuf for paintable");
            let buf = pixbuf.save_to_bufferv("jpeg", &[("quality", "90")])
                .expect("Should be able to export avatar as png");
            buf
        },
        None => Vec::new()
    };
    

    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp = window.imp();
        match profile::set_profile_info(&task_handle, &name, avatar).await {
            Ok(v) => match v {
                UpdateProfileInfoStatus::Ok => {
                    match imp.profile_avatar_main.custom_image() {
                        Some(v) => imp.profile_avatar.set_custom_image(Some(&v)),
                        None    => {
                            imp.profile_avatar_main.set_text(Some(&name));
                            imp.profile_avatar.set_text(Some(&name));
                        }
                    }
                    imp.profile_id.set_subtitle(&name);
                    imp.profile_edit_status.replace(false);
                    imp.profile_edit.set_visible(true);
                    imp.profile_edit_ok.set_visible(false);
                    imp.profile_key_package.set_visible(true);
                    imp.profile_identity.set_visible(true);
                    imp.profile_id_edit.set_visible(false);
                    imp.profile_id.set_visible(true);
                    imp.profile_id_edit.set_text("");
                    show_notification(&window, "Profile updated");
                },
                UpdateProfileInfoStatus::NameTooLong => {
                    gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Name is too long".to_string()));
                },
                UpdateProfileInfoStatus::AvatarSizeTooBig => {
                    gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Avatar image size is too big".to_string()));
                },
                UpdateProfileInfoStatus::Err(e) => {
                    gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), e));
                },
            },
            Err(_) => {
                return;
            }
        };
        update_contact_info(&window, 0, None);
    }));
}

pub fn update_content(window: &AppWindow) {
    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp         = window.imp();
        let task_handle = imp.tx_handle.get().unwrap().clone();
        let info        = match profile::get_profile_info(&task_handle).await {
            Ok(v) => v,
            Err(_) => {
                return;
            }
        };

        let name;
        if info.name.len() > 0 {
            name = info.name;
            imp.profile_id.set_subtitle(&name);
        } else {
            name = info.identity.clone();
        }

        if info.avatar.len() > 0 {
            let pixbuf = gtk::gdk_pixbuf::Pixbuf::from_read(std::io::Cursor::new(info.avatar))
                .expect("Couldn't read our own image");
            let p         = gtk::gdk::Paintable::from(gtk::gdk::Texture::for_pixbuf(&pixbuf));
            let paintable = AvatarPaintable::new();
            paintable.update(p);
            imp.profile_avatar_main.set_custom_image(Some(&paintable));
            imp.profile_avatar.set_custom_image(Some(&paintable));
        } else {
            imp.profile_avatar_main.set_text(Some(&name));
            imp.profile_avatar.set_text(Some(&name));
        }

        imp.profile_key_package.set_subtitle(&info.key_package);
        imp.profile_identity.set_subtitle(&info.identity);
    }));
}
