use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::glib;
use client_core::MessageType;

use super::conversation::MessageSender;

mod imp {
    use std::cell::RefCell;

    use gtk::glib;
    use gtk::subclass::prelude::*;
    use client_core::MessageType;

    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(string = r#"
using Gtk 4.0;
using Adw 1;

template $NotificationRow: Gtk.Box {
  hexpand: false;
  valign: start;
  halign: start;
  margin-bottom: 20;
  orientation: horizontal;
  spacing: 5;

  Gtk.Image {
    valign: start;
    icon-name: "lang-include-symbolic";
  }
    
  Gtk.Label action_text {
    halign: start;
    wrap: true;
  }
}
    "#)]
    pub struct NotificationRow {
        pub notification: RefCell<MessageType>,
        #[template_child]
        pub action_text: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for NotificationRow {
        const NAME: &'static str = "NotificationRow";
        type Type = super::NotificationRow;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for NotificationRow {}
    impl WidgetImpl for NotificationRow {}
    impl BoxImpl for NotificationRow {}
}

glib::wrapper! {
    pub struct NotificationRow(ObjectSubclass<imp::NotificationRow>)
        @extends gtk::Widget, gtk::Box;
}

impl Default for NotificationRow {
    fn default() -> Self {
        Self::new()
    }
}

impl NotificationRow {
    pub fn new() -> Self {
        glib::Object::new()
    }
    
    pub fn set_done_on(&self, name: &str) {
        let imp = self.imp();
        imp.action_text.set_text(&format!("{} {}", name, imp.action_text.text().as_str()));
    }
    
    pub fn set_done_by(&self, name: &str) {
        let imp = self.imp();
        imp.action_text.set_text(&format!("{} {}", imp.action_text.text().as_str(), name));
    }

    pub fn set_action_text(&self, name: &str) {
        let imp = self.imp();
        imp.action_text.set_text(name);
    }
}

pub fn create_notification(notif_type: &MessageType, sender: &MessageSender) -> gtk::Widget {
    let cont = NotificationRow::new();

    set_notification(&cont, notif_type, sender);

    cont.imp().notification.replace(notif_type.clone());
    cont.upcast::<gtk::Widget>()
}

pub fn update_notification(notification: &gtk::Widget, id: u32, sender: &MessageSender) {
    let cont = notification
        .downcast_ref::<NotificationRow>()
        .expect("Wrong widget put as a conversation notification");
    
    let notif_type = cont.imp().notification.take();
    
    match sender {
        MessageSender::Group { .. } => {
            let mut update = false;
            match notif_type {
                MessageType::NewMemberAdded { source, dest } => if id.eq(&source) || id.eq(&dest) {
                    update = true;
                },
                MessageType::ContactLeftGroup { source } => if id.eq(&source) {
                    update = true;
                },
                MessageType::NewMemberAddedByMe { dest } => if id.eq(&dest) {
                    update = true;
                }
                _ => ()
            }

            if update {
                set_notification(&cont, &notif_type, sender);
            }
        },
        _ => ()
    }

    cont.imp().notification.replace(notif_type);
}

fn set_notification(cont: &NotificationRow, notif_type: &MessageType, sender: &MessageSender) {
    match notif_type {
        MessageType::NewPendingContactSource => {
            cont.set_action_text("New conversation started! Your contact must reply first before starting conversation");
        },
        MessageType::NewPendingContactReceiver => {
            cont.set_action_text("New conversation started! You can decide whether to accept contact or not by replying");
        },
        notif @ (MessageType::NewMemberAdded { .. }
        | MessageType::ContactLeftGroup { .. }
        | MessageType::NewMemberAddedByMe { .. })=> {
            let contacts = match sender {
                MessageSender::Group { contacts } => contacts,
                _ => panic!("Group message must have a valid user")
            };

            let (source, dest) = match notif {
                MessageType::NewMemberAdded { source, dest } => {
                    cont.set_action_text("added to group by");
                    (Some(*source), Some(*dest))
                },
                MessageType::ContactLeftGroup { source } => {
                    cont.set_action_text("left group");
                    (None, Some(*source))
                },
                MessageType::NewMemberAddedByMe { dest } => {
                    cont.set_action_text("added to group by me");
                    (None, Some(*dest))
                },
                _ => unreachable!()
            };

            if let Some(source) = source {
                for contact in contacts {
                    if contact.0.eq(&source) {
                        cont.set_done_by(&contact.1);
                        break;
                    }
                }
            }

            if let Some(dest) = dest {
                for contact in contacts {
                    if contact.0.eq(&dest) {
                        cont.set_done_on(&contact.1);
                        break;
                    }
                }
            }
        },
        _ => panic!("{:?} is not a valid notification", notif_type)
    }
}
