use client_core::{PlainMessage, Receiver, ChannelType, SenderType, ChannelFeed};
use gtk::gdk::Paintable;
use gtk::{subclass::prelude::*, prelude::*};
use gtk::glib::{clone, MainContext};
use gtk::glib;
use client_core::{task::message, Message, MessageType, SendMessage};
use tracing::error;


use crate::app::message::MessageRow;
use crate::utils::humanize_time;
use super::AppWindow;
use super::avatar::AvatarPaintable;
use super::channel::{Channel, get_contact_widget};
use super::channel_notification::{create_notification, update_notification};
use super::contact::ContactRow;
use super::imp::MessageTracker;

const MESSAGE_BUNDLE_TIME: i64 = 60*5;
const MESSAGE_PAGE_SIZE: usize = 50;

#[derive(Debug)]
pub enum MessageSender {
    Me,
    Contact { id: u32, widget: ContactRow },
    Group { contacts: Vec<(u32, String, Option<Paintable>)> }
}

impl Default for MessageSender {
    fn default() -> Self {
        MessageSender::Me
    }
}

enum MessageContainer<'a> {
    New(&'a str),
    Stored(&'a Message)
}

fn new_label<'a>(message: &MessageContainer<'a>) -> gtk::Label {
    let msg = match message {
        MessageContainer::New(s) => gtk::Label::new(Some(s)),
        MessageContainer::Stored(c) => match &c.content {
            MessageType::Plain(msg) => match msg {
                PlainMessage::Text { msg } => gtk::Label::new(Some(&msg))
            },
            MessageType::DecryptionError => gtk::Label::new(Some("Message cannot be decrypted")),
            MessageType::Unreadable => gtk::Label::new(Some("Message is unreadable")),
            _ => panic!("Should not mishandle notification as message")
        }
    };
    msg.set_selectable(true);
    msg.set_halign(gtk::Align::Start);
    msg.set_wrap(true);
    msg.set_wrap_mode(gtk::pango::WrapMode::WordChar);

    msg
}

fn new_message_row<'a>(window: &AppWindow, current_time: i64, sender: &MessageSender, message: MessageContainer<'a>, time: i64) -> (gtk::ListBoxRow, gtk::Label) {
    let imp = window.imp();
    let row = MessageRow::new();

    match sender {
        MessageSender::Me => {
            match imp.profile_avatar.custom_image() {
                Some(v) => row.set_avatar(v),
                None => row.set_avatar_text(&imp.profile_avatar.text().unwrap()),
            }
            row.set_name("Me")
        },
        MessageSender::Contact { widget, id } => {
            row.imp().contact_id.replace(*id);
            let cimp = widget.imp();
            match cimp.avatar.custom_image() {
                Some(v) => row.set_avatar(v),
                None => row.set_avatar_text(&cimp.avatar.text().unwrap()),
            }
            row.set_name(&cimp.name.text())
        },
        MessageSender::Group { contacts } => {
            match message {
                MessageContainer::Stored(v) => match &v.source {
                    SenderType::User { id } => {
                        for contact in contacts {
                            if contact.0.eq(id) {
                                row.imp().contact_id.replace(*id);
                                row.set_name(&contact.1);
                                match contact.2.clone() {
                                    Some(p) => row.set_avatar(p),
                                    None => row.set_avatar_text(&contact.1)
                                }
                                break;
                            }
                        }
                    },
                    _ => panic!("Group message must be stored and have a valid user")
                },
                _ => panic!("Group message must be stored and have a valid user")
            }
        }
    }

    let time_text = humanize_time(current_time, time);
    row.set_time(&time_text);
    

    let msg = new_label(&message);
    row.imp().message_box.insert_child_after(&msg, None as Option<&gtk::Widget>);

    let new = gtk::ListBoxRow::new();
    new.set_selectable(false);
    new.set_activatable(false);

    unsafe {
        match message {
            MessageContainer::New(_) => {},
            MessageContainer::Stored(s) => {
                new.set_data("newid", s.message_id);
                new.set_data("oldid", s.message_id);
            }
        }
        new.set_data("time", time);
        new.set_data("notif", false);
    }

    new.set_child(Some(&row));

    (new, msg)
}

fn new_notification_row(window: &AppWindow, _current_time: i64, message: &Message, sender: &MessageSender, insert_pos: i32) -> gtk::ListBoxRow {
    let imp   = window.imp();
    let notif = create_notification(&message.content, sender);

    let new = gtk::ListBoxRow::new();
    new.set_selectable(false);
    new.set_activatable(false);

    new.set_child(Some(&notif));

    imp.chat_feed.insert(&new, insert_pos);
    unsafe {
        new.set_data("newid", message.message_id);
        new.set_data("oldid", message.message_id);
        new.set_data("time", message.timestamp);
        new.set_data("notif", true);
    }

    new
}

fn add_message_to_row<'a>(row: &gtk::ListBoxRow, message: MessageContainer<'a>, bottom: bool) -> gtk::Label {
    let cont = row.child()
        .and_downcast::<MessageRow>()
        .expect("Chat feed row should be a MessageRow");

    let msg = new_label(&message);

    if let Some(widget) = cont.imp().message_box.last_child() {
        if bottom {
            cont.imp().message_box.insert_child_after(&msg, Some(&widget));
        } else {
            cont.imp().message_box.insert_child_after(&msg, None as Option<&gtk::Widget>);
        }
    } else {
        cont.imp().message_box.insert_child_after(&msg, None as Option<&gtk::Widget>);
    }

    match message {
        MessageContainer::New(_) => {},
        MessageContainer::Stored(s) => {
            unsafe {
                let newid = row.data::<i64>("newid").unwrap().cast::<i64>().as_ref().to_owned();
                let oldid = row.data::<i64>("newid").unwrap().cast::<i64>().as_ref().to_owned();
                if s.message_id > newid {
                    row.set_data("newid", s.message_id);
                } else if s.message_id < oldid {
                    row.set_data("oldid", s.message_id);
                    row.set_data("time", s.timestamp);
                }
            }
        }
    }

    msg
}

pub fn init(window: &AppWindow) {
    let imp = window.imp();
    let s   = window;

    imp.chat_entry.connect_activate(clone!(@weak s => @default-return (), move |chat_entry| {
        let msg = chat_entry.text().to_string();
        chat_entry.set_text("");
        send_message(&s, msg);
    }));

    imp.chat_scroll.connect_edge_reached(clone!(@weak s => @default-return (), move |_, position| {
        let imp = s.imp();
        if position == gtk::PositionType::Bottom {
            imp.chat_sticky.replace(true);
        }
    }));

    imp.chat_feed.adjustment().unwrap().connect_upper_notify(clone!(@weak s => @default-return (), move |_| {
        let imp = s.imp();
        if imp.chat_sticky.borrow().eq(&true) {
            imp.chat_scroll
                .emit_by_name::<bool>("scroll-child", &[&gtk::ScrollType::End, &false]);

        }
    }));

    imp.chat_scroll.vadjustment().connect_changed(clone!(@weak s => @default-return (), move |_| {
        // When we load more feed at top
        // we want to stay at initial position
        let imp = s.imp();
        if let Some(child) = imp.chat_height.replace(None) {
            let vadj = imp.chat_scroll.vadjustment();
            vadj.set_value(vadj.upper()-child);
        }
    }));

    imp.chat_scroll.vadjustment().connect_value_changed(clone!(@weak s => @default-return (), move |_| {
        let imp = s.imp();
        let adj = imp.chat_scroll.vadjustment();
        // If we move from bottom we remove sticky
        if adj.upper()-adj.page_size() > adj.value() {
            imp.chat_sticky.replace(false);
        }

        // load more pages when we are about to reach edge

        let load_on_before_edge_pixels = imp.chat_scroll.vadjustment().page_size()/10.0;
        // Before doing that we need to make sure upper value is not near page size
        // Because we will never have more to load if that's the case
        if adj.upper()-adj.page_size() < load_on_before_edge_pixels {
            return;
        } else if adj.value() < load_on_before_edge_pixels {
            *imp.chat_height.borrow_mut() = Some(adj.upper());
            load_messages(&s, true, false);
        }
    }));

    glib::timeout_add_seconds_local(60, clone!(@weak window => @default-return Continue(true), move || {
        update_time(&window);
        Continue(true)
    }));
}

pub fn init_chat(window: &AppWindow) {
    let imp = window.imp();
    
    // Drop last message listener asap
    imp.message_activity.borrow_mut().take();

    // Removing all old children
    while let Some(child) = imp.chat_feed.observe_children().item(0) {
        imp.chat_feed.remove(&child.downcast::<gtk::Widget>().unwrap());
    };

    // Empty message buffer
    imp.chat_entry.set_text("");

    // Remove old pointers
    imp.last_message.replace(None);
    imp.old_message.replace(None);

    
    imp.channel_contact_info.replace(MessageSender::Group { contacts: Vec::new() });

    imp.chat_sticky.replace(true);
    load_messages(window, true, false);

    imp.chat_scroll
        .emit_by_name::<bool>("scroll-child", &[&gtk::ScrollType::End, &false]);
}

fn send_message(window: &AppWindow, msg: String) {
    let imp     = window.imp();
    let channel = imp.selected_channel.borrow().clone().unwrap();
    let (channel_type, id) = match channel {
        Channel::Contact { id } => (ChannelType::Direct, id),
        Channel::Group => {
            let borrow      = imp.group_listener.take().unwrap();
            let server      = borrow.id;
            imp.group_listener.replace(Some(borrow));
            (ChannelType::Group, server)
        }
    };

    let current_time = chrono::offset::Utc::now().timestamp();
    let message_label;
    let (old_id, can_be_added_to_old_row) = match imp.last_message.borrow_mut().clone().take() {
        Some(last_message) => {
            unsafe {
                if last_message.source == SenderType::Me
                        && current_time - last_message.timestamp < MESSAGE_BUNDLE_TIME {
                    (last_message.row.data::<i64>("newid").unwrap().cast::<i64>().as_ref().to_owned(), Some(last_message))
                } else {
                    (last_message.row.data::<i64>("newid").unwrap().cast::<i64>().as_ref().to_owned(), None)
                }
            }
        },
        None => (0, None)
    };

    let row;
    match can_be_added_to_old_row {
        None => {
            let msg_widg  = new_message_row(window, current_time, &MessageSender::Me, MessageContainer::New(&msg), current_time);
            row           = msg_widg.0.clone();
            imp.chat_feed.insert(&msg_widg.0, -1);
            message_label = msg_widg.1;

            imp.last_message.replace(Some(super::imp::MessageTracker {
                timestamp: current_time,
                source: SenderType::Me,
                row: msg_widg.0
            }));
        },
        Some(mut old_msg) => {
            message_label     = add_message_to_row(&old_msg.row, MessageContainer::New(&msg), true);
            row               = old_msg.row.clone();
            old_msg.timestamp = current_time;
            imp.last_message.replace(Some(old_msg));
        }
    }

    // Placing a tempary id that
    // we are sure we won't get duplicate
    // messages with
    unsafe {
        row.set_data("newid", old_id);
    }

    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp       = window.imp();
        let tx_handle = imp.tx_handle.get().unwrap().clone();
        let status    = message::send(&tx_handle, id, channel_type.clone(), msg)
            .await
            .expect("Can't send message to backend");

        match status {
            (SendMessage::Ok, v) | (SendMessage::NotSent, v) => {
                unsafe {
                    row.set_data("newid", v);
                }
                match channel_type {
                    ChannelType::Direct => {
                        super::channel::new_message(&window, id, current_time, true);
                    },
                    _ => ()
                }
            },
            (SendMessage::Err(e), _) => {
                message_label.add_css_class("message_error");
                message_label.set_tooltip_text(Some(&e));
            }
        }
    }));
}

fn get_channel_info(window: &AppWindow) -> Option<(ChannelType, u32)> {
    let imp     = window.imp();
    let channel = imp.selected_channel.borrow().clone().unwrap();
    match channel {
        Channel::Contact { id } => {
            match get_contact_widget(window, id) {
                Some(v) => {
                    imp.channel_contact_info.replace(MessageSender::Contact { id, widget: v });
                    Some((ChannelType::Direct, id))
                },
                None    => return None
            }
        },
        Channel::Group  => {
            let borrow = imp.group_listener.take().unwrap();
            let server = borrow.id;
            imp.group_listener.replace(Some(borrow));
            Some((ChannelType::Group, server))
        }
    }
}

pub fn load_messages(window: &AppWindow, ascending: bool, new_notify: bool) {
    let imp       = window.imp();
    let last_item = if ascending {
        match imp.old_message.borrow().clone().take() {
            Some(v) => {
                unsafe {
                    v.row.data::<i64>("oldid").unwrap().cast::<i64>().as_ref().to_owned()
                }
            }
            None => i64::MAX
        }
    } else {
        match imp.last_message.borrow().clone().take() {
            Some(v) => {
                unsafe {
                    v.row.data::<i64>("newid").unwrap().cast::<i64>().as_ref().to_owned()
                }
            }
            None => 0
        }
    };

    let (ch_type, id) = match get_channel_info(window) {
        Some(v) => v,
        None => return
    };

    // In order not to reload contacts info (name, avatar)
    // we send ids of already loaded ones
    let sender = imp.channel_contact_info.take();
    let loaded = match &sender {
        MessageSender::Group { contacts } => {
            contacts
                .iter()
                .map(|x| x.0).collect()
        },
        _ => {
            // We really don't need to do anything
            // cause if it's a single chat we have info
            // already in contact list
            Vec::new()
        }
    };
    imp.channel_contact_info.replace(sender);

    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp       = window.imp();
        let tx_handle = imp.tx_handle.get().unwrap().clone();
        let ret       = message::get_last_messages(&tx_handle, id, ch_type, ascending, last_item, MESSAGE_PAGE_SIZE, MESSAGE_BUNDLE_TIME, loaded)
            .await
            .expect("Should be able to send get messages request");

        load_message_reader(&window, ascending, new_notify, ret);
    }));
}

fn load_message_reader(window: &AppWindow, ascending: bool, new_notify: bool, mut ret: Receiver<ChannelFeed>) {
    let imp          = window.imp();
    let current_time = chrono::offset::Utc::now().timestamp();
    let mut info     = imp.channel_contact_info.take();
    //*imp.message_activity.borrow_mut() = Some(ret.1);


    // We are either reading in ascend or descend
    // When in acsend the bottom is oldest message and top is lastest message
    // When in descend we flip everything
    // We are reading messages in reverse, from newest to old 
    let (mut last_message, mut old_message, insert_pos) = if ascending {
        (imp.last_message.borrow().clone().take(), imp.old_message.borrow().clone().take(), 0)
    } else {
        (imp.old_message.borrow().clone().take(), imp.last_message.borrow().clone().take(), -1)
    };

    let mut none = true;
    let mut row: gtk::ListBoxRow;

    while let Some(feed) = ret.blocking_recv() {
        match feed {
            ChannelFeed::ContactInfo(contact) => {
                match &mut info {
                    MessageSender::Group { contacts } => {
                        let avatar = match contact.avatar {
                            Some(v) => load_pic_from_bytes(&contact.name, v),
                            None => None
                        };
                        contacts.push((contact.id, contact.name, avatar));
                    },
                    _ => panic!("Group should have MessageSender::Group")
                }
            },
            ChannelFeed::Message(message) => {
                none = false;
                // If message has no sender then it's a notification
                let source;
                if message.source == SenderType::Notification {
                    source = SenderType::Notification;
                    row    = new_notification_row(window, current_time, &message, &info, insert_pos);
                } else if message.source == SenderType::Me && new_notify {
                    // This is a duplicate
                    continue;
                } else if let Some(old_msg) = old_message.take() {
                    // If new message is not older than 5mins, we should just add it
                    // to same row of last message
                    source        = message.source.clone();
                    let time_diff = old_msg.timestamp - message.timestamp;
                    if old_msg.source == message.source
                        && time_diff > -MESSAGE_BUNDLE_TIME && time_diff < MESSAGE_BUNDLE_TIME {
                        row = old_msg.row;
                        add_message_to_row(&row, MessageContainer::Stored(&message), !ascending);
                    } else {
                        let msg_source = if message.source == SenderType::Me { true } else { false };
                        row = new_message_row(&window, current_time, if msg_source { &MessageSender::Me } else { &info }, MessageContainer::Stored(&message), message.timestamp).0;
                        imp.chat_feed.insert(&row, insert_pos);
                    }
                } else {
                    source = message.source.clone();
                    let msg_source = if message.source == SenderType::Me { true } else { false };
                    row = new_message_row(&window, current_time, if msg_source { &MessageSender::Me } else { &info }, MessageContainer::Stored(&message), message.timestamp).0;
                    imp.chat_feed.insert(&row, insert_pos);
                }

                if last_message.is_none() {
                    // This should be the newest row ever
                    last_message.replace(MessageTracker {
                        timestamp: message.timestamp, source: source.clone(), row: row.clone()
                    });
                }
                old_message.replace(MessageTracker { timestamp: message.timestamp, source, row });
            }
        }
    }

    if last_message.is_some() {
        if ascending {
            imp.last_message.replace(last_message);
            imp.old_message.replace(old_message);
        } else {
            imp.last_message.replace(old_message);
            imp.old_message.replace(last_message);
        }
    }
    
    if none {
        imp.chat_height.replace(None);
    }
    imp.channel_contact_info.replace(info);
}

pub fn update_time(window: &AppWindow) {
    let current_time = chrono::offset::Utc::now().timestamp();
    let mut pos      = 0;
    let childs       = window.imp().chat_feed.observe_children();
    while let Some(child) = childs.item(pos) {
        let row = child.downcast_ref::<gtk::ListBoxRow>()
            .expect("Should be able to cast row of chat feed");

        let notification = unsafe { row.data::<bool>("notif").unwrap().cast::<bool>().as_ref() };
        if *notification {
            pos += 1;
            continue;
        }

        let cont = row.child()
            .and_downcast::<MessageRow>()
            .expect("contact list row should be a MessageRow");


        let time = unsafe { row.data::<i64>("time").unwrap().cast::<i64>().as_ref() };

        cont.set_time(&humanize_time(current_time, *time));

        pos += 1;
    }
}

pub fn update_contact_info(window: &AppWindow, id: u32, name_avatar: Option<(String, Option<Paintable>)>) {
    let imp     = window.imp();
    let mut pos = 0;
    let childs  = window.imp().chat_feed.observe_children();

    let mut info = imp.channel_contact_info.take();
    // If id is 0 it is our own profile that was updated
    let (name, avatar) = if id.eq(&0) {
        ("Me".to_string(), imp.profile_avatar_main.custom_image())
    } else {
        let (na, av) = match &mut info {
            MessageSender::Contact { widget, .. } => {
                let contact = widget.imp();
                let avatar  = contact.avatar.custom_image();
                (contact.name.text().to_string(), avatar)
            },
            MessageSender::Group { contacts } => {
                let name_avatar = name_avatar
                    .expect("Group update contact info should have name avatar not None");

                let mut found = None;
                for contact in contacts {
                    if id.eq(&contact.0) {
                        match name_avatar.1 {
                            Some(p) => { contact.2.replace(p); },
                            None    => { contact.2.take(); }
                        }
                        contact.1 = name_avatar.0;
                        found = Some((contact.1.clone(), contact.2.clone()));
                        break;
                    }
                }
                if found.is_none() {
                    panic!("Can't have contact not in MessageSender group list when we are already updating");
                }
                found.unwrap()
            },
            _ => return
        };
        (na, av)
    };

    while let Some(child) = childs.item(pos) {
        let row = child.downcast_ref::<gtk::ListBoxRow>()
            .expect("Should be able to cast row of chat feed");

        let notification = unsafe { row.data::<bool>("notif").unwrap().cast::<bool>().as_ref() };
        if *notification {
            update_notification(&row.child().expect("Should have notification in row"), id, &info);
            pos += 1;
            continue;
        }

        let cont = row.child()
            .and_downcast::<MessageRow>()
            .expect("contact list row should be a MessageRow");

        if !cont.imp().contact_id.borrow().eq(&id) {
            pos += 1;
            continue;
        }

        match avatar.clone() {
            Some(p) => cont.set_avatar(p),
            None    => cont.set_avatar_text(&name)
        }

        cont.set_name(&name);
        
        pos += 1;
    }

    imp.channel_contact_info.replace(info);
}

pub fn load_pic_from_bytes(contact_name: &str, bytes: Vec<u8>) -> Option<Paintable> {
    match gtk::gdk_pixbuf::Pixbuf::from_read(std::io::Cursor::new(bytes)) {
        Ok(pixbuf) => {
            let p         = gtk::gdk::Paintable::from(gtk::gdk::Texture::for_pixbuf(&pixbuf));
            let paintable = AvatarPaintable::new();
            paintable.update(p);
            Some(paintable.into())
        },
        Err(e) => {
            error!("Error loading image for contact '{}': {}", contact_name, e);
            None
        }
    }
}
