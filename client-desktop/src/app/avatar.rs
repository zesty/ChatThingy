use gtk::gdk::{Paintable, Texture};
use gtk::{gdk, glib};
use gtk::prelude::*;
use gtk::subclass::prelude::*;

mod imp {
    use std::cell::RefCell;

    use gtk::gdk::Paintable;
    use gtk::prelude::*;
    use gtk::subclass::prelude::*;
    use gtk::{gdk, glib};

    pub struct AvatarPaintable {
        pub avatar: RefCell<Paintable>
    }

    impl Default for AvatarPaintable {
        fn default() -> Self {
            AvatarPaintable { avatar: RefCell::new(Paintable::new_empty(0, 0)) }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AvatarPaintable {
        const NAME: &'static str = "AvatarPaintable";
        type Type = super::AvatarPaintable;
        type Interfaces = (gdk::Paintable,);
    }

    impl ObjectImpl for AvatarPaintable {}

    impl PaintableImpl for AvatarPaintable {
        fn flags(&self) -> gdk::PaintableFlags {
            gdk::PaintableFlags::SIZE
        }

        fn intrinsic_width(&self) -> i32 {
            self.avatar.borrow().intrinsic_width()
        }

        fn intrinsic_height(&self) -> i32 {
            self.avatar.borrow().intrinsic_height()
        }

        fn snapshot(&self, snapshot: &gdk::Snapshot, width: f64, height: f64) {
            self.avatar.borrow().snapshot(snapshot, width, height)
        }
    }
}

glib::wrapper! {
    pub struct AvatarPaintable(ObjectSubclass<imp::AvatarPaintable>) @implements gdk::Paintable;
}

impl Default for AvatarPaintable {
    fn default() -> Self {
        Self::new()
    }
}

impl AvatarPaintable {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn texture(&self) -> Texture {
        self.imp().avatar.borrow().clone().downcast::<Texture>()
            .expect("Should be able to cast Paintable to Texture")
    }

    pub fn update(&self, p: Paintable) {
        let imp = self.imp();
        imp.avatar.replace(p);
        self.invalidate_contents();
    }
}
