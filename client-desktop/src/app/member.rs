use std::rc::Rc;

use client_core::{task::group, AddMember};
use gtk::{subclass::prelude::*, prelude::*};
use gtk::glib::{clone, MainContext};
use gtk::glib;


use super::AppWindow;


pub fn init(window: &AppWindow) {
    let imp = window.imp();

    imp.add_member.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        window.imp().main_stack.set_visible_child_name("add_member");
    
    }));

    imp.add_member_cancel.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        let imp = window.imp();
        imp.main_stack.set_visible_child_name("main");
        imp.add_member_key_package.set_text("");
    }));

    imp.add_member_cancel.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        window.imp().main_stack.set_visible_child_name("main");
    }));

    imp.add_member_confirm.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        add_action(window);
    }));

    imp.add_member_key_package.connect_text_notify(clone!(@weak window => @default-return (), move |_| {
        let imp = window.imp();
        if imp.add_member_key_package.text().len() != 0 {
            imp.add_member_confirm.set_sensitive(true);
        } else {
            imp.add_member_confirm.set_sensitive(false);
        }
    }));
}

fn add_action(window: AppWindow) {
    let imp         = window.imp();
    let key_package = imp.add_member_key_package.text().to_string();
    let borrow      = imp.group_listener.take().unwrap();
    let server      = borrow.id;
    imp.group_listener.replace(Some(borrow));

    window.set_sensitive(false);

    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp       = window.imp();
        let tx_handle = imp.tx_handle.get().unwrap().clone();
        let status    = group::add_member(&tx_handle, server, &key_package)
            .await
            .expect("Should be able to send group add request");

        match status {
            AddMember::Ok => {
                let imp = window.imp();
                super::show_notification(&window, &format!("New member added to group!"));
                imp.main_stack.set_visible_child_name("main");
                imp.add_member_key_package.set_text("");
            },
            AddMember::InvalidKey => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Invalid key package".to_string()));
            },
            AddMember::MemberExists => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Owner of key package is a member already".to_string()));
            },
            AddMember::ServerUnreachable => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Cannot reach server".to_string()));
            },
            AddMember::Err(e) => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), e));
            }
        }
        window.set_sensitive(true);
    }));
}
