use client_core::{ContactActivity, Contact};
use gtk::{subclass::prelude::*, prelude::*};
use gtk::glib;
use gtk::glib::clone;
use tracing::error;

use super::avatar::AvatarPaintable;
use super::contact::ContactRow;
use super::conversation::{MessageSender, update_contact_info, load_pic_from_bytes};
use super::room::RoomRow;
use super::AppWindow;

use crate::utils::humanize_time;

#[derive(Debug, Clone)]
pub enum Channel {
    Contact { id: u32 },
    Group
}

fn add_contact(contact_list: &gtk::ListBox, contact: Contact) -> gtk::ListBoxRow {
    let name = contact.name.as_str();
    let time = if let Some(v) = contact.last_timestamp {
        v
    } else {
        contact.pending.unwrap()
    };

    let child = ContactRow::new();
    child.set_name(&name);
    let human_time = humanize_time(chrono::offset::Utc::now().timestamp(), time);
    child.set_time(&human_time);

    match contact.avatar {
        Some(v) => {
            match gtk::gdk_pixbuf::Pixbuf::from_read(std::io::Cursor::new(v)) {
                Ok(pixbuf) => {
                    let p         = gtk::gdk::Paintable::from(gtk::gdk::Texture::for_pixbuf(&pixbuf));
                    let paintable = AvatarPaintable::new();
                    paintable.update(p);
                    child.set_avatar(paintable.into());
                },
                Err(e) => {
                    error!("Error loading image for contact '{}': {}", contact.name, e);
                    child.set_avatar_text(&contact.name);
                }
            }
        },
        None => child.set_avatar_text(&contact.name)
    }

    let row = gtk::ListBoxRow::new();
    row.add_css_class("channel-row");
    row.set_child(Some(&child));

    if contact.not_read > 0 {
        child.set_unread(&contact.not_read.to_string());
    }

    unsafe {
        row.set_data("id", contact.id);
        row.set_data("name", contact.name.to_owned());
        row.set_data("time", time);
    }

    contact_list.insert(&row, -1);

    row
}

pub fn new_message(window: &AppWindow, id: u32, time: i64, source: bool) {
    let imp          = window.imp();
    let current_time = chrono::offset::Utc::now().timestamp();
    let cont         = match get_contact_widget(window, id) {
        Some(v) => v,
        None => return
    };

    let row = cont.parent().and_downcast::<gtk::ListBoxRow>().unwrap();
    unsafe { row.set_data("time", time) };

    cont.set_time(&humanize_time(current_time, time));
    if !source {
        let mut currently_selected = false;
        if let Some(sel_row) = imp.contact_list.selected_row() {
            let sel_id = unsafe { sel_row.data::<u32>("id").unwrap().cast::<u32>().as_ref() };
            if sel_id.eq(&id) {
                currently_selected = true;
            }
        }

        if !currently_selected {
            if cont.imp().unread.text() == "" {
                cont.set_unread("1");
            } else {
                let new_count = cont.imp().unread.text().parse::<u32>().expect("Should be able to convert unread count label to number") + 1;
                cont.set_unread(&new_count.to_string());
            }
        }
    }

    if row.index() != 0 {
        row.changed();
    }
}

pub fn get_contact_widget(window: &AppWindow, id: u32) -> Option<ContactRow> {
    let mut pos = 0;
    let childs  = window.imp().contact_list.observe_children();
    while let Some(child) = childs.item(pos) {
        let row = child.downcast_ref::<gtk::ListBoxRow>()
            .expect("Should be able to cast row of contact list");

        let rid = unsafe { row.data::<u32>("id").unwrap().cast::<u32>().as_ref() };
        if rid.eq(&id) {
            let child = row.child()
                .and_downcast::<ContactRow>()
                .expect("contact list row should be a ContactRow");
            return Some(child);
        }

        pos += 1;
    }
    None
}

pub fn update_content(window: &AppWindow) {
    let imp          = window.imp();
    let mut activity = imp.group_listener.take().unwrap();
    let mut new_msg  = false; 
    while let Ok(event) = activity.contact.try_recv() {
        match event {
            // First event we ever receive
            ContactActivity::DirectContactList(mut contacts) => {
                // TODO: FIX ME
                let chat_room = RoomRow::new();
                chat_room.set_name("general");

                let row = gtk::ListBoxRow::new();
                row.add_css_class("channel-row");
                row.set_child(Some(&chat_room));

                imp.room_list.append(&row);

                for contact in contacts.drain(0..) {
                    add_contact(&imp.contact_list, contact);
                }
            },
            ContactActivity::NewMessage(wid, time) => {
                new_msg = true;
                new_message(window, wid, time, false);
            },
            ContactActivity::New(contact) => {
                let row = add_contact(&imp.contact_list, contact);
                row.changed();
            },
            ContactActivity::Update { id, name, avatar } => {
                let cid   = id;
                let child = match get_contact_widget(window, id) {
                    Some(v) => v,
                    None => continue
                };
                    
                child.set_name(&name);
                match avatar {
                    Some(v) => {
                        let mut paintable_avatar = None;
                        match gtk::gdk_pixbuf::Pixbuf::from_read(std::io::Cursor::new(v)) {
                            Ok(pixbuf) => {
                                let p         = gtk::gdk::Paintable::from(gtk::gdk::Texture::for_pixbuf(&pixbuf));
                                let paintable = AvatarPaintable::new();
                                paintable.update(p);
                                paintable_avatar = Some(paintable.clone().into());
                                child.set_avatar(paintable.into());
                            },
                            Err(e) => {
                                error!("Error loading image for contact '{}': {}", name, e);
                                child.set_avatar_text(&name);
                            }
                        }

                        // We next update contact info if conversation is open with this same
                        // contact
                        let info = imp.channel_contact_info.take();
                        match &info {
                            MessageSender::Contact { id, .. } => {
                                if cid.eq(id) {
                                    imp.channel_contact_info.replace(info);
                                    update_contact_info(&window, cid, None);
                                    continue;
                                }
                            },
                            MessageSender::Group { contacts } => {
                                for contact in contacts {
                                    if contact.0.eq(&id) {
                                        imp.channel_contact_info.replace(info);
                                        update_contact_info(&window, id, Some((name, paintable_avatar)));
                                        break;
                                    }
                                }
                                continue;
                            },
                            _ => ()
                        }
                        imp.channel_contact_info.replace(info);
                    },
                    None => child.set_avatar_text(&name)
                }
            },
            ContactActivity::UpdateGroupContact { id, name, avatar } => {
                let avatar = match avatar {
                    Some(v) => load_pic_from_bytes(&name, v),
                    None => None
                };
                update_contact_info(&window, id, Some((name, avatar)));
            },
            _ => {}
        }
    }
    imp.group_listener.replace(Some(activity));
    if new_msg && imp.selected_channel.borrow().is_some() {
        super::conversation::load_messages(window, false, true);
    }
}

pub fn update_time(window: &AppWindow) {
    let current_time = chrono::offset::Utc::now().timestamp();
    let mut pos      = 0;
    let childs       = window.imp().contact_list.observe_children();
    while let Some(child) = childs.item(pos) {
        let row = child.downcast_ref::<gtk::ListBoxRow>()
            .expect("Should be able to cast row of contact list");
        let cont = row.child()
            .and_downcast::<ContactRow>()
            .expect("contact list row should be a ContactRow");

        let time = unsafe { row.data::<i64>("time").unwrap().cast::<i64>().as_ref() };

        cont.set_time(&humanize_time(current_time, *time));

        pos += 1;
    }
}

pub fn init(window: &AppWindow) {
    let s   = window;
    let imp = window.imp();

    imp.contact_list.set_sort_func(|row: &gtk::ListBoxRow, rowi: &gtk::ListBoxRow| {
        let time;
        let timei;
        unsafe {
            time  = row.data::<i64>("time").unwrap().cast::<i64>().as_ref().to_owned();
            timei = rowi.data::<i64>("time").unwrap().cast::<i64>().as_ref().to_owned();
        }
        if timei < time {
            gtk::Ordering::Smaller
        } else {
            gtk::Ordering::Larger
        }
    });

    // When selecting a contact
    imp.contact_list.connect_row_selected(clone!(@weak s => @default-return (), move |_, row| {
        let imp = s.imp();
        if let Some(row) = row {
            let rid  = unsafe { row.data::<u32>("id").unwrap().cast::<u32>().as_ref().to_owned() };
            if let Some(sel) = imp.selected_channel.borrow().clone() {
                match sel {
                    Channel::Contact { id } => if rid.eq(&id) {
                        return;
                    }
                    _ => ()
                }
            }
            imp.selected_channel.replace(Some(Channel::Contact { id: rid }));
            super::conversation::init_chat(&s);
            imp.chat_stack.set_visible_child_name("chat");

            // If a group room is selected, we must unselect
            imp.room_list.unselect_all();

            let cont = row.child()
                .and_downcast::<ContactRow>()
                .expect("contact list row should be a ContactRow");
            cont.set_unread("");
        }
    }));

    imp.room_list.connect_row_selected(clone!(@weak s => @default-return (), move |_, row| {
        let imp = s.imp();
        if let Some(_) = row {
            if let Some(sel) = imp.selected_channel.borrow().clone() {
                match sel {
                    Channel::Group => {
                        return;
                    },
                    _ => ()
                }
            }
            imp.selected_channel.replace(Some(Channel::Group));
            super::conversation::init_chat(&s);
            imp.chat_stack.set_visible_child_name("chat");

            // If a direct contact is selected, we must unselect
            imp.contact_list.unselect_all();
        }
    }));

    glib::timeout_add_seconds_local(60, clone!(@weak s => @default-return Continue(true), move || {
        update_time(&s);
        Continue(true)
    }));
}
