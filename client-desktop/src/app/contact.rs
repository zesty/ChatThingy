use std::rc::Rc;

use client_core::{task::contact, AddContact};
use gtk::gdk::Paintable;
use gtk::{subclass::prelude::*, prelude::*};
use gtk::glib;
use gtk::glib::{clone, MainContext};

use super::AppWindow;

mod imp {
    use gtk::glib;
    use gtk::subclass::prelude::*;

    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(string = r#"
using Gtk 4.0;
using Adw 1;

template $ContactRow: Gtk.Box {
  hexpand: true;
  valign: start;
  margin-start: 5;
  margin-end: 5;
  margin-top: 10;
  margin-bottom: 10;
  orientation: horizontal;
  Adw.Avatar avatar {
    show-initials: true;
    size: 40;
  }
  Gtk.Label name {
    margin-start: 5;
    hexpand: true;
    halign: start;
    ellipsize: end;
  }
  Gtk.Box {
    orientation: vertical;
    halign: end;
    spacing: 5;
    Gtk.Label time {
      halign: end;
    }
    Gtk.Label unread {
      halign: end;
    }
  }
}
    "#)]
    pub struct ContactRow {
        #[template_child]
        pub avatar: TemplateChild<libadwaita::Avatar>,
        #[template_child]
        pub name: TemplateChild<gtk::Label>,
        #[template_child]
        pub time: TemplateChild<gtk::Label>,
        #[template_child]
        pub unread: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ContactRow {
        const NAME: &'static str = "ContactRow";
        type Type = super::ContactRow;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ContactRow {}
    impl WidgetImpl for ContactRow {}
    impl BoxImpl for ContactRow {}
}

glib::wrapper! {
    pub struct ContactRow(ObjectSubclass<imp::ContactRow>)
        @extends gtk::Widget, gtk::Box;
}

impl Default for ContactRow {
    fn default() -> Self {
        Self::new()
    }
}

impl ContactRow {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn set_name(&self, name: &str) {
        let imp = self.imp();
        imp.name.set_text(name);
    }

    pub fn set_avatar_text(&self, name: &str) {
        let imp = self.imp();
        imp.avatar.set_text(Some(name));
    }

    pub fn set_avatar(&self, p: Paintable) {
        let imp = self.imp();
        imp.avatar.set_custom_image(Some(&p));
    }

    pub fn set_time(&self, time: &str) {
        let imp = self.imp();
        imp.time.set_text(time);
    }

    pub fn set_unread(&self, unread: &str) {
        let imp = self.imp();
        imp.unread.set_text(unread);
        if unread.len() > 0 {
            imp.unread.add_css_class("unread");
        } else {
            imp.unread.remove_css_class("unread");
        }
    }
}

pub fn init(window: &AppWindow) {
    let imp = window.imp();

    imp.add_contact.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        window.imp().main_stack.set_visible_child_name("add_contact");
    }));

    imp.add_contact_cancel.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        let imp = window.imp();
        imp.main_stack.set_visible_child_name("main");
        imp.add_contact_identity.set_text("");
    }));

    imp.add_contact_cancel.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        window.imp().main_stack.set_visible_child_name("main");
    }));

    imp.add_contact_confirm.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        add_action(window);
    }));

    imp.add_contact_identity.connect_text_notify(clone!(@weak window => @default-return (), move |_| {
        let imp = window.imp();
        if imp.add_contact_identity.text().len() != 0 {
            imp.add_contact_confirm.set_sensitive(true);
        } else {
            imp.add_contact_confirm.set_sensitive(false);
        }
    }));
}

fn add_action(window: AppWindow) {
    let imp      = window.imp();
    let identity = imp.add_contact_identity.text().to_string();
    let borrow   = imp.group_listener.take().unwrap();
    let server   = borrow.id;
    imp.group_listener.replace(Some(borrow));

    window.set_sensitive(false);

    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp       = window.imp();
        let tx_handle = imp.tx_handle.get().unwrap().clone();
        let status    = contact::add(&tx_handle, server, &identity)
            .await
            .expect("Should be able to send contact add request");

        match status {
            AddContact::Ok => {
                let imp = window.imp();
                super::show_notification(&window, &format!("Contact {} added!", identity));
                imp.main_stack.set_visible_child_name("main");
                imp.add_member_key_package.set_text("");
            },
            AddContact::ServerUnreachable => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Cannot reach server".to_string()));
            },
            AddContact::InvalidKey => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Invalid identity".to_string()));
            },
            AddContact::ContactExists => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Contact already added".to_string()));
            },
            AddContact::NotInServer => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "This user may not be in this group".to_string()));
            },
            AddContact::TryLater => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "This user has no keys left in server. Retry later.".to_string()));
            },
            AddContact::Err(e) => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), e));
            }
        }
        window.set_sensitive(true);
    }));
}
