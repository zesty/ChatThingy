use gtk::gdk::Paintable;
use gtk::subclass::prelude::*;
use gtk::glib;

mod imp {
    use std::cell::RefCell;

    use gtk::glib;
    use gtk::subclass::prelude::*;

    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(string = r#"
using Gtk 4.0;
using Adw 1;

template $MessageRow: Gtk.Box {
  hexpand: true;
  valign: start;
  halign: start;
  margin-bottom: 20;
  orientation: horizontal;
  Adw.Avatar avatar {
    valign: start;
    size: 45;
    show-initials: true;
  }
  Gtk.Box {
    orientation: vertical;
    spacing: 10;
    margin-start: 10;
    valign: start;
    
    Gtk.Box {
      spacing: 15;
      Gtk.Label name {
        halign: start;
        ellipsize: end;
        wrap: true;
      }
      Gtk.Label time {
        halign: start;
        ellipsize: end;
        wrap: true;
      }
    }
    
    Gtk.Box message_box {
      spacing: 5;
      orientation: vertical;
      halign: start;
    }
  }
}
    "#)]
    pub struct MessageRow {
        pub contact_id: RefCell<u32>,
        #[template_child]
        pub avatar: TemplateChild<libadwaita::Avatar>,
        #[template_child]
        pub name: TemplateChild<gtk::Label>,
        #[template_child]
        pub time: TemplateChild<gtk::Label>,
        #[template_child]
        pub message_box: TemplateChild<gtk::Box>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MessageRow {
        const NAME: &'static str = "MessageRow";
        type Type = super::MessageRow;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MessageRow {}
    impl WidgetImpl for MessageRow {}
    impl BoxImpl for MessageRow {}
}

glib::wrapper! {
    pub struct MessageRow(ObjectSubclass<imp::MessageRow>)
        @extends gtk::Widget, gtk::Box;
}

impl Default for MessageRow {
    fn default() -> Self {
        Self::new()
    }
}

impl MessageRow {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn set_name(&self, name: &str) {
        let imp = self.imp();
        imp.name.set_text(name);
    }

    pub fn set_time(&self, time: &str) {
        let imp = self.imp();
        imp.time.set_text(time);
    }

    pub fn set_avatar_text(&self, name: &str) {
        let imp = self.imp();
        imp.avatar.set_text(Some(name));
    }

    pub fn set_avatar(&self, p: Paintable) {
        let imp = self.imp();
        imp.avatar.set_custom_image(Some(&p));
    }
}
