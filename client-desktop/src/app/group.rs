use client_core::{UnboundedReceiver, GroupsActivity};
use gtk::gdk::Rectangle;
use gtk::{subclass::prelude::*, prelude::*};
use gtk::glib;
use libadwaita::traits::MessageDialogExt;
use std::rc::Rc;
use gtk::glib::{clone, MainContext};

use client_core::{task::{group, network, contact}, AddGroup, LeaveGroup, NetworkStatus};

use super::imp::Actions;
use super::{AppWindow, channel};

mod imp {
    use std::cell::RefCell;

    use gtk::glib;
    use gtk::subclass::prelude::*;

    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(string = r#"
using Gtk 4.0;
using Adw 1;

template $GroupRow: Gtk.Box {
  halign: center;
  Gtk.Overlay {
    Adw.Avatar avatar {
      show-initials: true;
      size: 40;
    }
    
    [overlay]
    Gtk.Image status {
      pixel-size: 13;
      valign: end;
      halign: end;
      icon-name: "offline-symbolic";
    }
  }
}
    "#)]
    pub struct GroupRow {
        #[template_child]
        pub avatar: TemplateChild<libadwaita::Avatar>,
        #[template_child]
        pub status: TemplateChild<gtk::Image>,
        
        pub left_group: RefCell<bool>
    }

    #[glib::object_subclass]
    impl ObjectSubclass for GroupRow {
        const NAME: &'static str = "GroupRow";
        type Type = super::GroupRow;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for GroupRow {}
    impl WidgetImpl for GroupRow {}
    impl BoxImpl for GroupRow {}
}

glib::wrapper! {
    pub struct GroupRow(ObjectSubclass<imp::GroupRow>)
        @extends gtk::Widget, gtk::Box;
}

impl Default for GroupRow {
    fn default() -> Self {
        Self::new()
    }
}

impl GroupRow {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn set(&self, name: &str, last_seen: i64) {
        let imp = self.imp();
        imp.avatar.set_text(Some(name));
        let current_time = chrono::offset::Utc::now().timestamp();
        // If we have more than 20 seconds
        // We consider server inactive
        imp.left_group.replace(last_seen == -1);
        imp.status.set_icon_name(Some(if last_seen == -1 {
            "group-idle-symbolic"
        } else {
            if current_time - last_seen <= 20 {
                "online-symbolic"
            } else {
                "offline-symbolic"
            }
        }));
    }
}

pub fn hook_actions(window: &AppWindow, actions: &Actions) {
    actions.group_rejoin.connect_activate(clone!(@weak window => @default-return (), move |_, _| {
        rejoin_action(&window);
    }));

    actions.group_leave.connect_activate(clone!(@weak window => @default-return (), move |_, _| {
        leave_group(&window);
    }));

    actions.group_remove.connect_activate(clone!(@weak window => @default-return (), move |_, _| {
        delete_group(&window);
    }));

    actions.network_start.connect_activate(clone!(@weak window => @default-return (), move |_, _| {
        change_network_status(&window, true);
    }));

    actions.network_shutdown.connect_activate(clone!(@weak window => @default-return (), move |_, _| {
        change_network_status(&window, false);
    }));
}

pub fn init(window: &AppWindow) {
    let imp = window.imp();

    imp.group_list.connect_row_selected(clone!(@weak window => @default-return (), move |_, row| {
        if let Some(row) = row {
            let imp = window.imp();
            let rid = unsafe { row.data::<u32>("id").unwrap().cast::<u32>().as_ref().to_owned() };
            if let Some(v) = imp.group_listener.take() {
                if v.id.eq(&rid) {
                    imp.group_listener.replace(Some(v));
                    return;
                }
                imp.group_listener.replace(Some(v));
            }

            init_group(&window, rid);
            window.imp().group_stack.set_visible_child_name("group");
        }
    }));

    glib::timeout_add_seconds_local(1, clone!(@weak window => @default-return Continue(true), move || {
        if window.imp().group_listener.borrow().is_some() {
            channel::update_content(&window);
        }
        Continue(true)
    }));

    // Network
    let net_menu = gtk::PopoverMenu::from_model(Some(&imp.network_menu.clone().upcast::<gtk::gio::MenuModel>()));
    net_menu.set_parent(&imp.network_status.clone().upcast::<gtk::Widget>());
    let net_inactive_menu = gtk::PopoverMenu::from_model(Some(&imp.inactive_network_menu.clone().upcast::<gtk::gio::MenuModel>()));
    net_inactive_menu.set_parent(&imp.network_status.clone().upcast::<gtk::Widget>());
    let gesture = gtk::GestureClick::new();
    gesture.set_button(gtk::gdk::ffi::GDK_BUTTON_SECONDARY as u32);
    gesture.connect_pressed(clone!(@weak window, @weak net_menu, @weak net_inactive_menu => @default-return (), move |gesture, _, x, y| {
        handle_network_actions(&window, &net_menu, &net_inactive_menu, &gesture, x, y);
    }));
    imp.network_status.add_controller(gesture);

    let menu = gtk::PopoverMenu::from_model(Some(&imp.group_menu.clone().upcast::<gtk::gio::MenuModel>()));
    menu.set_parent(&imp.group_scroll.clone().upcast::<gtk::Widget>());
    let inactive_menu = gtk::PopoverMenu::from_model(Some(&imp.inactive_group_menu.clone().upcast::<gtk::gio::MenuModel>()));
    inactive_menu.set_parent(&imp.group_scroll.clone().upcast::<gtk::Widget>());

    // Right click menu
    let gesture = gtk::GestureClick::new();
    gesture.set_button(gtk::gdk::ffi::GDK_BUTTON_SECONDARY as u32);
    gesture.connect_pressed(clone!(@weak window, @weak menu, @weak inactive_menu => @default-return (), move |gesture, pos, x, y| {
        handle_group_actions(&window, &menu, &inactive_menu, &gesture, pos, x, y);
    }));
    imp.group_list.add_controller(gesture);

    // Add new group
    imp.group_add.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        window.imp().main_stack.set_visible_child_name("group");
    }));

    imp.new_group_host.connect_text_notify(clone!(@weak window => @default-return (), move |_| {
        let imp = window.imp();
        if imp.new_group_host.text().len() != 0 && imp.new_group_port.text().len() != 0 {
            imp.new_group_confirm.set_sensitive(true);
        } else {
            imp.new_group_confirm.set_sensitive(false);
        }
    }));

    imp.new_group_port.connect_text_notify(clone!(@weak window => @default-return (), move |_| {
        let imp = window.imp();
        if imp.new_group_host.text().len() != 0 && imp.new_group_port.text().len() != 0 {
            imp.new_group_confirm.set_sensitive(true);
        } else {
            imp.new_group_confirm.set_sensitive(false);
        }
    }));

    imp.new_group_cancel.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        let imp = window.imp();

        imp.main_stack.set_visible_child_name("main");
        imp.new_group_host.set_text("");
        imp.new_group_port.set_text("");
    }));

    imp.new_group_confirm.connect_clicked(clone!(@weak window => @default-return (), move |_| {
        add_action(&window);
    }));

}

fn init_group(window: &AppWindow, id: u32) {
    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp = window.imp();
        let tx  = imp.tx_handle.get().clone().unwrap();

        let listener = contact::subscriber(&tx, id)
            .await
            .expect("Couldn't create invite subscriber");

        imp.group_listener.replace(Some(super::imp::GroupActivity { id, contact: listener }));
    }));
}

pub fn update_content(window: &AppWindow) {
    let imp                 = window.imp();
    let task_handle         = imp.tx_handle.get().unwrap().clone();
    if let Some(v) = imp.server_listener.replace(None) {
        update_group_status(window, v);
    } else {
        let main_context = MainContext::default();
        main_context.spawn_local(clone!(@weak window => async move {
            if let Ok(v) = group::get_activity(&task_handle).await {
                update_group_status(&window, v);
            }
        }));
    };
}

fn update_group_status(window: &AppWindow, mut server_listener: UnboundedReceiver<GroupsActivity>) {
    let imp = window.imp();

    while let Ok(mut activity) = server_listener.try_recv() {
        // We first update network status
        imp.network_status.set_icon_name(Some(match activity.network_status {
            NetworkStatus::Starting => "offline-symbolic",
            NetworkStatus::Shutdown => "offline-symbolic",
            NetworkStatus::Ready => "online-symbolic",
        }));
        
        imp.network_state.replace(if activity.network_status == NetworkStatus::Ready { true } else { false });

        // then we update every group's status
        let mut pos   = 0;
        while let Some(child) = imp.group_list.observe_children().item(pos) {
            let row = child.downcast_ref::<gtk::ListBoxRow>()
                .expect("Should be able to cast row of group list");
            let child = row.child()
                .and_downcast::<GroupRow>()
                .expect("group list row should be a GroupRow");
            let rid = unsafe { row.data::<u32>("id").unwrap().cast::<u32>().as_ref().to_owned() };

            let mut ind: isize = -1;
            for i in 0..activity.groups.len() {
                if activity.groups[i as usize].0.eq(&rid) {
                    ind = i as isize;
                    break;
                }
            }

            if ind == -1 {
                if let Some(row2) = imp.group_list.selected_row() {
                    if row2.eq(row) {
                        imp.group_stack.set_visible_child_name("empty");
                    }
                }
                imp.group_list.remove(row);
                continue;
            } else {
                let pop = activity.groups.remove(ind as usize);
                child.set(&pop.1, pop.2);
            }
            pos += 1;
        }

        for (id, server, last_seen) in activity.groups.drain(0..) {
            let row = gtk::ListBoxRow::new();
            unsafe {
                row.set_data("id", id);
            }
            let child = GroupRow::new();
            child.set(&server, last_seen);
            row.set_child(Some(&child));
            imp.group_list.insert(&row, -1);
        }
    }

    imp.server_listener.replace(Some(server_listener));
}

fn handle_network_actions(window: &AppWindow, menu: &gtk::PopoverMenu, inactive_menu: &gtk::PopoverMenu,
                        gesture: &gtk::GestureClick, x: f64, y: f64) {
    let imp  = window.imp();
    let menu = if imp.network_state.borrow().eq(&true) {
        menu
    } else {
        inactive_menu
    };

    gesture.set_state(gtk::EventSequenceState::Claimed);
    menu.set_pointing_to(Some(&Rectangle::new(x as i32, y as i32, 0, 0)));
    menu.popup();
}

fn change_network_status(window: &AppWindow, status: bool) {
    let tx_handle = window.imp().tx_handle.get().unwrap().clone();

    network::change_status(&tx_handle, status)
        .expect("Should be able to send change network status request");
}

fn handle_group_actions(window: &AppWindow, menu: &gtk::PopoverMenu, inactive_menu: &gtk::PopoverMenu,
                        gesture: &gtk::GestureClick, pos: i32, x: f64, y: f64) {
    let imp = window.imp();
    let row = if let Some(row) = imp.group_list.row_at_y(pos) {
        row
    } else {
        return;
    };

    let id = unsafe { row.data::<u32>("id").unwrap().cast::<u32>().as_ref().to_owned() };

    let child = row.child()
        .and_downcast::<GroupRow>()
        .expect("group list row should be a GroupRow");

    let menu = if child.imp().left_group.borrow().eq(&true) {
        inactive_menu
    } else {
        menu
    };

    gesture.set_state(gtk::EventSequenceState::Claimed);
    menu.set_pointing_to(Some(&Rectangle::new(x as i32, y as i32, 0, 0)));
    menu.popup();

    imp.act_sel_group.replace(id);
}

fn leave_group(window: &AppWindow) {
    let dialog = libadwaita::MessageDialog::builder()
        .heading("Do you wanna leave group?")
        .body("Leaving group means you can't access it until an admin reinvites you.")
        .default_response("cancel")
        .close_response("ok")
        .transient_for(window)
        .build();
    dialog.add_response("cancel", "Cancel");
    dialog.add_response("ok", "Leave group");

    dialog.set_response_appearance("ok", libadwaita::ResponseAppearance::Destructive);
    dialog.set_default_response(Some("cancel"));
    dialog.connect_response(None, clone!(@weak window => @default-return (), move |_, resp| {
        if resp == "ok" {
            leave_group_action(&window, false);
        }
    }));

    dialog.present();
}

fn delete_group(window: &AppWindow) {
    let dialog = libadwaita::MessageDialog::builder()
        .heading("Do you wanna delete group?")
        .body("Warning: This action is irreversible, all conversations will be permantly deleted.")
        .default_response("cancel")
        .close_response("ok")
        .transient_for(window)
        .build();
    dialog.add_response("cancel", "Cancel");
    dialog.add_response("ok", "Delete group");

    dialog.set_response_appearance("ok", libadwaita::ResponseAppearance::Destructive);
    dialog.set_default_response(Some("cancel"));
    dialog.connect_response(None, clone!(@weak window => @default-return (), move |_, resp| {
        if resp == "ok" {
            leave_group_action(&window, true);
        }
    }));

    dialog.present();
}

fn leave_group_action(window: &AppWindow, delete: bool) {
    let imp = window.imp();
    let sel = imp.act_sel_group.take();
    
    window.set_sensitive(false);

    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp       = window.imp();
        let tx_handle = imp.tx_handle.get().unwrap().clone();
        let status    = group::leave(&tx_handle, sel, delete)
            .await
            .expect("Should be able to send group leave request");

        match status {
            LeaveGroup::Ok => {
                if delete {
                    window.imp().group_listener.take();
                }
                super::show_notification(&window, if delete { "Successfully deleted group" } else { "Successfully left group" });
            },
            LeaveGroup::ServerUnreachable => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Cannot reach server".to_string()));
            },
            LeaveGroup::Err(e) => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), e));
            }
        }
        window.set_sensitive(true);
    }));
}

fn rejoin_action(window: &AppWindow) {
    let imp = window.imp();
    let sel = imp.act_sel_group.take();

    window.set_sensitive(false);

    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp       = window.imp();
        let tx_handle = imp.tx_handle.get().unwrap().clone();
        let status    = group::rejoin(&tx_handle, sel)
            .await
            .expect("Should be able to send group rejoin request");

        match status {
            AddGroup::Ok => {
                super::show_notification(&window, "Successfully rejoined group");
            },
            AddGroup::NotInvited => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "You are not invited to the group".to_string()));
            },
            AddGroup::ServerUnreachable => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Cannot reach server".to_string()));
            }
            AddGroup::Error(e) => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), e));
            }
        }
        window.set_sensitive(true);
    }));
}

fn add_action(window: &AppWindow) {
    let imp  = window.imp();
    let host = imp.new_group_host.text().to_string();
    let port = match imp.new_group_port.text().as_str().parse::<u16>() {
        Ok(v) => v,
        Err(_) => {
            // We got something that is not a port
            gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Port is invalid".to_string()));
            return;
        }
    };

    window.set_sensitive(false);

    let main_context = MainContext::default();
    main_context.spawn_local(clone!(@weak window => async move {
        let imp       = window.imp();
        let tx_handle = imp.tx_handle.get().unwrap().clone();
        let status    = group::add(&tx_handle, &host, port)
            .await
            .expect("Should be able to send group add request");

        match status {
            AddGroup::Ok => {
                let imp = window.imp();
                super::show_notification(&window, &format!("Joined group in \"{}:{}\"", host, port));
                imp.main_stack.set_visible_child_name("main");
                imp.new_group_host.set_text("");
                imp.new_group_port.set_text("");
            },
            AddGroup::NotInvited => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "You are not invited to the group".to_string()));
            },
            AddGroup::ServerUnreachable => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), "Cannot reach server".to_string()));
            }
            AddGroup::Error(e) => {
                gtk::glib::MainContext::default().spawn_local(super::error_action(Rc::new(window.clone()), e));
            }
        }
        window.set_sensitive(true);
    }));
}
