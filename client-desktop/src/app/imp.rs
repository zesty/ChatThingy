use client_core::{TaskRequest, UnboundedReceiver, ContactActivity, MessageActivity, GroupsActivity, SenderType};
use gtk::gio::SimpleAction;
use std::cell::RefCell;
use gtk::glib::once_cell::sync::OnceCell;
use gtk::glib;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;
use libadwaita::subclass::prelude::AdwApplicationWindowImpl;

use super::{
    conversation::MessageSender,
    channel::Channel
};

#[derive(Debug, Default, CompositeTemplate)]
#[template(string = r#"
using Gtk 4.0;
using Adw 1;

template $NeroMain : Adw.ApplicationWindow {
  default-width: 1200;
  default-height: 900;
  title: _("Nero");
  resizable: true;
  

  Adw.ToastOverlay toast_overlay {
    Gtk.WindowHandle {
      Gtk.Stack main_stack {
        transition-type: slide_left;

        StackPage {
          name: "main";
          child:
          Gtk.Box {
            orientation: vertical;
            Adw.HeaderBar {
              [end]
              MenuButton menu_button {
                icon-name: "open-menu-symbolic";
                menu-model: primary_menu;
              }
            }
            Gtk.Box {
              vexpand: true;
              Gtk.Box {
                width-request: 0;
                vexpand: true;
                hexpand: false;
                orientation: vertical;
                  
                Gtk.ScrolledWindow group_scroll {
                  vexpand: true;
                  Gtk.Box {
                    spacing: 5;
                    orientation: vertical;
                    Gtk.ListBox group_list {
                      styles [ "left-bar" ]
                      hexpand: true;
                      valign: start;
                    }

                    Gtk.Button group_add {
                      halign: center;
                      valign: start;
                      Adw.ButtonContent {
                        icon-name: "list-add-symbolic";
                      }
                    }
                  }
                }
                
                Gtk.Button profile_button {
                  halign: center;
                  styles [ "profile-button" ]
                  Adw.Avatar profile_avatar {
                    show-initials: true;
                    size: 50;
                  }
                }
                
                Gtk.Image network_status {
                  styles [ "network-status" ]
                  icon-name: "offline-symbolic";
                }
              }
            
              Gtk.Box {
                vexpand: false;
                hexpand: true;
                
                Gtk.Stack group_stack {
                  StackPage {
                    name: "empty";
                    child:
                    Adw.StatusPage {
                      hexpand: true;
                      title: "No group selected";
                      icon-name: "chat-message-new-symbolic";
                    };
                  }

                  StackPage {
                    name: "group";
                    child:
                    Gtk.Box {
                      Gtk.Box {
                        styles [ "left-bar" ]
                        orientation: vertical;
                        StackSwitcher {
                          stack: conversation_stack;
                        }
                        Gtk.Stack conversation_stack {
                          styles [ "chat-bar" ]
                          Gtk.StackPage {
                            title: "Contacts";
                            child:
                            Gtk.Box {
                              spacing: 10;
                              orientation: vertical;
                              Gtk.Button add_contact {
                                halign: end;
                                Adw.ButtonContent {
                                  icon-name: "list-add-symbolic";
                                }
                              }
                              Gtk.ScrolledWindow {
                                vexpand: true;
                                styles [ "left-bar" ]
                                width-request: 320;
                                Gtk.ListBox contact_list {
                                  styles [ "left-bar" ]
                                }
                              }
                            };
                          }
                          
                          Gtk.StackPage {
                            title: "Rooms";
                            child:
                            Gtk.ScrolledWindow {
                              vexpand: true;
                              styles [ "left-bar" ]
                              width-request: 320;
                              Gtk.Expander {
                                label: "Chatrooms";
                                expanded: true;
                                Gtk.ListBox room_list {
                                  styles [ "left-bar" ]
                                }
                              }
                            };
                          }
                        }
                      }
                      

                      Gtk.Stack chat_stack {
                        hexpand: true;
                        Gtk.StackPage {
                          name: "empty";
                          child:
                          Adw.StatusPage {
                            hexpand: true;
                            title: "No chat selected";
                            description: "Select a chat to start a conversation!";
                            icon-name: "chat-message-new-symbolic";
                          };
                        }

                        Gtk.StackPage {
                          name: "chat";
                          child:
                          Gtk.Box {
                            orientation: vertical;
                            Gtk.ScrolledWindow chat_scroll {
                              hscrollbar-policy: never;
                              Gtk.ListBox chat_feed {
                                styles [ "message-list" ]
                                margin-start: 10;
                                margin-end: 10;
                                vexpand: true;
                              }
                            }
                            Gtk.Box {
                              styles [ "entry-bar" ]
                              orientation: horizontal;
                              valign: end;
                              spacing: 5;
                              Gtk.Entry chat_entry {
                                margin-start: 10;
                                margin-top: 10;
                                margin-end: 10;
                                margin-bottom: 10;
                                hexpand: true;
                                enable-emoji-completion: true;
                                show-emoji-icon: true;
                              }
                            }
                          };
                        }
                      }
                      
                      Gtk.Box {
                        styles [ "left-bar", "group-bar" ]
                        width-request: 250;
                        orientation: vertical;
                        spacing: 20;
                        
                        Gtk.Box {
                          valign: start;
                          halign: end;
                          Gtk.Button add_member {
                            Adw.ButtonContent {
                              icon-name: "list-add-symbolic";
                            }
                          }
                        }
                        
                        StackSwitcher {
                          stack: group_details;
                        }
                        
                        Gtk.Stack group_details {
                          Gtk.StackPage {
                            name: "members";
                            title: _("Members");
                            child:
                            Gtk.Box {
                              Gtk.ListView group_members {}
                            };
                          }
                        }
                      }
                    };
                  }
                }
              }
            }
          };
        }
        
        StackPage {
          name: "group";
          child:
          Gtk.Box {
            orientation: vertical;
            vexpand: true;
            Adw.HeaderBar {
              title-widget:
              Label {
                styles [ "title" ]
                label: "Join group";
              };
              Gtk.Button new_group_cancel {
                Adw.ButtonContent {
                  icon-name: "go-previous-symbolic";
                }
              }
            }
          
            Adw.StatusPage {
              title: "Join a new group";
              icon-name: "people-symbolic";
              vexpand: true;
            
              Gtk.Box {
                orientation: vertical;
                halign: center;
                spacing: 20;
                Adw.Clamp {
                  maximum-size: 400;
                  Adw.PreferencesGroup {
                    hexpand: true;
                    Adw.EntryRow new_group_host {
                      title: _("Server");
                    }

                    Adw.EntryRow new_group_port {
                      input-purpose: number;
                      title: _("Port");
                    }
                  }
                }
                
                Button new_group_confirm {
                  sensitive: false;
                  label: _("Join");

                  styles [
                    "large",
                    "suggested-action",
                  ]
                }
              }
            }
          };
        }
        
        StackPage {
          name: "profile";
          child:
          Gtk.Box {
            orientation: vertical;
            
            Adw.HeaderBar {
              title-widget:
              Label {
                styles [ "title" ]
                label: "Profile";
              };
              
              [end]
              Gtk.Button profile_edit {
                Adw.ButtonContent {
                  icon-name: "document-edit-symbolic";
                }
              }

              [end]
              Gtk.Button profile_edit_ok {
                visible: false;
                Adw.ButtonContent {
                  icon-name: "emblem-ok-symbolic";
                }
              }

              Gtk.Button profile_cancel {
                Adw.ButtonContent {
                  icon-name: "go-previous-symbolic";
                }
              }
            }
            
            Gtk.Box {
              hexpand: true;
              vexpand: true;
              halign: center;
              valign: center;
              orientation: vertical;
              spacing: 100;
              
              Gtk.Button profile_avatar_edit {
                halign: center;
                styles [ "profile-button" ]
                Adw.Avatar profile_avatar_main {
                  show-initials: true;
                  size: 200;
                }
              }
              
              Adw.Clamp {
                maximum-size: 400;
                Adw.PreferencesGroup {
                  Adw.ActionRow profile_id {
                    subtitle-lines: 1;
                    title: "Name";
                  }
                  Adw.EntryRow profile_id_edit {
                    visible: false;
                    title: "Name";
                  }

                  Adw.ActionRow profile_identity {
                    subtitle-lines: 1;
                    activatable: true;
                    title: "Identity";
                    Gtk.Image {
                      icon-name: "edit-copy-symbolic";
                    }
                  }

                  Adw.ActionRow profile_key_package {
                    subtitle-lines: 1;
                    activatable: true;
                    title: "Key package";
                    Gtk.Image {
                      icon-name: "edit-copy-symbolic";
                    }
                  }
                }
              }
            }
          };
        }

        StackPage {
          name: "add_member";
          child:
          Gtk.Box {
            orientation: vertical;
            
            Adw.HeaderBar {
              title-widget:
              Label {
                styles [ "title" ]
                label: "Add new member";
              };

              Gtk.Button add_member_cancel {
                Adw.ButtonContent {
                  icon-name: "go-previous-symbolic";
                }
              }
            }
            
            Adw.StatusPage {
              title: "Add new member";
              icon-name: "people-symbolic";
              vexpand: true;
            
              
              Gtk.Box {
                orientation: vertical;
                halign: center;
                spacing: 20;
                Adw.Clamp {
                  maximum-size: 400;
                  Adw.PreferencesGroup {
                    Adw.EntryRow add_member_key_package {
                      title: "New member key package";
                    }
                  }
                }

                Button add_member_confirm {
                  sensitive: false;
                  label: _("Add");

                  styles [
                    "large",
                    "suggested-action",
                  ]
                }
              }
            }
          };
        }

        StackPage {
          name: "add_contact";
          child:
          Gtk.Box {
            orientation: vertical;
            
            Adw.HeaderBar {
              title-widget:
              Label {
                styles [ "title" ]
                label: "Add new contact";
              };

              Gtk.Button add_contact_cancel {
                Adw.ButtonContent {
                  icon-name: "go-previous-symbolic";
                }
              }
            }
            
            Adw.StatusPage {
              title: "Add new contact";
              icon-name: "people-symbolic";
              vexpand: true;
            
              
              Gtk.Box {
                orientation: vertical;
                halign: center;
                spacing: 20;
                Adw.Clamp {
                  maximum-size: 400;
                  Adw.PreferencesGroup {
                    Adw.EntryRow add_contact_identity {
                      title: "Contact identity";
                    }
                  }
                }

                Button add_contact_confirm {
                  sensitive: false;
                  label: _("Add");

                  styles [
                    "large",
                    "suggested-action",
                  ]
                }
              }
            }
          };
        }
      }
      
    }
  }
}

menu network_menu {
  item {
    label: _("_Shutdown");
    action: "app.network_shutdown";
  }
}

menu inactive_network_menu {
  item {
    label: _("_Start");
    action: "app.network_start";
  }
}

menu group_menu {
  section {
    item {
      label: _("_Leave group");
      action: "app.group_leave";
    }

    item {
      label: _("_Delete group");
      action: "app.group_remove";
    }
  }
}

menu inactive_group_menu {
  section {
    item {
      label: _("_Rejoin group");
      action: "app.group_rejoin";
    }
  }

  section {
    item {
      label: _("_Delete group");
      action: "app.group_remove";
    }
  }
}

menu primary_menu {
  section {
    item {
      label: _("_About");
      action: "app.about";
    }

    item {
      label: _("_Quit");
      action: "app.quit";
    }
  }
}
"#)]
pub struct AppWindow {
    pub tx_handle: OnceCell<TaskRequest>,
    
    // Selected group for an action
    pub act_sel_group: RefCell<u32>,

    // Profile
    #[template_child]
    pub profile_button: TemplateChild<gtk::Button>,
    #[template_child]
    pub profile_avatar: TemplateChild<libadwaita::Avatar>,
    #[template_child]
    pub profile_avatar_main: TemplateChild<libadwaita::Avatar>,
    #[template_child]
    pub profile_avatar_edit: TemplateChild<gtk::Button>,
    #[template_child]
    pub profile_cancel: TemplateChild<gtk::Button>,
    #[template_child]
    pub profile_edit: TemplateChild<gtk::Button>,
    #[template_child]
    pub profile_edit_ok: TemplateChild<gtk::Button>,
    #[template_child]
    pub profile_id: TemplateChild<libadwaita::ActionRow>,
    #[template_child]
    pub profile_id_edit: TemplateChild<libadwaita::EntryRow>,
    #[template_child]
    pub profile_key_package: TemplateChild<libadwaita::ActionRow>,
    #[template_child]
    pub profile_identity: TemplateChild<libadwaita::ActionRow>,
    pub profile_edit_status: RefCell<bool>,

    #[template_child]
    pub main_stack: TemplateChild<gtk::Stack>,
    #[template_child]
    pub group_stack: TemplateChild<gtk::Stack>,
    #[template_child]
    pub chat_stack: TemplateChild<gtk::Stack>,

    #[template_child]
    pub toast_overlay: TemplateChild<libadwaita::ToastOverlay>,

    #[template_child]
    pub group_scroll: TemplateChild<gtk::ScrolledWindow>,
    #[template_child]
    pub group_list: TemplateChild<gtk::ListBox>,
    #[template_child]
    pub group_add: TemplateChild<gtk::Button>,

    // Group things
    pub server_listener: RefCell<Option<UnboundedReceiver<GroupsActivity>>>,
    pub group_listener: RefCell<Option<GroupActivity>>,
    #[template_child]
    pub group_menu: TemplateChild<gtk::gio::MenuModel>,
    #[template_child]
    pub inactive_group_menu: TemplateChild<gtk::gio::MenuModel>,
    pub network_state: RefCell<bool>,

    // Network things
    #[template_child]
    pub network_status: TemplateChild<gtk::Image>,
    #[template_child]
    pub network_menu: TemplateChild<gtk::gio::MenuModel>,
    #[template_child]
    pub inactive_network_menu: TemplateChild<gtk::gio::MenuModel>,

    #[template_child]
    pub new_group_cancel: TemplateChild<gtk::Button>,
    #[template_child]
    pub new_group_host: TemplateChild<libadwaita::EntryRow>,
    #[template_child]
    pub new_group_port: TemplateChild<libadwaita::EntryRow>,
    #[template_child]
    pub new_group_confirm: TemplateChild<gtk::Button>,

    // Add member to group things
    #[template_child]
    pub add_member: TemplateChild<gtk::Button>,
    #[template_child]
    pub add_member_cancel: TemplateChild<gtk::Button>,
    #[template_child]
    pub add_member_key_package: TemplateChild<libadwaita::EntryRow>,
    #[template_child]
    pub add_member_confirm: TemplateChild<gtk::Button>,

    // Add contact
    #[template_child]
    pub add_contact: TemplateChild<gtk::Button>,
    #[template_child]
    pub add_contact_cancel: TemplateChild<gtk::Button>,
    #[template_child]
    pub add_contact_identity: TemplateChild<libadwaita::EntryRow>,
    #[template_child]
    pub add_contact_confirm: TemplateChild<gtk::Button>,

    // Message
    pub selected_channel: RefCell<Option<Channel>>,
    /// Conversation page
    #[template_child]
    pub chat_entry: TemplateChild<gtk::Entry>,
    /// Conversation message list
    #[template_child]
    pub chat_feed: TemplateChild<gtk::ListBox>,
    /// Chat feed height
    pub chat_height: RefCell<Option<f64>>,
    /// We got new message notification
    pub chat_sticky: RefCell<bool>,
    /// Conversation scroll
    #[template_child]
    pub chat_scroll: TemplateChild<gtk::ScrolledWindow>,
    /// Message activity listener
    pub message_activity: RefCell<Option<UnboundedReceiver<MessageActivity>>>,
    /// Last message container
    pub last_message: RefCell<Option<MessageTracker>>,
    /// Oldest message container
    pub old_message: RefCell<Option<MessageTracker>>,


    /// A dynamic object holding widgets for current contacts present in a conversation
    /// We do this in order not to load their info everytime from database
    pub channel_contact_info: RefCell<MessageSender>,
    #[template_child]
    pub contact_list: TemplateChild<gtk::ListBox>,
    #[template_child]
    pub room_list: TemplateChild<gtk::ListBox>,
}

#[derive(Debug)]
pub struct Actions {
    pub network_start: SimpleAction,
    pub network_shutdown: SimpleAction,

    pub group_rejoin: SimpleAction,
    pub group_remove: SimpleAction,
    pub group_leave: SimpleAction
}

#[derive(Debug, Clone)]
pub struct MessageTracker {
    pub timestamp: i64,
    pub source: SenderType,
    pub row: gtk::ListBoxRow
}

#[glib::object_subclass]
impl ObjectSubclass for AppWindow {
    const NAME: &'static str = "NeroMain";
    type Type = super::AppWindow;
    type ParentType = libadwaita::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for AppWindow {
    fn constructed(&self) {
        self.parent_constructed();
        self.obj().init_label();
    }
}

impl WidgetImpl for AppWindow {}
impl WindowImpl for AppWindow {}
impl ApplicationWindowImpl for AppWindow {}
impl AdwApplicationWindowImpl for AppWindow {}

#[derive(Debug)]
pub struct GroupActivity {
    pub id: u32,
    pub contact: UnboundedReceiver<ContactActivity>
}
