use chrono::prelude::*;

pub fn humanize_time(current_time: i64, time: i64) -> String {
    let human_time;
    let diff = current_time-time;
    if diff < 60 {
        human_time = "Just now".to_owned();
    } else if diff < 86400*2 {
        human_time = chrono_humanize::HumanTime::from(chrono::Duration::seconds(time - current_time)).to_string();
    } else {
        let converted: DateTime<Local> = DateTime::from(chrono::DateTime::<chrono::Utc>::from_utc(chrono::NaiveDateTime::from_timestamp_opt(time, 0).expect("Nanosecs should be valid"), chrono::Utc));
        human_time = converted.format("%y/%m/%d").to_string();
    }
    human_time
}
