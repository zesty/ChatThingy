mod profile;
mod app;
mod about;
mod utils;

use std::error::Error;
use app::AppWindow;
use client_core::block_on;
use gtk::gio::{SimpleAction, ApplicationFlags};
use gtk::{gdk, gio, CssProvider};
use gtk::prelude::*;
use dirs::home_dir;

use client_core::{init, task::profile::ProfileTaskRequest, TaskRequest};

use libadwaita::StyleManager;
use profile::AccountWindow;

fn load_resources() {
    gio::resources_register_include!("compiled.gresource")
        .expect("Failed to register resources.");
}

fn profile_access(task_handle: ProfileTaskRequest) -> Result<(), Box<dyn Error + Send + Sync>> {
    let application = libadwaita::Application::new(
        Some("x.zesty.Nero"),
        Default::default(),
    );

    application.set_flags(ApplicationFlags::NON_UNIQUE);

    application.connect_activate(move |app| {
        let provider = CssProvider::new();
        provider.load_from_data(include_str!("../data/css/profile.css"));

        // Add the provider to the default screen
        gtk::style_context_add_provider_for_display(
            &gdk::Display::default().expect("Error initializing gtk css provider."),
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );

        let win = AccountWindow::new(app, &task_handle);
        let style = StyleManager::default();
        style.set_color_scheme(libadwaita::ColorScheme::ForceDark);
        win.present();
    });

    // Basic actions for menubar
    let quit      = SimpleAction::new("quit", None);
    let app_clone = application.clone();
    quit.connect_activate(move |_, _| {
        app_clone.quit();
    });

    let about = SimpleAction::new("about", None);

    let app_clone = application.clone();
    about.connect_activate(move |_, _| {
        about::present(&app_clone.active_window().unwrap());
    });

    application.add_action(&quit);
    application.add_action(&about);

    application.run();
    Ok(())
}

fn main_window(task_handle: TaskRequest) -> Result<(), Box<dyn Error + Send + Sync>> {
    let application = gtk::Application::new(
        Some("x.zesty.Nero"),
        Default::default(),
    );
    application.set_flags(ApplicationFlags::NON_UNIQUE);

    application.connect_activate(move |app| {
        // Loading our punny css file
        let provider = CssProvider::new();
        provider.load_from_data(include_str!("../data/css/main_window.css"));

        // Add the provider to the default screen
        gtk::style_context_add_provider_for_display(
            &gdk::Display::default().expect("Error initializing gtk css provider."),
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );

        let actions = AppWindow::init_actions(&app);
        let win     = AppWindow::new(app, &task_handle, actions);
        win.present();
    });

    // Basic actions for menubar
    let quit      = SimpleAction::new("quit", None);
    let app_clone = application.clone();
    quit.connect_activate(move |_, _| {
        app_clone.quit();
    });

    let about = SimpleAction::new("about", None);
    let app_clone = application.clone();
    about.connect_activate(move |_, _| {
        about::present(&app_clone.active_window().unwrap());
    });

    application.add_action(&quit);
    application.add_action(&about);

    application.run();
    Ok(())
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    // Logger
    let filter = match tracing_subscriber::EnvFilter::try_from_env("DEBUG") {
        Ok(v)  => v.add_directive("sqlx::query=debug".parse()?)
            .add_directive("debug".parse()?),
        Err(_) => tracing_subscriber::EnvFilter::from_default_env().add_directive("sqlx::query=off".parse()?)
            .add_directive("info".parse()?)
    };

    tracing_subscriber::fmt()
        .with_env_filter(filter)
        .with_thread_names(true)
        .init();

    // Checking if our program dir exists and creating it if not there
    let prog_dir;
    match std::env::var("NEW_HOME") {
        Ok(v) => prog_dir = v,
        _ => {
            let mut home = home_dir()
                .unwrap();
            home.push(".Nero/");
            if !home.exists() {
                std::fs::create_dir(&home)
                    .expect("Should be able to create program directory")
            }
            prog_dir = home.to_str().unwrap().to_owned();
        }
    }

    // Initializing backend
    let task_handle = init(prog_dir.as_str())?;

    // Loading resources
    load_resources();

    // Profile access phase
    profile_access(task_handle.clone())?;

    if let Some((_, profile_tx)) = block_on(client_core::task::profile::active(&task_handle))? {
        main_window(profile_tx)?;
    }

    Ok(())
}
