use client_core::task::profile::ProfileTaskRequest;
use gtk::glib;
use gtk::glib::once_cell::sync::OnceCell;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;
use libadwaita::subclass::prelude::AdwApplicationWindowImpl;
use zeroize::Zeroizing;
use std::sync::Arc;

#[derive(Debug, Default, CompositeTemplate)]
#[template(string = r#"
using Gtk 4.0;
using Adw 1;

template $Profile : Adw.ApplicationWindow {
  default-width: 1200;
  default-height: 900;
  title: _("Nero");
  resizable: true;

  WindowHandle {
    Box {
      orientation: vertical;

      Adw.HeaderBar {
        title-widget: account_stack_switcher;

        [end]
        MenuButton menu_button {
          icon-name: "open-menu-symbolic";
          menu-model: primary_menu;
        }
      }

      Adw.ToastOverlay toast_overlay {
        Stack account_stack {
          transition-type: slide_left;

          StackPage {
            name: "login";
            title: _("Login");
            child: 
            Box {
              orientation: vertical;
              valign: center;
              halign: center;
              vexpand: true;
              spacing: 20;

              styles [
                "main-box",
              ]

              Adw.Clamp {
                Adw.PreferencesGroup {
                  Adw.ComboRow login_profile {
                    title: _("Profile");
                  }

                  Adw.PasswordEntryRow login_password {
                    title: _("Password");
                  }
                }
              }

              Adw.PreferencesGroup {
                Button login_confirm {
                  label: _("Login");

                  styles [
                    "large",
                    "suggested-action",
                  ]
                }
              }
            };
          }

          StackPage {
            name: "new_account";
            title: _("New account");
            child: 
            Box {
              orientation: vertical;
              valign: center;
              halign: center;
              vexpand: true;
              spacing: 20;

              styles [
                "main-box",
              ]

              Adw.Clamp {
                Adw.PreferencesGroup {
                  Adw.EntryRow new_account_profile {
                    title: _("Profile");
                  }

                  Adw.PasswordEntryRow new_account_password {
                    title: _("Password");
                  }

                  Adw.PasswordEntryRow new_account_confirm_password {
                    title: _("Password");
                  }
                }
              }

              Adw.PreferencesGroup {
                Button new_account_confirm {
                  label: _("Create");

                  styles [
                    "large",
                    "suggested-action",
                  ]
                }
              }
            };
          }
          
          StackPage {
            name: "profile";
            child: 
            Box {
              orientation: vertical;
              valign: center;
              halign: center;
              vexpand: true;
              spacing: 20;
              
              Gtk.Image {
                styles [ "profile_ready_icon" ]
                resource: "/x/zesty/Nero/images/ready.svg";
              }
              
              Gtk.Label {
                styles [ "profile_ready" ]
                label: "Your new profile is ready!";
              }
              
              Gtk.Button new_account_next {
                halign: center;
                label: _("Next");

                styles [
                  "pill",
                  "text-button",
                  "suggested-action",
                ]
              }
            };
          }
        }
      }
      
    }
  }
}

StackSwitcher account_stack_switcher {
  stack: account_stack;
}

menu primary_menu {
  section {
    item {
      label: _("_About");
      action: "app.about";
    }

    item {
      label: _("_Quit");
      action: "app.quit";
    }
  }
}
"#)]
pub struct AccountWindow {
    /// Handle for communication with backend
    pub tx_handle: OnceCell<ProfileTaskRequest>,
    /// Storing creds for created account
    pub new_acc: OnceCell<Arc<(String, Zeroizing<String>)>>,

    /// The window stack
    #[template_child]
    pub account_stack: TemplateChild<gtk::Stack>,
    // Fields for accessing existing profile
    #[template_child]
    pub login_profile: TemplateChild<libadwaita::ComboRow>,
    #[template_child]
    pub login_password: TemplateChild<libadwaita::PasswordEntryRow>,
    #[template_child]
    pub login_confirm: TemplateChild<gtk::Button>,

    // Fields for creating new profile
    #[template_child]
    pub new_account_profile: TemplateChild<libadwaita::EntryRow>,
    #[template_child]
    pub new_account_password: TemplateChild<libadwaita::PasswordEntryRow>,
    #[template_child]
    pub new_account_confirm_password: TemplateChild<libadwaita::PasswordEntryRow>,
    #[template_child]
    pub new_account_confirm: TemplateChild<gtk::Button>,

    #[template_child]
    pub account_stack_switcher: TemplateChild<gtk::StackSwitcher>,

    #[template_child]
    pub toast_overlay: TemplateChild<libadwaita::ToastOverlay>,

    #[template_child]
    pub new_account_next: TemplateChild<gtk::Button>
}

#[glib::object_subclass]
impl ObjectSubclass for AccountWindow {
    const NAME: &'static str = "Profile";
    type Type = super::AccountWindow;
    type ParentType = libadwaita::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for AccountWindow {
    fn constructed(&self) {
        self.parent_constructed();
        self.obj().init_label();
    }
}

impl WidgetImpl for AccountWindow {}
impl WindowImpl for AccountWindow {}
impl ApplicationWindowImpl for AccountWindow {}
impl AdwApplicationWindowImpl for AccountWindow {}
