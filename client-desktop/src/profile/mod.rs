mod imp;

use std::sync::Arc;
use client_core::SecretPass;
use gtk::glib::{MainContext, clone};
use gtk::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;
use gtk::{gio, glib};
use client_core::task::profile::{self, ProfileTaskRequest};
use libadwaita::traits::{ComboRowExt, EntryRowExt};
use zeroize::Zeroizing;

glib::wrapper! {
    pub struct AccountWindow(ObjectSubclass<imp::AccountWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, @implements gio::ActionMap, gio::ActionGroup;
}

impl AccountWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(app: &P, tx_handle: &ProfileTaskRequest) -> Self {
        let new_self: Self = glib::Object::builder().property("application", app).build();
        let imp = new_self.imp();
        imp.tx_handle.set(tx_handle.clone()).expect("Couldn't initialize account ui: failed to pass task handle");
        new_self
    }

    fn init_label(&self) {
        let imp = self.imp();
        // Account access
        // When one window in stack is visible
        let s = self;
        imp.account_stack.connect_visible_child_notify(clone!(@weak s => @default-return (), move |stack| {
            let imp = s.imp();
            if let Some(child_name) = stack.visible_child_name() {
                // Emptying all fields so that we don't need to see them next time
                if child_name.as_str() == "login" {
                    imp.login_profile.set_model(None as Option<&gtk::StringList>);
                    imp.login_password.set_text("");
                    Self::get_profiles(&s);
                } else if child_name.as_str() == "new_account" {
                    imp.new_account_profile.set_text("");
                    imp.new_account_password.set_text("");
                    imp.new_account_confirm_password.set_text("");
                }
            }
        }));

        // When page is first loaded, this closure is executed
        self.connect_visible_notify(|s| {
            if s.is_visible() {
                // We need to catch enter events in order to click confirm button
                // Login page
                let login_button = s.imp().login_confirm.clone();
                s.imp().login_password.connect_entry_activated(clone!(@weak login_button => @default-return (), move |_| {
                    login_button.emit_clicked();
                }));

                // New account page
                let new_acc_button = s.imp().new_account_confirm.clone();
                s.imp().new_account_profile.connect_entry_activated(move |_| {
                    new_acc_button.emit_clicked();
                });
                let new_acc_button = s.imp().new_account_confirm.clone();
                s.imp().new_account_password.connect_entry_activated(move |_| {
                    new_acc_button.emit_clicked();
                });
                let new_acc_button = s.imp().new_account_confirm.clone();
                s.imp().new_account_confirm_password.connect_entry_activated(move |_| {
                    new_acc_button.emit_clicked();
                });


                // We can finally populate profiles
                Self::get_profiles(&s);
            }
        });

        // Account login
        imp.login_confirm.connect_clicked(clone!(@weak s => @default-return (), move |_| {
            AccountWindow::login_confirm(&s);
        }));

        // Account creation
        imp.new_account_confirm.connect_clicked(clone!(@weak s => @default-return (), move |_| {
            AccountWindow::account_create(&s);
        }));

        // When we want to access new created account
        imp.new_account_next.connect_clicked(clone!(@weak s => @default-return (), move |_| {
            let new_acc = s.imp().new_acc.get().expect("Account onecell must be set at this point");
            AccountWindow::profile_login(&s, new_acc.0.clone(), new_acc.1.clone());
        }));
    }

    pub fn account_create(s: &AccountWindow) {
        let log_window  = s.clone();
        let imp         = log_window.imp();
        let name        = &imp.new_account_profile;
        let pass        = &imp.new_account_password;
        let conf_pass   = &imp.new_account_confirm_password;

        let mut error = None;
        match name.text().as_str() {
            "" => error = Some("Empty profile name".to_owned()),
            _  => {
                let gpass      = pass.text();
                let gconf_pass = conf_pass.text();
                if gpass.len() > 0 && gconf_pass.len() > 0 { 
                    if gpass != gconf_pass {
                        error = Some("Password and it's confirmation don't match".to_owned())
                    } else {
                        let new_profile = name.text().to_string();
                        let sec_pass    = Zeroizing::new(gpass.to_string());
                        let pr_pass     = Arc::new((new_profile, sec_pass));

                        log_window.set_focus(None as Option<&gtk::Widget>);
                        log_window.set_sensitive(false);

                        let main_context = MainContext::default();
                        main_context.spawn_local(clone!(@weak log_window, @strong pr_pass => async move {
                            let imp       = log_window.imp();
                            let tx_handle = imp.tx_handle.get().unwrap().clone();

                            let error = match profile::add_profile(&tx_handle, &pr_pass.0, SecretPass(pr_pass.1.clone())).await {
                                Ok(_) => None,
                                Err(e) => Some(e.to_string())
                            };

                            if let Some(error_str) = error {
                                let toast = libadwaita::Toast::new(&error_str);
                                toast.set_priority(libadwaita::ToastPriority::High);
                                toast.set_timeout(3);
                                imp.toast_overlay.add_toast(toast);
                            } else {
                                imp.account_stack_switcher.set_visible(false);
                                imp.account_stack.set_visible_child_name("profile");
                                imp.new_acc.set(pr_pass.clone()).expect("Must be able to set creds of new account");
                            }
                            log_window.set_sensitive(true);
                        }));
                    }
                } else {
                    error = Some("Password field is empty".to_owned())
                }
                // TODO: How to sanitize GString
            }
        }

        match error {
            Some(error_str) => {
                let toast = libadwaita::Toast::new(&error_str);
                toast.set_priority(libadwaita::ToastPriority::High);
                toast.set_timeout(3);
                log_window.imp().toast_overlay.add_toast(toast);
            },
            None => ()
        }
    }

    pub fn login_confirm(s: &AccountWindow) {
        let log_window  = s.clone();
        let imp         = log_window.imp();
        let profile     = &imp.login_profile;
        let pass        = &imp.login_password;

        let mut error = None;
        match profile.selected_item() {
            Some(sel_profile) => {
                let gpass = pass.text();
                match gpass.as_str() {
                    "" => error = Some("Password field is empty".to_owned()),
                    password @ _ => {
                        let sec_pass  = Zeroizing::new(password.to_string());
                        let nprofile  = sel_profile.downcast::<gtk::StringObject>().expect("Profile ComboRow elements must be String").string().to_string();

                        AccountWindow::profile_login(&log_window, nprofile, sec_pass);
                    }
                }
                // TODO: How to sanitize GString
            },
            None => error = Some("No profile selected".to_owned())
        }

        match error {
            Some(error_str) => {
                let toast = libadwaita::Toast::new(&error_str);
                toast.set_priority(libadwaita::ToastPriority::High);
                toast.set_timeout(3);
                log_window.imp().toast_overlay.add_toast(toast);
            },
            None => ()
        }
    }

    pub fn profile_login(log_window: &AccountWindow, nprofile: String, sec_pass: Zeroizing<String>) {
        log_window.set_focus(None as Option<&gtk::Widget>);
        log_window.set_sensitive(false);

        let main_context = MainContext::default();
        main_context.spawn_local(clone!(@weak log_window => async move {
            let imp       = log_window.imp();
            let tx_handle = imp.tx_handle.get().unwrap().clone();

            let error = match profile::change_profile(&tx_handle, Some((nprofile, SecretPass(sec_pass)))).await {
                Ok(_) => None,
                Err(e) => Some(e.to_string())
            };

            if let Some(error_str) = error {
                let toast = libadwaita::Toast::new(&error_str);
                toast.set_priority(libadwaita::ToastPriority::High);
                toast.set_timeout(3);
                log_window.imp().toast_overlay.add_toast(toast);
            } else {
                log_window.close();
            }
            log_window.set_sensitive(true);
        }));
    }

    pub fn get_profiles(log_window: &AccountWindow) {
        let main_context = MainContext::default();
        main_context.spawn_local(clone!(@weak log_window => async move {
            let imp       = log_window.imp();
            let profile   = &imp.login_profile;
            let tx_handle = imp.tx_handle.get().unwrap().clone();

            let existing_profiles = profile::get_profiles(&tx_handle)
                .await
                .expect("Error loading profile list");
            let prs: Vec<&str>    = existing_profiles.iter().map(|x| &**x).collect();

            profile.set_model(Some(&gtk::StringList::new(&prs)));
        }));
    }
}
