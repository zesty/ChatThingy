use borsh::{ BorshSerialize, BorshDeserialize };
pub use crypto::{digest::Digest, sha2::Sha256};

#[repr(u8)]
#[derive(BorshSerialize, BorshDeserialize, Debug)]
pub enum Hash {
    Sha256([u8; 32])
}

impl Hash {
    pub fn get_inner(&self) -> Vec<u8> {
        match self {
            Hash::Sha256(v) => v.to_vec()
        }
    }

    pub fn get_inner_ref(&self) -> &[u8] {
        match self {
            Hash::Sha256(v) => v.as_slice()
        }
    }
}

pub struct HashFn {
    comp: Vec<u8>,
    hash: Box<dyn Digest + Send>
}

impl HashFn {
    pub fn new(hash: &Hash) -> HashFn {
        HashFn {
            hash: Box::new(match hash {
                Hash::Sha256(_) => Sha256::new()
            }),
            comp: hash.get_inner()
        }
    }

    pub fn digest(&mut self, input: &[u8]) {
        self.hash.input(input)
    }

    pub fn compare(&mut self) -> bool {
        let mut new_hash = vec![0u8; self.hash.output_bytes()];
        self.hash.result(&mut new_hash);

        new_hash.eq(&self.comp)
    }
}
