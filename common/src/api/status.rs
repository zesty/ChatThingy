use borsh::{BorshSerialize, BorshDeserialize};

/// Generic response status
#[derive(Debug, BorshSerialize, BorshDeserialize, PartialEq, Clone, Copy)]
pub enum Status {
    /// Everything is good
    Ok,
    /// Public key of destination is unknewn
    UnknewnPublicKey,
    /// Public key of destination is valid but is not registered in this instance?
    UnknewnDestPublicKey,
    /// Signature is invalid
    SigError,
    /// General failure
    ReqErr
}
