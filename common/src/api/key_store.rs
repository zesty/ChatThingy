use borsh::{BorshDeserialize, BorshSerialize};

use crate::keys::PublicKey;

/// Total number of keys to send
pub const KEYS_COUNT: usize = 100;
/// Threshold in which we send notification to refill keys to user
pub const KEYS_REFILL_THRESHOLD: usize = 50;
/// Max size of response of RunOutOfEphPkNotification
pub const KEYS_REFILL_RESPONSE_MAX_SIZE: u32 = 12000;

/// Request to get an ephemeral key of a user
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct GetEphPkRequest {
    /// Destination
    pub user: PublicKey
}

/// Response of ephpemeral key request
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub enum GetEphPkResponse {
    /// Everything is good
    Ok(Vec<u8>, Vec<u8>),
    /// User in not in server
    NotInServer,
    /// User already run out of stored ephemeral keys
    OutOfKeys
}


/// A notification sent by server when it is running out of a user ephemeral keys
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct RunOutOfEphPkNotification {
    /// Number of ephemeral keys to generate
    pub number: u8
}

/// A response due to RunOutOfEphPkNotification sent to refill it's ephemeral keys
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct AddMoreEphPk {
    /// List of ephemeral keys accompanied by it's signature
    pub list: Vec<(Vec<u8>, Vec<u8>)>
}
