pub mod handshake;
pub mod message;
pub mod key_store;
pub mod group;
pub mod update;
pub mod status;

pub const VERSION: u64 = 1;
pub const MIN_SUPPORTED_VER: u64 = 1;

pub const PING_TIMEOUT_SECS: u64 = 10;

/// Requests that can be made by client
#[repr(u8)]
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum RequestType {
    /// Keepalive
    Ping,
    /// A stream of incoming message
    MessageListener,
    /// Send new message
    SendMessage,
    /// Get a user ephemeral public key to establish
    /// a secure communication channel
    GetEphPk,
    /// Initialize group
    InitGroup,
    /// Add new member to group
    AddMember,
    /// Update info of user
    UpdateInfo,
    /// Get updated info of a user
    GetUpdateInfo,
    /// Leave group
    LeaveGroup,
    /// Unknewn request
    Unknewn
}

impl TryFrom<u8> for RequestType {
    type Error = std::io::Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Ok(match value {
            0 => RequestType::Ping,
            1 => RequestType::MessageListener,
            2 => RequestType::SendMessage,
            3 => RequestType::GetEphPk,
            4 => RequestType::InitGroup,
            5 => RequestType::AddMember,
            6 => RequestType::UpdateInfo,
            7 => RequestType::GetUpdateInfo,
            8 => RequestType::LeaveGroup,
            _ => RequestType::Unknewn
        })
    }
}


/// Service side requests
#[repr(u8)]
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum ServerRequestType {
    RunOutOfEphPkNotification,
    Unknewn
}

impl TryFrom<u8> for ServerRequestType {
    type Error = std::io::Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Ok(match value {
            0 => ServerRequestType::RunOutOfEphPkNotification,
            _ => ServerRequestType::Unknewn
        })
    }
}
