use borsh::{BorshSerialize, BorshDeserialize};

use crate::keys::PublicKey;

/// Maximum size of a handshake message unit
pub const HANDSHAKE_MESSAGE_SIZE_MAX: u32 = 2000;

/// Initial Message of handshake sent by server containing challenge
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct Init {
    pub version: u64,
    pub challenge: Vec<u8>
}

/// Response of client containing version, signature of challenge
/// And user own public key used for signing
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct Second {
    pub version: u64,
    pub sig: Vec<u8>,
    pub pk: PublicKey
}

/// Third message sent from server sending status of handshake
/// ie. whether user is accepted or not
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct Third {
    pub status: Status
}

#[derive(Debug, BorshSerialize, BorshDeserialize)]
#[allow(non_camel_case_types)]
pub enum Status {
    /// Everything is ok
    OK,
    /// MSG format is wrong?
    INVALID_MSG,
    /// Invalid signature sent by user
    INVALID_SIG,
    /// User is not a member of any group
    NOT_MEMBER,
    /// Old version for either server or client
    OLD_VERSION,
    /// Sent once when server is created
    /// and administrator first access
    OK_NEW,
    /// General failure
    Err
}
