use borsh::{BorshSerialize, BorshDeserialize};

use crate::keys::PublicKey;

pub static MAX_UPDATE_SIZE: usize = 30000;

/// Get update of user
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct GetUserUpdate {
    pub height: u32,
    pub public_key: PublicKey
}

/// Get update of user response
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub enum GetUserUpdateResponse {
    /// Everything is fine
    Ok(Vec<u8>),
    /// We are probably out of sync
    InvalidHeight,
    /// User not found
    NotInServer
}

/// Set profile update
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct SetUserUpdate {
    pub height: u32,
    pub info: Vec<u8>
}

/// Set update of user response
#[repr(u8)]
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub enum SetUserUpdateResponse {
    Ok,
    Err
}
