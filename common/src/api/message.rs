use borsh::{BorshSerialize, BorshDeserialize};

use crate::{
    keys::PublicKey,
    api::status::Status, hash::Hash
};

/// Max total size of a message unit sent between server and users
pub const MESSAGE_RAW_SIZE_MAX: usize = 4096;

/// Message payload
#[repr(u8)]
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub enum MessagePayload {
    DirectMessage(DirectMessage),
    GroupMessage(GroupMessage)
}

/// Direct message payload
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct DirectMessage {
    /// The message itself
    pub message: Vec<u8>,
    /// Public key source
    pub source: PublicKey,
    /// The destination of the message
    pub destination: PublicKey,
    /// Timestamp of message according to user
    /// This differs from server timestamp used to keep track of messages
    pub timestamp: i64,
    /// A hash for digesting message and destination and timestamp
    pub hash: Hash,
    /// A signature for the hash
    /// this way we can authenticate both message and destination
    pub sig: Vec<u8>,
}

/// Group message payload
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct GroupMessage {
    pub message: Vec<u8>,
    pub timestamp: i64
}

/// Send message response
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct SendMessageResponse {
    pub status: Status
}


/// Message notification
/// Sent by client to specify starting read position
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct MessageNotification {
    pub last_id: i64,
    pub last_group_id: i64
}
