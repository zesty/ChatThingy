use borsh::{BorshSerialize, BorshDeserialize};

use crate::keys::PublicKey;


/// Initialize status sent by server
/// This is only called once in the lifetime
/// of this instance to notify creation of the group
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub enum InitializeStatus {
    /// Everything is ok
    Ok,
    /// Group already instanciated
    AlreadyInstanciated,
    /// Denied, a user may be trying to call this
    Denied,
    /// General failure
    Error
}


/// Add new member to group
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub struct AddMember {
    pub public_key: PublicKey
}

/// Add new member status
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub enum AddMemberStatus {
    /// Member added
    Ok,
    /// Member already in group
    MemberExists,
    /// Permission denied
    PermissionDenied,
    /// General failure
    Error
}

/// Status of a leave group request
#[derive(Debug, BorshSerialize, BorshDeserialize)]
pub enum LeaveGroupStatus {
    /// Member added
    Ok,
    /// General failure
    Error
}
