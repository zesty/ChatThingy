use std::error::Error;
use nero_client_api::ChannelType;
use tokio::sync::mpsc;

use crate::{
    client::{TaskStatus, TaskType},
    MessageActivity, message::ChannelFeed
};
use crate::SendMessage;
use super::TaskRequest;

pub async fn subscriber(task_handle: &TaskRequest, id: u32, channel_type: ChannelType) -> Result<mpsc::UnboundedReceiver<MessageActivity>, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::MessageActivity { id, channel_type }).await {
        Ok(v) => {
            match v {
                TaskStatus::MessageActivity(r) => Ok(r),
                _ => panic!("Wrong call in message subscriber")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn send(task_handle: &TaskRequest, id: u32, channel_type: ChannelType, msg: String) -> Result<(SendMessage, i64), Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::SendMessage { id, channel_type, msg }).await {
        Ok(v) => {
            match v {
                TaskStatus::SendMessage(r, w) => Ok((r, w)),
                _ => panic!("Wrong call in Add server")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn get_last_messages(task_handle: &TaskRequest, id: u32, channel_type: ChannelType, ascending: bool,
                         first_item: i64, page_item_number: usize, grouping_time: i64, loaded_contacts: Vec<u32>) -> Result<mpsc::Receiver<ChannelFeed>, Box<dyn Error + Send + Sync>> {
    let (tx, rx) = mpsc::channel(page_item_number as usize);
    match super::send(task_handle, TaskType::GetLastMessages { resp_channel: tx, id, channel_type, ascending, first_item, page_item_number, grouping_time, loaded_contacts }).await {
        Ok(v) => {
            match v {
                TaskStatus::GetLastMessages => Ok(rx),
                _ => panic!("Wrong call in Add server")
            }
        },
        Err(e) => Err(e)
    }
}
