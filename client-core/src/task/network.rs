use tokio::sync::oneshot;
use anyhow::Result;

use crate::client::{Task, TaskType};

use super::TaskRequest;

pub fn change_status(task_handle: &TaskRequest, status: bool) -> Result<()> {
    let (tx, _) = oneshot::channel();

    match task_handle.tx.blocking_send(Task {
        typ: TaskType::ChangeNetworkStatus { status },
        resp: tx
    }) {
        Ok(_) => Ok(()),
        Err(e) => Err(anyhow::Error::msg(format!("Error sending task: {}", e)))
    }
}
