use std::error::Error;
use tokio::sync::mpsc;

use nero_client_api::AddContact;

use crate::{ContactActivity, client::{EditContact, RemoveContact, TaskStatus, TaskType}};
use super::TaskRequest;

pub async fn subscriber(task_handle: &TaskRequest, id: u32) -> Result<mpsc::UnboundedReceiver<ContactActivity>, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::ContactActivity(id)).await {
        Ok(v) => {
            match v {
                TaskStatus::ContactActivity(r) => Ok(r),
                _ => panic!("Wrong call in Subscribe contact")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn add(task_handle: &TaskRequest, server: u32, pk: &str) -> Result<AddContact, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::AddContact { server, pk: pk.to_string() }).await {
        Ok(v) => {
            match v {
                TaskStatus::AddContact(r) => Ok(r),
                _ => panic!("Wrong call in Add contact")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn edit(task_handle: &TaskRequest, id: u32, name: &str) -> Result<EditContact, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::EditContact(id, name.to_owned())).await {
        Ok(v) => {
            match v {
                TaskStatus::EditContact(r) => Ok(r),
                _ => panic!("Wrong call in Edit contact")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn remove(task_handle: &TaskRequest, ids: Vec<u32>) -> Result<RemoveContact, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::RemoveContact(ids)).await {
        Ok(v) => {
            match v {
                TaskStatus::RemoveContact(r) => Ok(r),
                _ => panic!("Wrong call in Remove contact")
            }
        },
        Err(e) => Err(e)
    }
}

