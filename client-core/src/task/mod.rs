pub mod profile;
pub mod network;
pub mod group;
pub mod contact;
pub mod message;

use std::{error::Error, mem::MaybeUninit};
use tokio::sync::{mpsc, oneshot};

use crate::client::{Task, TaskType, TaskStatus};

#[derive(Debug, Clone)]
pub struct TaskRequest {
    pub tx: mpsc::Sender<Task>
}

/// Must not be used unless it's impossible to do without this
/// Unsafe
#[allow(invalid_value)]
impl Default for TaskRequest {
    fn default() -> Self {
        TaskRequest {
            tx: unsafe { MaybeUninit::uninit().assume_init() }
        }
    }
}

pub async fn send(task_handle: &TaskRequest, typ: TaskType) -> Result<TaskStatus, Box<dyn Error + Send + Sync>> {
    let (tx, rx) = oneshot::channel();

    match task_handle.tx.send(Task {
        typ,
        resp: tx
    }).await {
        Ok(_) => (),
        Err(e) => return Err(Box::new( std::io::Error::new( std::io::ErrorKind::Other, format!("Error sending task: {}", e) ) ))
    };

    Ok(match rx.await {
        Ok(v) => v,
        Err(e) => return Err(Box::new( std::io::Error::new( std::io::ErrorKind::Other, format!("Error getting task response: {}", e) ) ))
    })
}
