use std::error::Error;
use nero_client_api::{AddMember, LeaveGroup};
use tokio::sync::mpsc;

use crate::client::{TaskStatus, TaskType, AddGroup, RemoveGroup, GroupsActivity, GroupInfo};
use super::TaskRequest;

pub async fn add_member(task_handle: &TaskRequest, server: u32, key_package: &str) -> Result<AddMember, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::AddMember { server, key_package: key_package.to_string() }).await {
        Ok(v) => {
            match v {
                TaskStatus::AddMember(r) => Ok(r),
                _ => panic!("Wrong call in Add server")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn add(task_handle: &TaskRequest, host: &str, port: u16) -> Result<AddGroup, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::AddGroup { host: host.to_owned(), port }).await {
        Ok(v) => {
            match v {
                TaskStatus::AddGroup(r) => Ok(r),
                _ => panic!("Wrong call in Add server")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn rejoin(task_handle: &TaskRequest, server: u32) -> Result<AddGroup, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::RejoinGroup { id: server }).await {
        Ok(v) => {
            match v {
                TaskStatus::AddGroup(r) => Ok(r),
                _ => panic!("Wrong call in Add server")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn leave(task_handle: &TaskRequest, server: u32, delete: bool) -> Result<LeaveGroup, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::LeaveGroup { id: server, delete }).await {
        Ok(v) => {
            match v {
                TaskStatus::LeaveGroup(r) => Ok(r),
                _ => panic!("Wrong call in Add server")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn get_activity(task_handle: &TaskRequest) -> Result<mpsc::UnboundedReceiver<GroupsActivity>, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::GroupsActivity).await {
        Ok(v) => {
            match v {
                TaskStatus::GroupsActivity(r) => Ok(r),
                _ => panic!("Wrong call in Add server")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn get_info(task_handle: &TaskRequest, server: u32) -> Result<Option<GroupInfo>, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::GetGroupInfo { id: server }).await {
        Ok(v) => {
            match v {
                TaskStatus::GetGroupInfo(r) => Ok(r),
                _ => panic!("Wrong call in Add server")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn remove(task_handle: &TaskRequest, server: &str) -> Result<RemoveGroup, Box<dyn Error + Send + Sync>> {
    match super::send(task_handle, TaskType::RemoveGroup(server.to_owned())).await {
        Ok(v) => {
            match v {
                TaskStatus::RemoveGroup(r) => Ok(r),
                _ => panic!("Wrong call in Add server")
            }
        },
        Err(e) => Err(e)
    }
}
