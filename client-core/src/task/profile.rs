use zeroize::Zeroizing;
use std::{error::Error, mem::MaybeUninit};
use tokio::sync::{mpsc, oneshot};
use crate::{r#loop::{TaskStatus, TaskType, Task, SecretPass}, client::{ProfileInfo, UpdateProfileInfoStatus}};
use super::TaskRequest;

#[derive(Debug, Clone)]
pub struct ProfileTaskRequest {
    pub tx: mpsc::Sender<Task>
}

/// Must not be used unless it's impossible to do without this
/// Unsafe
#[allow(invalid_value)]
impl Default for ProfileTaskRequest {
    fn default() -> Self {
        ProfileTaskRequest {
            tx: unsafe { MaybeUninit::uninit().assume_init() }
        }
    }
}

pub async fn send(task_handle: &ProfileTaskRequest, typ: TaskType) -> Result<TaskStatus, Box<dyn Error + Send + Sync>> {
    let handle = task_handle.tx.clone();

    let (tx, rx) = oneshot::channel();

    match handle.send(Task {
        typ,
        resp: tx
    }).await {
        Ok(_) => (),
        Err(e) => return Err(Box::new( std::io::Error::new( std::io::ErrorKind::Other, format!("Error sending task: {}", e) ) ))
    };

    Ok(match rx.await {
        Ok(v) => v,
        Err(e) => return Err(Box::new( std::io::Error::new( std::io::ErrorKind::Other, format!("Error getting task response: {}", e) ) ))
    })
}

pub async fn get_profiles(task_handle: &ProfileTaskRequest) -> Result<Vec<String>, Box<dyn Error + Send + Sync>> {
    match send(task_handle, TaskType::GetProfiles).await {
        Ok(v) => {
            match v {
                TaskStatus::GetProfiles(v) => Ok(v),
                _ => panic!("Wrong call in Get profiles")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn add_profile(task_handle: &ProfileTaskRequest, name: &str, pass: SecretPass) -> Result<(), Box<dyn Error + Send + Sync>> {
    match send(task_handle, TaskType::AddProfile(name.to_owned(), pass)).await {
        Ok(v) => {
            match v {
                TaskStatus::AddProfile(r) => match r {
                    crate::profile::AddProfile::Ok => Ok(()),
                    crate::profile::AddProfile::Err(e) => Err(Box::new( std::io::Error::new( std::io::ErrorKind::Other, e) ) ),
                },
                _ => panic!("Wrong call in Get profiles")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn change_profile(task_handle: &ProfileTaskRequest, set: Option<(String, SecretPass)>) -> Result<(), Box<dyn Error + Send + Sync>> {
    match send(task_handle, TaskType::ChangeProfile(set)).await {
        Ok(v) => {
            match v {
                TaskStatus::ChangeProfile(r) => match r {
                    crate::profile::ChangeProfile::Ok => Ok(()),
                    crate::profile::ChangeProfile::Err(e) => Err(Box::new( std::io::Error::new( std::io::ErrorKind::Other, e) ) ),
                },
                _ => panic!("Wrong call in Change profiles")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn active(task_handle: &ProfileTaskRequest) -> Result<Option<(String, TaskRequest)>, Box<dyn Error + Send + Sync>> {
    match send(task_handle, TaskType::ActiveProfile).await {
        Ok(v) => {
            match v {
                TaskStatus::ActiveProfile(r) => {
                    if let Some(v) = r {
                    Ok(Some((v.0, TaskRequest { tx: v.1 })))
                    } else {
                        Ok(None)
                    }
                },
                _ => panic!("Wrong call in Change profiles")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn get_profile_info(task: &TaskRequest) -> Result<ProfileInfo, Box<dyn Error + Send + Sync>> {
    match super::send(task, crate::client::TaskType::ProfileInfo).await {
        Ok(v) => {
            match v {
                crate::client::TaskStatus::ProfileInfo(r) => Ok(r),
                _ => panic!("Wrong call in Profile info")
            }
        },
        Err(e) => Err(e)
    }
}

pub async fn set_profile_info(task: &TaskRequest, name: &str, avatar: Vec<u8>) -> Result<UpdateProfileInfoStatus, Box<dyn Error + Send + Sync>> {
    match super::send(task, crate::client::TaskType::UpdateProfileInfo { name: name.to_owned(), avatar }).await {
        Ok(v) => {
            match v {
                crate::client::TaskStatus::UpdateProfileInfo(r) => Ok(r),
                _ => panic!("Wrong call in Profile info")
            }
        },
        Err(e) => Err(e)
    }
}
