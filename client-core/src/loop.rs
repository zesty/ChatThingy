use tokio::{
    sync::{mpsc, oneshot, RwLock}, task::JoinHandle
};
use tracing::{warn, info};
use std::{error::Error, sync::Arc, fmt::Debug};
use zeroize::Zeroizing;
use hashbrown::HashMap;

use crate::database::SessionDB;

pub struct Task {
    pub typ: TaskType,
    pub resp: oneshot::Sender<TaskStatus>
}

#[derive(Debug)]
pub enum TaskType {
    Exit,
    GetProfiles,
    ActiveProfile,
    AddProfile(String, SecretPass),
    ChangeProfile(Option<(String, SecretPass)>),
}

#[derive(Clone)]
pub struct SecretPass(pub Zeroizing<String>);

impl Debug for SecretPass {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[=SECRET=]")
    }
}

#[derive(Debug)]
pub enum TaskStatus {
    NotConnected,

    GetProfiles(Vec<String>),
    ActiveProfile(Option<(String, mpsc::Sender<crate::client::Task>)>),
    AddProfile(crate::profile::AddProfile),
    ChangeProfile(crate::profile::ChangeProfile),
}

pub struct LoopSet {
    pub profile_tasks: Arc<RwLock<HashMap<String, Vec<JoinHandle<()>>>>>,
    pub sess_db: SessionDB,
    pub session_task_tx: Option<mpsc::Sender<crate::client::Task>>,
    pub session_name: Option<String>,
    pub work_dir: String
}

async fn suspend_profile_tasks(loop_set: &mut LoopSet) {
    if loop_set.profile_tasks.read().await.is_empty() {
        return;
    }

    // Aborting tasks
    let mut task = loop_set.profile_tasks.write().await;
    for (server, i) in task.iter_mut() {
        if server.is_empty() {
            info!("Abording main loop event handler for active profile");
        } else {
            info!("Abording tasks for server {}", server);
        }
        for task in i {
            task.abort();
        }
    }
    // Waiting for them to stop and removing them from hashmap
    for (_, i) in task.drain() {
        for task in i {
            task.await.ok();
        }
    }
}

#[tokio::main]
async fn start(mut loop_set: LoopSet, mut rx: mpsc::Receiver<Task>) -> Result<(), Box<dyn Error + Send + Sync>> {
    loop {
        match rx.recv().await {
            Some(task) => {
                info!("New request {:?}", task.typ);
                match &task.typ {
                    TaskType::Exit => {
                        // Doing a gracefull exit
                        suspend_profile_tasks(&mut loop_set).await;
                        break;
                    }
                    TaskType::GetProfiles => {
                        let db = loop_set.sess_db.clone();
                        tokio::task::spawn(async move {
                            crate::profile::get_all(db, task.resp).await;
                        });
                    },
                    TaskType::ActiveProfile => {
                        let profile = if loop_set.session_name.is_some() {
                            Some((loop_set.session_name.clone().unwrap(), loop_set.session_task_tx.clone().unwrap()))
                        } else {
                            None
                        };
                        task.resp.send(TaskStatus::ActiveProfile(profile)).ok();
                    },
                    TaskType::AddProfile(name, pass) => {
                        crate::profile::add_profile(&mut loop_set, name.clone(), pass.clone(), task.resp).await;
                    },
                    TaskType::ChangeProfile(ch) => {
                        // We first suspend all current profile tasks
                        suspend_profile_tasks(&mut loop_set).await;
                        // Then we change profile
                        if let Some((task_tx, name)) = crate::profile::change_profile(&mut loop_set, ch.clone(), task.resp).await {
                            loop_set.session_task_tx = Some(task_tx);
                            loop_set.session_name    = Some(name);
                        } else {
                            loop_set.session_task_tx = None;
                            loop_set.session_name    = None;
                        }
                    }
                }
            },
            None => {
                // Something went wrong exiting
                warn!("Something went wrong in main loop handler, stopping all profile worker tasks");
                let mut lock = loop_set.profile_tasks.write().await;
                for (_, i) in &mut lock.iter_mut() {
                    for task in i {
                        task.abort();
                    }
                }
                for (_, i) in lock.drain() {
                    for task in i {
                        task.await.ok();
                    }
                }
                break
            }
        }
    };


    Ok(())
}

#[tokio::main]
pub async fn init(home_dir: &str) -> Result<crate::task::profile::ProfileTaskRequest, Box<dyn Error + Send + Sync>> {
    let home_dir = home_dir.to_owned();

    let loop_set = LoopSet {
        profile_tasks: Arc::new(RwLock::new(HashMap::new())),
        sess_db: SessionDB::new(home_dir.as_str()).await?,
        session_task_tx: None,
        session_name: None,
        work_dir: home_dir
    };

    let (tx, rx) = mpsc::channel(100);

    std::thread::spawn(move || {
        start(loop_set, rx).ok();
        info!("Nero backend exited gracefully");
    });
    info!("Nero backend initialized");

    Ok(crate::task::profile::ProfileTaskRequest { tx })
}

