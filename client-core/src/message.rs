use nero_client_api::{SendMessage, PlainMessage, ChannelType};
use tokio::sync::{oneshot, mpsc};
use futures::stream::StreamExt;
use tracing::error;
use borsh::{BorshSerialize, BorshDeserialize};
use anyhow::Result;

use crate::{database::{message, contact::{self, ContactInfo}}, client::{ClientEvents, TaskStatus}};

#[repr(u8)]
#[derive(BorshSerialize, BorshDeserialize, Debug, Clone)]
pub enum Message {
    Plain(PlainMessage),
    NewPendingContactSource,
    NewPendingContactReceiver,
    ContactLeftGroup { source: u32 },
    NewMemberAdded { source: u32, dest: u32 },
    NewMemberAddedByMe { dest: u32 },
    DecryptionError,
    Unreadable
}

impl Default for Message {
    fn default() -> Self {
        Message::Unreadable
    }
}

impl Message {
    pub async fn from(e: &ClientEvents, server: u32, channel_type: ChannelType, source: u32, message: nero_client_api::Message) -> Result<Message> {
        Ok(match message {
            nero_client_api::Message::Plain(v) => Self::Plain(v),
            nero_client_api::Message::Unreadable => Self::Unreadable,
            nero_client_api::Message::DecryptionError => Self::DecryptionError,
            nero_client_api::Message::NewPendingContactSource => {
                if channel_type == ChannelType::Group {
                    panic!("Can't have NewPendingContactSource as group notification");
                }
                Self::NewPendingContactSource
            },
            nero_client_api::Message::NewPendingContactReceiver => {
                if channel_type == ChannelType::Group {
                    panic!("Can't have NewPendingContactReceiver as group notification");
                }
                Self::NewPendingContactReceiver
            },
            nero_client_api::Message::ContactLeftGroup => {
                if channel_type == ChannelType::Direct {
                    panic!("Can't have ContactLeftGroup as direct notification");
                }
                Self::ContactLeftGroup { source }
            },
            nero_client_api::Message::NewMemberAdded(dest) => {
                if channel_type == ChannelType::Direct {
                    panic!("Can't have NewMemberAdded as direct notification");
                }
                let ser_pk = dest.try_to_vec()?;
                let dest   = contact::get_contact_from_pk(&e.db, &ser_pk, server).await?;
                if source.eq(&0) {
                    Self::NewMemberAddedByMe { dest }
                } else {
                    Self::NewMemberAdded { source, dest }
                }
            }
        })
    }
}

pub enum MessageActivity {
    New(message::Message),
}

#[derive(Debug)]
pub enum ChannelFeed {
    ContactInfo(ContactInfo),
    Message(message::Message)
}

pub async fn activity(e: &ClientEvents, id: u32, channel_type: ChannelType, tx_resp: oneshot::Sender<TaskStatus>) {
    let (tx, rx) = mpsc::unbounded_channel();

    tx_resp.send(TaskStatus::MessageActivity(rx)).ok();

    *e.events.message.write().await = Some((channel_type, id, tx.clone()));
}

pub async fn get_last_messages(e: &ClientEvents, resp_channel: mpsc::Sender<ChannelFeed>,
                           id: u32, channel_type: ChannelType, ascending: bool, first_item: i64, page_item_number: usize,
                           grouping_time: i64, mut contact_loaded: Vec<u32>, tx: oneshot::Sender<TaskStatus>) {
    if channel_type == ChannelType::Group {
        // We are always source 0
        contact_loaded.push(0);
        let mut query = String::new();
        let mut citer = contact::get_contact_info_for_messages(&e.db, &mut query, id, ascending, first_item, page_item_number as u32, &contact_loaded).await;
        while let Some(Ok(next)) = citer.next().await {
            if resp_channel.send(ChannelFeed::ContactInfo(next)).await.is_err() {
                return;
            } 
        }
        e.events.group_chat_loaded_members.write().await.replace((channel_type.clone(), id, contact_loaded));
    }
    let mut miter = message::get_last_messages(&e.db, id, channel_type.clone(), ascending, first_item, page_item_number, grouping_time).await;
    tx.send(TaskStatus::GetLastMessages).ok();
    let mut last_id = 0;
    while let Some(Ok(next)) = miter.next().await {
        if next.message_id > last_id {
            last_id = next.message_id;
        }
        if resp_channel.send(ChannelFeed::Message(next)).await.is_err() {
            return;
        } 
    }
    if last_id != 0 {
        match message::update_last_read(&e.db, id, channel_type, last_id).await {
            Ok(_) => (),
            Err(e) => {
                error!("Error updating last read message: {}", e);
            }
        }
    }
}

pub async fn send(e: &ClientEvents, id: u32, channel_type: ChannelType, msg: String, tx_resp: oneshot::Sender<TaskStatus>) {
    let client;

    let status = match channel_type {
        ChannelType::Direct => {
            let (pk, server) = match contact::get_contact_pk_server(&e.db, id).await {
                Ok(v) => (v.0, v.1),
                Err(e) => {
                    tx_resp.send(TaskStatus::SendMessage(SendMessage::Err("Message unknewn destination".to_owned()), 0)).ok();
                    error!("Can't fetch user details to send message: {}", e);
                    return;
                }
            };

            if let Some(s) = e.servers.read().await.get(&server) {
                client = match s.client.clone() {
                    Some(v) => v,
                    None => {
                        tx_resp.send(TaskStatus::SendMessage(SendMessage::Err("Not in server".to_owned()), 0)).ok();
                        return;
                    }
                };
            } else {
                tx_resp.send(TaskStatus::SendMessage(SendMessage::Err("Message has no knewn destination".to_owned()), 0)).ok();
                error!("Send message for user {} is not in any server?", pk);
                return;
            }

            client.send_message(&pk, PlainMessage::Text { msg: msg.clone() }).await
        },
        ChannelType::Group => {
            if let Some(s) = e.servers.read().await.get(&id) {
                client = match s.client.clone() {
                    Some(v) => v,
                    None => {
                        tx_resp.send(TaskStatus::SendMessage(SendMessage::Err("Not in server".to_owned()), 0)).ok();
                        return;
                    }
                };
            } else {
                tx_resp.send(TaskStatus::SendMessage(SendMessage::Err("Message has no knewn destination".to_owned()), 0)).ok();
                return;
            }

            client.send_group_message(PlainMessage::Text { msg: msg.clone() }).await
        }
    };

    match status {
        SendMessage::Ok | SendMessage::NotSent => (),
        SendMessage::Err(_) => {
            tx_resp.send(TaskStatus::SendMessage(status, 0)).ok();
            return;
        }
    }

    let ser_msg = match Message::Plain(PlainMessage::Text { msg }).try_to_vec() {
        Ok(v) => v,
        Err(e) => {
            error!("Can't serialize sent message: {}", e);
            return;
        }
    };

    let timestamp = chrono::offset::Utc::now().timestamp();

    let ret = match channel_type {
        ChannelType::Direct => message::add(&e.db, id, timestamp, Some(false), &ser_msg).await,
        ChannelType::Group => message::add_group(&e.db, id, timestamp, Some(0), &ser_msg).await
    };
    match ret {
        Ok(v) => {
            tx_resp.send(TaskStatus::SendMessage(status, v)).ok();
        },
        Err(e) => {
            error!("Alas, message sent to couldn't be stored: {}", e)
        }
    }
}
