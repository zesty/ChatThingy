#![feature(new_uninit)]

mod error;
mod database;
mod r#loop;
mod profile;
mod client;
mod message;
mod contact;
pub mod task;
mod client_state;

pub use task::TaskRequest;
pub use r#loop::{init, SecretPass};
pub use message::{MessageActivity, ChannelFeed, Message as MessageType};
pub use client::{GroupsActivity, EditContact, RemoveContact, AddGroup, RemoveGroup, UpdateProfileInfoStatus};
pub use contact::ContactActivity;
pub use database::{
    message::{SenderType, Message},
    contact::Contact
};

pub use tokio::sync::mpsc::{Receiver, UnboundedReceiver};
pub use nero_client_api::{
    create_identity,
    AddMember, KeyPackage,
    AddContact,
    SendMessage,
    PlainMessage,
    ChannelType, LeaveGroup,
    NetworkStatus
};
pub use futures::executor::block_on;
