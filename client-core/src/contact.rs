use tracing::error;
use tokio::sync::{oneshot, mpsc};

use crate::{
    database::contact::{self, Contact},
    client::{TaskStatus, ClientEvents}
};

pub enum ContactActivity {
    DirectContactList(Vec<Contact>),
    New(Contact),
    NewMessage(u32, i64),
    Remove(Vec<u32>),
    Update { id: u32, name: String, avatar: Option<Vec<u8>> },
    /// Updating group contacts profile in active group conversation
    UpdateGroupContact { id: u32, name: String, avatar: Option<Vec<u8>> }
}

pub async fn activity(e: &ClientEvents, id: u32, tx_resp: oneshot::Sender<TaskStatus>) {
    let (tx, rx) = mpsc::unbounded_channel();

    // Sending all contacts first
    match contact::get_all(&e.db, id).await {
        Ok(v) => {
            // In order not to miss first broadcast
            // We need to send it as soon as we send channel
            tx_resp.send(TaskStatus::ContactActivity(rx)).ok();
            tx.send(ContactActivity::DirectContactList(v)).ok();
        },
        Err(e) => {
            tx_resp.send(TaskStatus::ContactActivity(rx)).ok();
            error!("Error reading contacts from database: {}", e);
        }
    }

    *e.events.contact.write().await = Some((id, tx.clone()));
}
