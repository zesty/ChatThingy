use std::sync::Arc;
use borsh::BorshSerialize;
use futures::executor::block_on;
use nero_client_api::{
    RustCrypto,
    ClientStateTrait, PublicKey, Result,
    async_trait, RatchetEncHeader, Zeroizing, ProfileUpdate
};
use tokio::sync::RwLock;

use crate::{database::{key_store, Database, contact, server, message, update}, profile::Profile};

pub struct NeroClientState {
    server_id: u32,
    profile: Arc<RwLock<Profile>>,
    db: Database,
    crypto: RustCrypto
}

impl NeroClientState {
    pub fn new(server_id: u32, db: Database, profile: Arc<RwLock<Profile>>) -> NeroClientState {
        NeroClientState { server_id, profile, db, crypto: RustCrypto::default() }
    }
}

#[async_trait]
impl ClientStateTrait for NeroClientState {
    async fn get_state(&self, dest: &PublicKey) -> Result<RatchetEncHeader> {
        let pk_ser  = dest.try_to_vec()?;
        let rat_bin = Zeroizing::new(contact::get_contact_ratchet(&self.db, self.server_id, &pk_ser).await?);
        let ratchet = match RatchetEncHeader::import(rat_bin.as_slice()) {
            Ok(v) => v,
            Err(_) => return Err(anyhow::Error::msg("Can't decode ratchet"))
        };

        Ok(ratchet)
    }

    async fn save_state(&self, dest: &PublicKey, ratchet: RatchetEncHeader) -> Result<()> {
        let pk_ser  = dest.try_to_vec()?;
        let rat_bin = match ratchet.export() {
            Ok(v) => Zeroizing::new(v),
            Err(_) => return Err(anyhow::Error::msg("Can't encode ratchet"))
        };

        contact::update_contact_ratchet(&self.db, self.server_id, &pk_ser, &rat_bin).await
    }

    async fn get_eph_sk(&self, eph_pk: &[u8; 32]) -> Result<Zeroizing<[u8; 32]>> {
        let key = key_store::eph_pop(&self.db, self.server_id, eph_pk).await?;
        let new: [u8; 32] = key.as_slice().try_into()?;

        Ok(Zeroizing::new(new))
    }

    async fn save_eph_sk_list(&self, eph_sk_list: &[([u8; 32], Zeroizing<[u8; 32]>)]) -> Result<()> {
        key_store::eph_insert(&self.db, self.server_id, eph_sk_list).await?;

        Ok(())
    }

    async fn get_group_state(&self) -> Result<Zeroizing<Vec<u8>>> {
        server::get_group_state(&self.db, self.server_id).await
    }
    
    async fn save_group_state(&self, state: &[u8]) -> Result<()> {
        server::update_group_state(&self.db, self.server_id, state).await
    }

    fn crypto_provider(&self) -> &RustCrypto {
        &self.crypto
    }

    fn store_group_key(&self, k: &[u8], v: &[u8]) -> Result<()> {
        block_on(key_store::set_group_key(&self.db, k, v))
    }
    
    fn read_group_key(&self, k: &[u8]) -> Result<Vec<u8>> {
        block_on(key_store::get_group_key(&self.db, k))
    }
    
    fn delete_group_key(&self, _k: &[u8]) -> Result<()> {
        //block_on(key_store::delete_group_key(&self.db, k))
        // TODO: For some reason, Openmls sometimes delete group key and sometimes not
        // So we end up being not able to rejoin group
        Ok(())
    }

    async fn save_pending_message(&self, msg: Vec<u8>) -> Result<()> {
        message::add_pending_message(&self.db, &msg).await
    }

    async fn get_pending_message(&self) -> Result<(u32, Vec<u8>)> {
        message::get_pending_message(&self.db).await
    }

    async fn delete_pending_message(&self, index: u32) -> Result<()> {
        message::delete_pending_message(&self.db, index).await
    }

    async fn save_pending_task(&self, msg: Vec<u8>) -> Result<()> {
        update::add_pending(&self.db, &msg).await
    }

    async fn get_pending_task(&self) -> Result<(u32, Vec<u8>)> {
        update::get_pending(&self.db).await
    }

    async fn delete_pending_task(&self, index: u32) -> Result<()> {
        update::delete_pending(&self.db, index).await
    }

    async fn get_profile_update(&self) -> Result<Option<ProfileUpdate>> {
        update::get_profile_update_secret(&self.db, self.server_id).await
    }
    
    async fn save_profile_update(&self, update: &ProfileUpdate) -> Result<()> {
        update::update_profile_update_secret(&self.db, self.server_id, update).await
    }

    async fn last_update_height(&self, pk: &PublicKey) -> Result<u32> {
        let pk_ser = pk.try_to_vec()?;
        contact::get_contact_update_height(&self.db, &pk_ser, self.server_id).await
    }

    async fn last_profile_update_height(&self) -> Result<u32> {
        match self.profile.read().await.height {
            Some(v) => Ok(v),
            None    => Err(anyhow::Error::msg("No profile update done yet"))
        }
    }
}
