use std::{sync::Arc, str::FromStr};
use borsh::BorshSerialize;
use tokio::{
    sync::{RwLock, oneshot, mpsc},
    task::JoinHandle,
    time::Duration
};
use tracing::{info, error};
use hashbrown::HashMap;

use common::keys::PublicKey;
use crate::{
    database::{Database, server, contact, message, update},
    error::ChatError,
    ContactActivity, client_state::NeroClientState, profile::Profile, SenderType, message::{ChannelFeed, Message},
    MessageActivity
};
use nero_client_api::{
    Client, ResumePoint, Context, AddContact,
    AddMember, KeyPackage,
    TlsDeserializeTrait, SendMessage,
    JoinStatus, ProfileUpdateStatus, PROFILE_NAME_TOO_LONG, AVATAR_SIZE_TOO_BIG, ChannelType, LeaveGroup, ClientState, NetworkStatus
};

static PROFILE_UPDATE_INTERVAL: Duration = Duration::from_secs(10);

pub struct Task {
    pub typ: TaskType,
    pub resp: oneshot::Sender<TaskStatus>
}

#[derive(Debug)]
pub enum TaskType {
    ChangeNetworkStatus { status: bool },
    ProfileInfo,
    UpdateProfileInfo { name: String, avatar: Vec<u8> },
    AddGroup { host: String, port: u16 },
    RejoinGroup { id: u32 },
    LeaveGroup { id: u32, delete: bool },
    RemoveGroup(String),
    GetGroupInfo { id: u32 },
    GroupsActivity,
    ContactActivity(u32),
    AddContact { server: u32, pk: String },
    EditContact(u32, String),
    RemoveContact(Vec<u32>),
    AddMember { server: u32, key_package: String },
    SendMessage { id: u32, channel_type: ChannelType, msg: String },
    GetLastMessages {
        resp_channel: mpsc::Sender<ChannelFeed>,
        id: u32,
        channel_type: ChannelType,
        ascending: bool,
        first_item: i64,
        page_item_number: usize,
        grouping_time: i64,
        loaded_contacts: Vec<u32>
    },
    MessageActivity { id: u32, channel_type: ChannelType }
}

#[derive(Debug)]
pub enum TaskStatus {
    ProfileInfo(ProfileInfo),
    UpdateProfileInfo(UpdateProfileInfoStatus),
    AddGroup(AddGroup),
    LeaveGroup(LeaveGroup),
    RemoveGroup(RemoveGroup),
    GetGroupInfo(Option<GroupInfo>),
    GroupsActivity(mpsc::UnboundedReceiver<crate::client::GroupsActivity>),
    ContactActivity(mpsc::UnboundedReceiver<ContactActivity>),
    AddContact(AddContact),
    EditContact(crate::client::EditContact),
    RemoveContact(crate::client::RemoveContact),
    AddMember(nero_client_api::AddMember),
    SendMessage(SendMessage, i64),
    GetLastMessages,
    MessageActivity(mpsc::UnboundedReceiver<MessageActivity>)
}

pub struct Group {
    pub server: String,
    pub last_id: i64,
    pub last_group_id: i64,
    pub update_height: Option<u32>,
    pub client: Option<Client>,
    pub activity: Option<mpsc::UnboundedReceiver<nero_client_api::Activity>>
}

pub struct Activity {
    pub contact: RwLock<Option<(u32, mpsc::UnboundedSender<ContactActivity>)>>,
    pub message: RwLock<Option<(ChannelType, u32, mpsc::UnboundedSender<MessageActivity>)>>,
    pub group_chat_loaded_members: RwLock<Option<(ChannelType, u32, Vec<u32>)>>
}

impl Activity {
    pub fn new() -> Activity {
        Activity {
            contact: RwLock::new(None),
            message: RwLock::new(None),
            group_chat_loaded_members: RwLock::new(None)
        }
    }
}

pub struct ClientEvents {
    pub ctx: Context,
    pub profile: Arc<RwLock<Profile>>,
    pub db: Database,
    pub servers: RwLock<HashMap<u32, Group>>,
    pub tasks: Arc<RwLock<HashMap<String, Vec<JoinHandle<()>>>>>,
    pub profile_update_task: RwLock<Option<JoinHandle<()>>>,
    pub events: Activity
}

pub struct ClientEventHandler {}

impl ClientEventHandler {
    pub async fn init(e: Arc<ClientEvents>) -> Result<(), ChatError> {
        let db          = e.db.clone();
        let mut servers = match server::get_all(&db).await {
            Ok(v) => v,
            Err(_) => return Err(ChatError::Database)
        };


        let servers_list = servers.drain(0..).map(|x| {
            let (client, activity) = if x.last_id == -1 {
                (None, None)
            } else {
                let (a, b) = e.ctx.instance_client(&x.server,
                   ResumePoint { direct: x.last_id, group: x.last_group_id },
                   Arc::new(Box::new(NeroClientState::new(x.id, e.db.clone(), e.profile.clone()))));

                (Some(a), Some(b))
            };

            (x.id, Group {
                server: x.server,
                last_id: x.last_id,
                last_group_id: x.last_group_id,
                update_height: x.update_height,
                client,
                activity
            })
        }).collect();

        ClientEventHandler::spawn_server_tasks(e, servers_list).await;

        Ok(())
    }

    pub async fn events_loop(mut channel: mpsc::Receiver<Task>, e: Arc<ClientEvents>) {
        while let Some(v) = channel.recv().await {
            info!("New profile request {:?}", v.typ);
            let e_clone = e.clone();
            tokio::spawn(async move {
                match v.typ {
                    TaskType::ChangeNetworkStatus { status } => change_network_status(&e_clone, status).await,
                    TaskType::ProfileInfo => profile_info(&e_clone, v.resp).await,
                    TaskType::UpdateProfileInfo { name, avatar } => update_profile_info(e_clone, name, avatar, v.resp).await,
                    TaskType::AddGroup { host, port } => add_group(e_clone, AddGroupArgs::Add { host, port }, v.resp).await,
                    TaskType::RejoinGroup { id } => add_group(e_clone, AddGroupArgs::Rejoin { id }, v.resp).await,
                    TaskType::LeaveGroup { id, delete } => leave_group(&e_clone, id, delete, v.resp).await,
                    TaskType::RemoveGroup(server) => remove_group(&e_clone, &server, v.resp).await,
                    TaskType::GroupsActivity => get_group_activity(e_clone, v.resp).await,
                    TaskType::GetGroupInfo { id } => get_group_info(&e_clone, id, v.resp).await,
                    TaskType::AddContact { server, pk } => add_contact(&e_clone, server, &pk, v.resp).await,
                    TaskType::EditContact(id, name) => edit_contact(&e_clone, id, &name, v.resp).await,
                    TaskType::RemoveContact(id) => remove_contact(&e_clone, &id, v.resp).await,
                    TaskType::ContactActivity(id) => crate::contact::activity(&e_clone, id, v.resp).await,
                    TaskType::SendMessage { id, channel_type, msg } => crate::message::send(&e_clone, id, channel_type, msg, v.resp).await,
                    TaskType::GetLastMessages { resp_channel, id, channel_type, ascending, first_item, page_item_number, grouping_time, loaded_contacts } => {
                        crate::message::get_last_messages(&e_clone, resp_channel, id, channel_type, ascending, first_item, page_item_number, grouping_time, loaded_contacts, v.resp).await;
                    },
                    TaskType::MessageActivity { id, channel_type } => {
                        crate::message::activity(&e_clone, id, channel_type, v.resp).await;
                    },
                    TaskType::AddMember { server, key_package } => add_member(&e_clone, server, &key_package, v.resp).await
                }
            });
        }
    }

    async fn spawn_server_tasks(e: Arc<ClientEvents>, servers_list: Vec<(u32, Group)>) {
        let mut needs_profile_update   = false;
        let last_profile_update_height = e.profile.read().await.height;
        for (server, server_info) in servers_list {
            let is_active = server_info.client.is_some();
            // We are checking if all servers have our latest profile update
            // If one of them doesn't we need to synchronise!!
            let height = server_info.update_height;
            if is_active && last_profile_update_height.is_some() {
                let profile_height = last_profile_update_height.clone().unwrap();
                match height {
                    Some(h) => if profile_height > h {
                        needs_profile_update = true;
                    },
                    None => needs_profile_update = true
                }
            }

            let server_addr = server_info.server.clone();
            {
                let mut lock = e.servers.write().await;
                lock.insert(server, server_info);
            }
            if is_active {
                let mut lock       = e.tasks.write().await;
                let mut serv_tasks = vec![];
                let e_clone        = e.clone();
                serv_tasks.push(tokio::spawn(async move {
                    ClientEventHandler::event_reader(e_clone, server).await;
                }));
                lock.insert(server_addr, serv_tasks);
            }
        }

        if needs_profile_update {
            let e_clone = e.clone();
            *e.profile_update_task.write().await = Some(tokio::spawn(async move {
                ClientEventHandler::sync_profile_update(e_clone, last_profile_update_height.unwrap()).await;
            }));
        }
    }

    async fn sync_profile_update(e: Arc<ClientEvents>, height: u32) {
        info!("Synchronising profile updates with all groups");

        loop {
            let mut needs_update = Vec::new();
            for (server, server_info) in e.servers.read().await.iter() {
                if server_info.client.is_some() {
                    match server_info.update_height {
                        Some(h) => if height > h {
                            needs_update.push((server.clone(), *server, server_info.client.clone().unwrap()));
                        },
                        None => {
                            needs_update.push((server.clone(), *server, server_info.client.clone().unwrap()));
                        }
                    }
                }
            }

            if needs_update.is_empty() {
                break;
            }

            let (name, avatar) = {
                let profile = e.profile.read().await;
                (profile.name.clone(), profile.avatar.clone())
            };
            let mut failed = false;
            for server in needs_update {
                let mut failed_attempts = 0;
                loop {
                    match server.2.update_profile(&name, avatar.clone()).await {
                        ProfileUpdateStatus::Ok(height) => {
                            crate::database::server::update_last_height(&e.db, server.1, height).await.ok();
                            if let Some(s) = e.servers.write().await.get_mut(&server.0) {
                                s.update_height.replace(height);
                            }
                            break;
                        },
                        ProfileUpdateStatus::ServerUnreachable => {
                            // We can't reach server
                            // We will skip it for now
                            failed = true;
                            continue;
                        },
                        ProfileUpdateStatus::Failed => {
                            failed_attempts += 1;
                            if failed_attempts >= 3 {
                                break;
                            }
                            tokio::time::sleep(PROFILE_UPDATE_INTERVAL).await;
                        },
                        ProfileUpdateStatus::Skip | ProfileUpdateStatus::NameTooLong | ProfileUpdateStatus::AvatarSizeTooBig => {
                            // We can't recover
                            // We'll mark server as if it is in last height
                            crate::database::server::update_last_height(&e.db, server.1, height).await.ok();
                            if let Some(s) = e.servers.write().await.get_mut(&server.0) {
                                s.update_height.replace(height);
                            }
                            break;
                        }
                    }
                }
            }

            if failed {
                tokio::time::sleep(PROFILE_UPDATE_INTERVAL).await;
            } else {
                break;
            }
        }

        info!("Synchronising profile updates with finished!");
    }

    async fn event_reader(e: Arc<ClientEvents>, server_id: u32) {
        let mut listener;
        if let Some(s) = e.servers.write().await.get_mut(&server_id) {
            listener  = s.activity.take().expect("Should have listener at this point");
        } else {
            return;
        }

        while let Some(activity) = listener.recv().await {
            match activity {
                nero_client_api::Activity::Message(activity) => message_activity(e.clone(), server_id, activity).await,
                nero_client_api::Activity::User(activity) => user_activity(e.clone(), server_id, activity).await,
                nero_client_api::Activity::Contact(nero_client_api::ContactActivity::Add(pk, first_reached)) => {
                    match contact::add_contact_list(&e.db, first_reached, &pk.try_to_vec().unwrap(), server_id).await {
                        Ok(ct) => {
                            let contact_events = e.events.contact.read().await.clone();
                            if let Some(ct_target) = contact_events {
                                if !ct_target.0.eq(&server_id) {
                                    return;
                                }
                                ct_target.1.send(ContactActivity::New(crate::Contact {
                                    id: ct.id,
                                    name: ct.name,
                                    avatar: ct.avatar,
                                    not_read: 0,
                                    pending: Some(chrono::offset::Utc::now().timestamp()),
                                    first_reached,
                                    last_timestamp: None
                                })).ok();
                            }
                        },
                        Err(e) => {
                            error!("{}", e);
                        }
                    }
                },
                _ => {}
            }
        }
    }
}

async fn user_activity(e: Arc<ClientEvents>, server_id: u32, activity: nero_client_api::UserActivity) {
    match activity {
        nero_client_api::UserActivity::Add(pk) => {
            if let Err(e) = contact::add(&e.db, &pk.export(), &pk.try_to_vec().unwrap(), server_id).await {
                error!("Adding new contact {} failed: {}", pk, e);
            }
            // TODO: For some reason there is a race condition with new member notification
            tokio::time::sleep(std::time::Duration::from_millis(100)).await;
        },
        nero_client_api::UserActivity::Update(pk, height, update) => {
            if let Ok(ser_pk) = pk.try_to_vec() {
                let avatar = if update.avatar.len() == 0 { None } else { Some(update.avatar) };
                match contact::set_update_info(&e.db, &ser_pk, server_id, height, &update.name, &avatar).await {
                    Ok(c) => if c.is_direct_contact {
                        if let Some((sid, contact_events)) = e.events.contact.read().await.clone() {
                            if server_id.eq(&sid) {
                                contact_events.send(ContactActivity::Update { id: c.id, name: update.name, avatar }).ok();
                            }
                        }
                    } else {
                        if let Some(loaded) = e.events.group_chat_loaded_members.read().await.clone() {
                            if loaded.0 == ChannelType::Group && loaded.1.eq(&server_id) && loaded.2.contains(&c.id) {
                                if let Some((sid, contact_events)) = e.events.contact.read().await.clone() {
                                    if sid == loaded.1 {
                                        contact_events.send(ContactActivity::UpdateGroupContact { id: c.id, name: update.name, avatar }).ok();
                                    }
                                }
                            }
                        }
                    },
                    Err(e) => error!("Updating user info failed for {}: {}", pk, e)
                }
            }
        },
        _ => {}
    }
}

async fn message_activity(e: Arc<ClientEvents>, server_id: u32, activity: nero_client_api::MessageActivity) {
    match activity {
        nero_client_api::MessageActivity::Text(msg) => {
            if msg.id > 0 {
                let (last_id, last_group_id) = match msg.ch_type {
                    ChannelType::Direct => (Some(msg.id), None),
                    ChannelType::Group  => (None, Some(msg.id))
                };
                let serv_id = server_id;
                let e_clone = e.clone();
                tokio::spawn(async move {
                    if let Some(s) = e_clone.servers.write().await.get_mut(&serv_id) {
                        let ulast_id = match last_id {
                            Some(v) => v,
                            None    => s.last_id
                        };
                        let ulast_group_id = match last_group_id {
                            Some(v) => v,
                            None    => s.last_group_id
                        };
                        s.last_id       = ulast_id;
                        s.last_group_id = ulast_group_id;
                        drop(s);

                        server::update_last_id(&e_clone.db, serv_id, ulast_id, ulast_group_id).await.ok();
                    }
                });
            }

            let id = if e.ctx.keypair().public_key().eq(&msg.source) {
                0
            } else {
                let ser_pk = match msg.source.try_to_vec() {
                    Ok(v) => v,
                    Err(e) => {
                        error!("We can't serialize {}: {}", msg.source, e);
                        return;
                    },
                };

                match contact::get_contact_from_pk(&e.db, &ser_pk, server_id).await {
                    Ok(v) => v,
                    Err(e) => {
                        error!("Couldn't fetch {} id from database: {}", msg.source, e);
                        return;
                    }
                }
            };

            let message = match Message::from(&e, server_id, msg.ch_type.clone(), id, msg.content).await {
                Ok(v) => v,
                Err(e) => {
                    error!("Couldn't transform message: {}", e);
                    return;
                }
            };
            let ser_msg = match message.try_to_vec() {
                Ok(v)  => v,
                Err(e) => {
                    error!("Couldn't serialize message from {}: {}", msg.source, e);
                    return;
                }
            };
            
            let source;
            let add_res = match msg.ch_type {
                ChannelType::Direct => {
                    let store_source;
                    (store_source, source) = match message {
                        Message::NewPendingContactSource
                            | Message::NewPendingContactReceiver => (None, SenderType::Notification),
                        _ => (Some(true), SenderType::Contact)
                    };
                    message::add(&e.db, id, msg.timestamp, store_source, &ser_msg).await
                }
                ChannelType::Group => {
                    let store_source;
                    (store_source, source) = match message {
                        Message::Plain(_) => (Some(id), SenderType::User { id }),
                        _ => (None, SenderType::Notification)
                    };

                    match message::add_group(&e.db, server_id, msg.timestamp, store_source, &ser_msg).await {
                        Ok(id) => {
                            match &message {
                                Message::ContactLeftGroup { source } => {
                                    message::add_group_notification(&e.db, id, *source, None).await.ok();
                                },
                                Message::NewMemberAdded { source, dest } => {
                                    message::add_group_notification(&e.db, id, *source, Some(*dest)).await.ok();
                                },
                                Message::NewMemberAddedByMe { dest } => {
                                    message::add_group_notification(&e.db, id, 0, Some(*dest)).await.ok();
                                },
                                _ => ()
                            }
                            Ok(id)
                        },
                        Err(e) => Err(e)
                    }
                }
            };
            match add_res {
                Ok(v) => {
                    let contact_events = e.events.contact.read().await.clone();
                    if let Some(ct_target) = contact_events {
                        if !ct_target.0.eq(&server_id) {
                            return;
                        }
                        ct_target.1.send(ContactActivity::NewMessage(id, msg.timestamp)).ok();
                    }
                    if let Some((channel_type, sid, message_notif)) = e.events.message.read().await.clone() {
                        if msg.ch_type == channel_type && server_id.eq(&sid) {
                            message_notif.send(MessageActivity::New(crate::Message {
                                message_id: v,
                                content: message,
                                source,
                                timestamp: msg.timestamp
                            })).ok();
                        }
                        message::update_last_read(&e.db, id, msg.ch_type, v).await.ok();
                    }
                }
                Err(e) => {
                    error!("Alas, message received from {} couldn't be stored: {}", msg.source, e);
                }
            }
        }
    }
}

async fn change_network_status(e: &ClientEvents, status: bool) {
    let mut network = e.ctx.network().write().await;
    match status {
        true  => network.start().await,
        false => network.stop().await
    }
}

#[derive(Debug)]
pub struct ProfileInfo {
    pub key_package: String,
    pub identity: String,
    pub name: String,
    pub avatar: Vec<u8>
}

async fn profile_info(e: &ClientEvents, tx: oneshot::Sender<TaskStatus>) {
    let (key_package, identity, name, avatar) = {
        let profile = e.profile.read().await;
        (profile.key_package.clone(), profile.identity.clone(), profile.name.clone(), profile.avatar.clone())
    };

    tx.send(TaskStatus::ProfileInfo(ProfileInfo { key_package, identity, name, avatar })).ok();
}


#[derive(Debug)]
pub enum UpdateProfileInfoStatus {
    Ok,
    NameTooLong,
    AvatarSizeTooBig,
    Err(String)
}

async fn update_profile_info(e: Arc<ClientEvents>, name: String, avatar: Vec<u8>, tx: oneshot::Sender<TaskStatus>) {
    if name.len() > PROFILE_NAME_TOO_LONG {
        tx.send(TaskStatus::UpdateProfileInfo(UpdateProfileInfoStatus::NameTooLong)).ok();
        return;
    } else if avatar.len() > AVATAR_SIZE_TOO_BIG {
        tx.send(TaskStatus::UpdateProfileInfo(UpdateProfileInfoStatus::AvatarSizeTooBig)).ok();
        return;
    }

    // we first stop profile update task if there is one
    if let Some(utask) = e.profile_update_task.write().await.take() {
        utask.abort();
        utask.await.ok();
    }

    // Next we update profile locally
    match update::set_profile_info(&e.db, &name, &avatar).await {
        Ok(_) => { tx.send(TaskStatus::UpdateProfileInfo(UpdateProfileInfoStatus::Ok)).ok(); },
        Err(e) => {
            error!("Error updating profile info: {}", e);
            tx.send(TaskStatus::UpdateProfileInfo(UpdateProfileInfoStatus::Err(e.to_string()))).ok();
        }
    }

    let mut lock = e.profile.write().await;
    lock.name    = name;
    lock.avatar  = avatar;
    let height   = match lock.height {
        Some(v) => v+1,
        None    => 0
    };
    lock.height  = Some(height);
    drop(lock);

    // lastly we launch a new update task to broadcast update to all groups
    let e_clone = e.clone();
    *e.profile_update_task.write().await = Some(tokio::spawn(async move {
        ClientEventHandler::sync_profile_update(e_clone, height).await;
    }));
}

#[derive(Debug)]
pub enum AddGroup {
    Ok,
    NotInvited,
    ServerUnreachable,
    Error(String)
}

#[derive(Debug)]
pub enum RemoveGroup {
    Ok,
    Error(String)
}

async fn remove_group(e: &ClientEvents,
                       server: &str, tx: oneshot::Sender<TaskStatus>) {
}

async fn leave_group(e: &ClientEvents, server: u32, delete: bool, tx: oneshot::Sender<TaskStatus>) {
    
    let client = match e.servers.read().await.get(&server) {
        Some(v) => match v.client.clone() {
            Some(c) => Some(c),
            None => {
                if !delete {
                    tx.send(TaskStatus::LeaveGroup(LeaveGroup::Err("Already left group".to_string()))).ok();
                    return;
                }
                None
            }
        },
        None => {
            tx.send(TaskStatus::LeaveGroup(LeaveGroup::Err("Server not found".to_string()))).ok();
            return;
        }
    };

    if let Some(client) = client {
        let status = client.leave_group().await;

        match status {
            LeaveGroup::Ok => {
                if let Some(s) = e.servers.write().await.get_mut(&server) {
                    s.client        = None;
                    s.last_id       = -1;
                    s.last_group_id = -1;
                    if let Some(tasks) = e.tasks.write().await.remove(&s.server) {
                        info!("Aborting {} tasks after leaving group", s.server);
                        for task in tasks {
                            task.abort();
                        }
                    }
                }
                crate::database::server::update_last_id(&e.db, server, -1, -1).await.ok();
                crate::database::server::delete_group_state(&e.db, server).await.ok();
            },
            _ => ()
        }

        if !delete || status != LeaveGroup::Ok {
            tx.send(TaskStatus::LeaveGroup(status)).ok();
            return;
        }
    }
    
    // Now we remove group completely from database
    e.servers.write().await.remove(&server);
    if let Err(e) = server::remove(&e.db, server).await {
        tx.send(TaskStatus::LeaveGroup(LeaveGroup::Err(format!("Can't remove group: {}", e)))).ok();
        return;
    }

    tx.send(TaskStatus::LeaveGroup(LeaveGroup::Ok)).ok();
}

enum AddGroupArgs {
    Add { host: String, port: u16 },
    Rejoin { id: u32 }
}

async fn add_group(e: Arc<ClientEvents>,
                    args: AddGroupArgs,
                    tx: oneshot::Sender<TaskStatus>) {
    // TODO
    /*if !host.ends_with(".onion") {
        tx.send(TaskStatus::AddServer(AddServer::Error(format!("Unknewn network type for '{}'", host)))).ok();
        return;
    }*/
    
    let server;

    let mut rejoin = None;
    match args {
        AddGroupArgs::Add { host, port } => {
            server = format!("{}:{}", host, port);
            for serv in e.servers.read().await.iter() {
                if serv.1.server.eq(&server) {
                    tx.send(TaskStatus::AddGroup(AddGroup::Error(format!("Server {} already added", server)))).ok();
                    return;
                }
            }
        },
        AddGroupArgs::Rejoin { id } => {
            if let Some(s) = e.servers.read().await.get(&id) {
                server = s.server.clone();
            } else {
                tx.send(TaskStatus::AddGroup(AddGroup::Error("Can't rejoin non existing group".to_owned()))).ok();
                return;
            }
            rejoin = Some(id);
        }
    }

    match e.ctx.join_group(&server).await {
        JoinStatus::Ok => (),
        JoinStatus::NotInvited => {
            tx.send(TaskStatus::AddGroup(AddGroup::NotInvited)).ok();
            return;
        },
        JoinStatus::ServerUnreachable => {
            tx.send(TaskStatus::AddGroup(AddGroup::ServerUnreachable)).ok();
            return;
        }
    }

    let add = match rejoin {
        None => crate::database::server::add_server(&e.db, &server).await,
        Some(id) => crate::database::server::update_last_id(&e.db, id, 0, 0).await
            .and(Ok(id))
    };

    match add {
        Ok(id) => {
            tx.send(TaskStatus::AddGroup(AddGroup::Ok)).ok();
            // Now we spawn tasks for server
            let state: ClientState = Arc::new(Box::new(NeroClientState::new(id, e.db.clone(), e.profile.clone())));
            let (client, activity) = e.ctx.instance_client(&server,
                                                           ResumePoint { direct: 0, group: 0 },
                                                           state);
            let server_info = Group {
                server,
                last_id: 0,
                last_group_id: 0,
                update_height: None,
                client: Some(client),
                activity: Some(activity)
            };
            ClientEventHandler::spawn_server_tasks(e, vec![(id, server_info)]).await;
        },
        Err(e) => {
            tx.send(TaskStatus::AddGroup(AddGroup::Error(format!("Fatal error when storing server {} session info to local database: {}", server, e)))).ok();
        }
    }
}

#[derive(Debug)]
pub struct GroupsActivity {
    pub network_status: NetworkStatus,
    pub groups: Vec<(u32, String, i64)>,
}

async fn write_group_activity(e: &ClientEvents, tx: &mpsc::UnboundedSender<GroupsActivity>) {
    let network_status = e.ctx.network().write().await.status().await;

    let mut activity = Vec::new();
    for i in e.servers.read().await.iter() {
        let last_seen = match i.1.client.clone() {
            Some(client) => client.last_seen().await,
            None => -1
        };
        activity.push((*i.0, i.1.server.clone(), last_seen));
    }
    tx.send(GroupsActivity { network_status, groups: activity }).ok();
}

async fn get_group_activity(e: Arc<ClientEvents>, tx: oneshot::Sender<TaskStatus>) {
    let (tx1, rx) = mpsc::unbounded_channel();

    // Sending initial activity here so we get results quicker on frontend
    write_group_activity(&e, &tx1).await;

    tx.send(TaskStatus::GroupsActivity(rx)).ok();

    let e_clone = e.clone();
    let handle = tokio::spawn(async move {
        let mut interval = tokio::time::interval(Duration::from_secs(2));
        loop {
            interval.tick().await;
            write_group_activity(&e_clone, &tx1).await;
        };
    });

    let mut lock   = e.tasks.write().await;
    if let Some(p) = lock.get_mut("") {
        p.push(handle);
    } else {
        lock.insert("".to_string(), vec![handle]);
    }
}

#[derive(Debug)]
pub struct GroupInfo {
    pub host: String,
    pub port: u16,
    pub last_seen: i64
}

async fn get_group_info(e: &ClientEvents, server_id: u32, tx: oneshot::Sender<TaskStatus>) {
    let mut info = None;
    if let Some(s) = e.servers.read().await.get(&server_id) {
        let host;
        let port;

        let spl = s.server.split(":").collect::<Vec<&str>>();
        host    = spl[0].to_owned();
        port    = u16::from_str(spl[1]).expect("Couldn't parse port from url");
        
        info = Some(GroupInfo {
            host,
            port,
            last_seen: match s.client.clone() {
                Some(client) => client.last_seen().await,
                None => -1
            }
        });
    }
    tx.send(TaskStatus::GetGroupInfo(info)).ok();
}

async fn add_contact(e: &ClientEvents, server_id: u32,
                     pk: &str, tx: oneshot::Sender<TaskStatus>) {
    let pk = match PublicKey::import(pk) {
        Ok(v) => v,
        Err(_) => {
            tx.send(TaskStatus::AddContact(AddContact::InvalidKey)).ok();
            return;
        }
    };

    let status = if let Some(s) = e.servers.read().await.get(&server_id) {
        match s.client.clone() {
            Some(client) => client.add_contact(&pk).await,
            None => AddContact::Err("Not in group".to_string())
        }
    } else {
        AddContact::Err("Server not found".to_string())
    };
    tx.send(TaskStatus::AddContact(status)).ok();
}

#[derive(Debug)]
pub enum EditContact {
    Ok,
    Err(String)
}

async fn edit_contact(e: &ClientEvents, id: u32, name: &str, tx: oneshot::Sender<TaskStatus>) {
}

#[derive(Debug)]
pub enum RemoveContact {
    Ok,
    Err(String)
}

async fn remove_contact(e: &ClientEvents, id: &[u32], tx: oneshot::Sender<TaskStatus>) {
}

async fn add_member(e: &ClientEvents, server_id: u32, key_package: &str, resp_channel: oneshot::Sender<TaskStatus>) {
    let key_package = match base64::decode(key_package) {
        Ok(v) => {
            match KeyPackage::tls_deserialize(&mut v.as_slice()) {
                Ok(v) => Some(v),
                Err(_) => None
            }
        },
        Err(_) => None,
    };

    let key_package = match key_package {
        Some(v) => v,
        None => {
            resp_channel.send(TaskStatus::AddMember(AddMember::InvalidKey)).ok();
            return;
        }
    };

    let client;
    if let Some(s) = e.servers.read().await.get(&server_id) {
        client = match s.client.clone() {
            Some(v) => v,
            None => {
                resp_channel.send(TaskStatus::AddMember(AddMember::Err("Not in group".to_string()))).ok();
                return;
            }
        };
    } else {
        resp_channel.send(TaskStatus::AddMember(AddMember::ServerUnreachable)).ok();
        return;
    }

    resp_channel.send(TaskStatus::AddMember(client.add_member(key_package).await)).ok();
}
