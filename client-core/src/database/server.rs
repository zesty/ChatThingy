use super::Database;
use anyhow::Result;
use nero_client_api::Zeroizing;

#[derive(sqlx::FromRow, Debug)]
struct AddServer {
    id: u32
}

#[derive(sqlx::FromRow, Debug)]
pub struct Server {
    pub id: u32,
    pub server: String,
    pub last_id: i64,
    pub last_group_id: i64,
    pub update_height: Option<u32>
}

#[derive(sqlx::FromRow, Debug)]
pub struct GroupState {
    state: Vec<u8>
}


pub async fn add_server(con: &Database, server: &str) -> Result<u32> {
    let ret = sqlx::query_as::<_, AddServer>(r#"insert into server(server, last_id, last_group_id) values (?1, ?2, 0) returning id"#)
        .bind(server)
        .bind(0_i64)
        .fetch_one(&con.con)
        .await?;

    Ok(ret.id)
}

pub async fn update_last_id(con: &Database, server_id: u32, last_id: i64, last_group_id: i64) -> Result<()> {
    if sqlx::query(r#"update server set last_id = ?1, last_group_id = ?2 where id = ?3"#)
        .bind(last_id)
        .bind(last_group_id)
        .bind(server_id)
        .execute(&con.con)
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("updating last id failed"));
    }
    Ok(())
}

pub async fn get_all(con: &Database) -> Result<Vec<Server>> {
    let rez = sqlx::query_as::<_, Server>(r#"select id, server, last_id, last_group_id, update_height from server"#)
        .fetch_all(&con.con)
        .await?;

    Ok(rez)
}

pub async fn update_last_height(con: &Database, id: u32, update_height: u32) -> Result<()> {
    if sqlx::query(r#"update server set update_height = ?1 where id = ?2"#)
        .bind(update_height)
        .bind(id)
        .execute(&con.con)
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("updating last update height failed"));
    }
    Ok(())
}

pub async fn remove(con: &Database, id: u32) -> Result<()> {
    if sqlx::query(r#"delete from server where id = ?1"#)
        .bind(id)
        .execute(&con.con)
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg(format!("removing group with id {} failed", id)));
    }
    Ok(())
}

pub async fn delete_group_state(con: &Database, server_id: u32) -> Result<()> {
    if sqlx::query(r#"update server set state = null where id = ?1"#)
        .bind(server_id)
        .execute(&con.con)
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("Error deleting group state in database"));
    }
    Ok(())
}

pub async fn update_group_state(con: &Database, server_id: u32, state: &[u8]) -> Result<()> {
    if sqlx::query(r#"update server set state = ?1 where id = ?2"#)
        .bind(state)
        .bind(server_id)
        .execute(&con.con)
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("Error updating group state in database"));
    }
    Ok(())
}

pub async fn get_group_state(con: &Database, server_id: u32) -> Result<Zeroizing<Vec<u8>>> {
    let rez = sqlx::query_as::<_, GroupState>(r#"select state from server where id = ?1 and state not null"#)
        .bind(server_id)
        .fetch_one(&con.con)
        .await?;
    Ok(Zeroizing::new(rez.state))
}
