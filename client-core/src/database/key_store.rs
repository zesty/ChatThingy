use super::Database;
use nero_client_api::Zeroizing;
use anyhow::Result;

#[derive(sqlx::FromRow)]
pub struct EphSk {
    pub eph_sk: Vec<u8>
}

#[derive(sqlx::FromRow)]
pub struct GroupKey {
    pub value: Vec<u8>
}

pub async fn eph_insert(con: &Database, server_id: u32, keys: &[([u8; 32], Zeroizing<[u8; 32]>)]) -> Result<()> {
    let mut tx = con.con.begin().await?;

    for key in keys {
        sqlx::query(r#"insert into eph_keys (eph_pk, eph_sk, server) values (?1, ?2, ?3)"#)
            .bind(key.0.as_slice())
            .bind(key.1.as_slice())
            .bind(server_id)
            .execute(&mut tx)
            .await?;
    }

    tx.commit().await?;
    Ok(())
}

pub async fn eph_pop(con: &Database, server_id: u32, eph_pk: &[u8]) -> Result<Zeroizing<Vec<u8>>> {
    let ret = sqlx::query_as::<_, EphSk>(r#"delete from eph_keys where server = ?1 and eph_pk = ?2 returning eph_sk"#)
        .bind(server_id)
        .bind(eph_pk)
        .fetch_one(&con.con)
        .await?;

    Ok(Zeroizing::new(ret.eph_sk))
}

pub async fn set_group_key(con: &Database, k: &[u8], v: &[u8]) -> Result<()> {
    sqlx::query(r#"insert into group_key_store values (?1, ?2) on conflict(key) do update set key = excluded.key"#)
        .bind(k)
        .bind(v)
        .execute(&con.con)
        .await?;

    Ok(())
}

pub async fn get_group_key(con: &Database, k: &[u8]) -> Result<Vec<u8>> {
    let ret = sqlx::query_as::<_, GroupKey>(r#"select value from group_key_store where key = ?1"#)
        .bind(k)
        .fetch_one(&con.con)
        .await?;

    Ok(ret.value)
}

pub async fn delete_group_key(con: &Database, k: &[u8]) -> Result<()> {
    sqlx::query(r#"delete from group_key_store where key = ?1"#)
        .bind(k)
        .execute(&con.con)
        .await?;

    Ok(())
}
