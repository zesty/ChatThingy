pub mod profile;
pub mod server;
pub mod contact;
pub mod message;
pub mod key_store;
pub mod update;

use sqlx::{sqlite::{SqliteConnectOptions, SqlitePool, Sqlite}, Pool};
use std::{error::Error, str::FromStr};

#[derive(Debug, Clone)]
pub struct SessionDB {
    pub con: Pool<Sqlite>
}

impl SessionDB {
    pub async fn new(dir: &str) -> Result<SessionDB, Box<dyn Error + Send + Sync>> {
        let db = SessionDB { con: SessionDB::connect(dir).await? };
        db.create_if_not_exists().await?;
        Ok(db)
    }

    async fn connect(dir: &str) -> Result<Pool<Sqlite>, Box<dyn Error + Send + Sync>> {
        let opt = SqliteConnectOptions::from_str(&format!("sqlite://{}{}", dir, "/session.db"))?
            .create_if_missing(true);

        Ok(SqlitePool::connect_with(opt).await?)
    }

    pub async fn create_if_not_exists(&self) -> Result<(), Box<dyn Error + Send + Sync>> {
        sqlx::query("create table if not exists session (
                     name text primary key,
                     enc_keypair blob not null
                 )")
            .execute(&self.con)
            .await?;

        Ok(())
    }
}


#[derive(Debug, Clone)]
pub struct Database {
    pub con: Pool<Sqlite>
}

impl Database {
    #[cfg(debug_assertions)]
    pub async fn new(session_dir: &str) -> Result<Database, Box<dyn Error + Send + Sync>> {
        let opt = SqliteConnectOptions::from_str(&format!("sqlite://{}/storage.db", session_dir))?
            .create_if_missing(true);
 
        let db = Database { con: SqlitePool::connect_with(opt).await? };
        db.create_if_not_exists().await?;
        Ok(db)
    }

    #[cfg(not(debug_assertions))]
    pub async fn new(session_dir: &str, mut pass: String) -> Result<Database, Box<dyn Error + Send + Sync>> {
        // We always add a first character to letter so it can be accepted
        // as pragma value
        pass.insert(0, 'A');

        let opt = SqliteConnectOptions::from_str(&format!("sqlite://{}/storage.db", session_dir))?
            .pragma("key", pass)
            .pragma("cipher_page_size", "1024")
            .pragma("kdf_iter", "64000")
            .pragma("cipher_hmac_algorithm", "HMAC_SHA1")
            .pragma("cipher_kdf_algorithm", "PBKDF2_HMAC_SHA1")
            .journal_mode(sqlx::sqlite::SqliteJournalMode::Delete)
            .create_if_missing(true);

        let db = Database { con: SqlitePool::connect_with(opt).await? };
        db.create_if_not_exists().await?;
        Ok(db)
    }

    pub async fn create_if_not_exists(&self) -> Result<(), Box<dyn Error + Send + Sync>> {
        sqlx::query("create table if not exists profile_info (
                     id integer primary key,
                     name text not null,
                     avatar not null,
                     height integer not null
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists server (
                     id integer primary key,
                     server text not null,
                     last_id integer not null,
                     last_group_id integer not null,
                     update_height integer,
                     state blob
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists contact (
                     id integer primary key,
                     public_key blob not null,
                     server integer not null,
                     ratchet blob,
                     first_reached bool,
                     name text not null,
                     avatar blob,
                     height integer,
                     unique(public_key, server),
                     foreign key(server) references server(id) on delete cascade
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists message (
                     message_id integer primary key,
                     contact integer not null,
                     source bool,
                     content blob not null,
                     timestamp integer not null,
                     foreign key(contact) references contact(id) on delete cascade
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists group_message (
                     message_id integer primary key,
                     room_id integer not null,
                     server integer not null,
                     source int,
                     content blob not null,
                     timestamp integer not null,
                     foreign key(server) references server(id) on delete cascade
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists group_message_notification (
                     message_id integer primary key,
                     notif_source integer not null,
                     notif_dest integer not null,
                     foreign key(message_id) references group_message(message_id) on delete cascade
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists last_read (
                     contact integer primary key,
                     last_read integer not null,
                     foreign key(contact) references contact(id) on delete cascade
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists last_read_group (
                     server integer not null,
                     room_id integer not null,
                     last_read integer not null,
                     foreign key(server) references server(id) on delete cascade
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists pending_contact (
                     contact integer primary key,
                     add_time integer not null,
                     foreign key(contact) references contact(id) on delete cascade
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists eph_keys (
                     id integer primary key,
                     eph_pk blob not null,
                     eph_sk blob not null,
                     server integer not null,
                     foreign key(server) references server(id) on delete cascade
                )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists group_key_store (
                     key blob primary key,
                     value blob not null
                )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists pending_message (
                     id integer primary key,
                     content blob not null
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists pending_task (
                     id integer primary key,
                     content blob not null
                 )")
            .execute(&self.con)
            .await?;

        sqlx::query("create table if not exists profile_update_secret (
                     id integer primary key,
                     height integer not null,
                     secret blob,
                     nonce blob,
                     foreign key(id) references server(id) on delete cascade
                 )")
            .execute(&self.con)
            .await?;

        // When we add a new contact it is considered pending
        sqlx::query("drop trigger if exists new_pending_contact;
                     create trigger new_pending_contact after insert 
                     on contact
                     begin
                        insert into pending_contact (contact, add_time) values (NEW.id, strftime('%s', 'now'));
                     end;")
            .execute(&self.con)
            .await?;

        // When we send first message ever to contact, it is no longer pending
        sqlx::query("drop trigger if exists new_contact;
                     create trigger new_contact after insert 
                     on message
                     when 1 = (select 1 from pending_contact where pending_contact.contact = NEW.contact)
                     begin
                        delete from pending_contact where pending_contact.contact = NEW.contact;
                     end;")
            .execute(&self.con)
            .await?;

        Ok(())
    }
}
