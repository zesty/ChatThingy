use super::Database;
use anyhow::Result;
use async_stream::stream;
use futures::{Stream, TryStreamExt};
use borsh::BorshDeserialize;
use nero_client_api::ChannelType;
use tracing::error;


#[derive(sqlx::FromRow)]
#[allow(unused)]
struct InnerMessage {
    message_id: i64,
    content: Vec<u8>,
    source: Option<u32>,
    timestamp: i64,
}

#[derive(Debug)]
pub struct Message {
    pub message_id: i64,
    pub content: crate::message::Message,
    pub source: SenderType,
    pub timestamp: i64
}

#[derive(Debug, Clone, PartialEq)]
pub enum SenderType {
    Notification,
    Me,
    Contact,
    User { id: u32 }
}

#[derive(sqlx::FromRow)]
pub struct NewMessage {
    pub message_id: i64
}

#[derive(sqlx::FromRow)]
struct PendingMessage {
    pub id: u32,
    pub content: Vec<u8>
}

pub async fn add(con: &Database, user_id: u32, msg_timestamp: i64, source: Option<bool>, ser_msg: &[u8]) -> Result<i64> {
    Ok(sqlx::query_as::<_, NewMessage>(r#"insert into message (contact, source, content, timestamp) values (?1, ?2, ?3, ?4)
                                          returning message_id"#)
        .bind(user_id)
        .bind(source)
        .bind(ser_msg)
        .bind(msg_timestamp)
        .fetch_one(&con.con)
        .await?
        .message_id
    )
}

pub async fn add_group(con: &Database, id: u32, msg_timestamp: i64, source: Option<u32>, ser_msg: &[u8]) -> Result<i64> {
    Ok(sqlx::query_as::<_, NewMessage>(r#"insert into group_message (server, room_id, source, content, timestamp) values (?1, 0, ?2, ?3, ?4)
                                          returning message_id"#)
        .bind(id)
        .bind(source)
        .bind(ser_msg)
        .bind(msg_timestamp)
        .fetch_one(&con.con)
        .await?
        .message_id
    )
}

pub async fn add_group_notification(con: &Database, message_id: i64, source: u32, dest: Option<u32>) -> Result<i64> {
    Ok(sqlx::query_as::<_, NewMessage>(r#"insert into group_message_notification values (?1, ?2, ?3)"#)
        .bind(message_id)
        .bind(source)
        .bind(dest)
        .fetch_one(&con.con)
        .await?
        .message_id
    )
}

pub async fn get_last_messages<'a>(con: &'a Database, id: u32, channel_type: ChannelType, ascending: bool,
                                   first_item: i64, page_item_number: usize, grouping_time: i64) -> impl Stream<Item = Result<Message>> + 'a + Unpin {
    let query = match channel_type {
        ChannelType::Direct => if ascending {
            r#"select message_id, content, source, timestamp
                from message where contact = ?1 and message_id < ?2
                order by message_id desc;"#
        } else {
            r#"select message_id, content, source, timestamp
                from message where contact = ?1 and message_id > ?2
                order by message_id asc;"#
        },
        ChannelType::Group => if ascending {
            r#"select message_id, content, source, timestamp
                from group_message where server = ?1 and room_id = 0 and message_id < ?2
                order by message_id desc;"#
        } else {
            r#"select message_id, content, source, timestamp
                from group_message where server = ?1 and room_id = 0 and message_id > ?2
                order by message_id asc;"#

        }
    };

    let mut rez = sqlx::query_as::<_, InnerMessage>(query)
        .bind(id)
        .bind(first_item)
        .fetch(&con.con);

    Box::pin(stream! {
        let mut counter  = 0;
        let mut previous = None;
        loop {
            let next = rez.try_next().await?;
            match next {
                Some(data) => {
                    if counter == page_item_number && data.source.is_some() {
                        let sid = data.source.unwrap();
                        previous = Some((if sid == 0 { SenderType::Me } else if channel_type == ChannelType::Group { SenderType::User { id: sid } } else { SenderType::Contact }, data.timestamp));
                    } else if let Some(prev) = previous {
                        if data.source.is_none() {
                            break;
                        }
                        let sid = data.source.unwrap();
                        let current_source = if sid == 0 { SenderType::Me } else if channel_type == ChannelType::Group { SenderType::User { id: sid } } else { SenderType::Contact };
                        if current_source != prev.0 || data.timestamp-prev.1 > grouping_time || prev.1-data.timestamp > grouping_time {
                            break;
                        }
                        previous = Some((current_source, data.timestamp));
                    }

                    yield Ok(Message {
                        message_id: data.message_id,
                        content: match crate::message::Message::try_from_slice(&data.content) {
                            Ok(v) => v,
                            Err(e) => {
                                error!("Error serializing message with id {}: {}", data.message_id, e);
                                crate::message::Message::Unreadable
                            }
                        },
                        source: match data.source {
                            Some(v) => if v == 0 { SenderType::Me } else if channel_type == ChannelType::Group { SenderType::User { id: v } } else { SenderType::Contact },
                            None => SenderType::Notification
                        },
                        timestamp: data.timestamp
                    })
                },
                None => {
                    break;
                }
            }
            counter += 1;
        }
    })
}

pub async fn update_last_read(con: &Database, id: u32, channel_type: ChannelType, last_read: i64) -> Result<()> {
    let query = match channel_type {
        ChannelType::Group => r#"insert into last_read_group (server, room_id, last_read) values (?1, 0, ?2) on conflict do update set last_read = max(last_read, ?2)"#,
        ChannelType::Direct => r#"insert into last_read (contact, last_read) values (?1, ?2) on conflict do update set contact = ?1, last_read = max(last_read, ?2)"#
    };

    if sqlx::query(query)
        .bind(id)
        .bind(last_read)
        .execute(&con.con)
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("Error updating last read message in database"));
    }
    Ok(())
}

pub async fn add_pending_message(con: &Database, msg: &[u8]) -> Result<()> {
    sqlx::query(r#"insert into pending_message (content) values (?1)"#)
        .bind(msg)
        .execute(&con.con)
        .await?;

    Ok(())
}

pub async fn get_pending_message(con: &Database) -> Result<(u32, Vec<u8>)> {
    let ret = sqlx::query_as::<_, PendingMessage>(r#"select id, content from pending_message limit 1"#)
        .fetch_one(&con.con)
        .await?;

    Ok((ret.id, ret.content))
}

pub async fn delete_pending_message(con: &Database, id: u32) -> Result<()> {
    sqlx::query(r#"delete from pending_message where id = ?1"#)
        .bind(id)
        .execute(&con.con)
        .await?;

    Ok(())
}
