use common::keys::PublicKey;

use super::Database;
use anyhow::Result;
use borsh::BorshDeserialize;
use async_stream::stream;
use futures::{Stream, TryStreamExt};

#[derive(sqlx::FromRow, Debug)]
pub struct NewContact {
    pub id: u32
}

#[derive(sqlx::FromRow, Debug)]
pub struct NewPendingContact {
    pub id: u32,
    pub name: String,
    pub avatar: Option<Vec<u8>>
}

#[derive(sqlx::FromRow, Debug)]
pub struct Contact {
    pub id: u32,
    pub name: String,
    pub avatar: Option<Vec<u8>>,
    pub not_read: u32,
    pub pending: Option<i64>,
    pub first_reached: bool,
    pub last_timestamp: Option<i64>
}

#[derive(sqlx::FromRow, Debug)]
pub struct ContactInfo {
    pub id: u32,
    pub name: String,
    pub avatar: Option<Vec<u8>>
}

#[derive(sqlx::FromRow, Debug)]
pub struct ContactFind {
    pub id: u32
}

#[derive(sqlx::FromRow, Debug)]
pub struct IsDirectContact {
    pub id: u32,
    pub is_direct_contact: bool
}

#[derive(sqlx::FromRow)]
struct Ratchet {
    ratchet: Vec<u8>
}

#[derive(sqlx::FromRow)]
struct InnerPublicKey {
    public_key: Vec<u8>,
    server: u32
}

#[derive(sqlx::FromRow)]
struct Server {
    server: String
}

#[derive(sqlx::FromRow)]
struct ContactInfoHeight {
    height: u32
}

#[derive(sqlx::FromRow)]
struct UserCount {
    count: u32
}

pub async fn get_all(con: &Database, server_id: u32) -> Result<Vec<Contact>> {
    // Get all contacts ordered by last one we receive message from
    let rez = sqlx::query_as::<_, Contact>(r#"select id, name, avatar, first_reached, message.timestamp as "last_timestamp",
                                            (select add_time from pending_contact where pending_contact.contact = contact.id) as "pending",
                                            (select count(*) from message where message.contact = contact.id and (message.source is null or message.source = true)
                                                and message.message_id > ifnull((select last_read from last_read where last_read.contact = contact.id), 0)
                                            ) as "not_read"
                                            from contact
                                            left join message on contact.id = message.contact
											where server = ?1 and ratchet is not null
                                            group by contact.id
                                            order by max(ifnull(message.timestamp, pending)) desc;"#)
        .bind(server_id)
        .fetch_all(&con.con)
        .await?;
    Ok(rez)
}

pub async fn get_contact_info_for_messages<'a>(con: &'a Database, query: &'a mut String, server_id: u32, ascending: bool,
                                           first_item: i64, page_item_number: u32, loaded_contact: &[u32]) -> impl Stream<Item = Result<ContactInfo>> + 'a + Unpin {
    let list: String = loaded_contact.iter().map( |&id| id.to_string() + ",").collect();
    *query = format!(r#"select distinct id, name, avatar from contact, group_message
        left join group_message_notification on group_message.message_id = group_message_notification.message_id
        where group_message.server = ?1
        and (contact.id = source or contact.id = notif_source or contact.id = notif_dest) and group_message.message_id {} ?2
        and contact.id not in ({}) order by group_message.message_id {} limit ?3;"#,
        if ascending { "<" } else { ">" },
        &list[..list.len()-1],
        if ascending { "desc" } else { "asc" });

    let mut rez = sqlx::query_as::<_, ContactInfo>(query)
        .bind(server_id)
        .bind(first_item)
        .bind(page_item_number)
        .fetch(&con.con);

    Box::pin(stream! {
        loop {
            let next = rez.try_next().await?;
            match next {
                Some(v) => yield Ok(v),
                None => break
            }
        };
    })
}

pub async fn exists(con: &Database, pubkey: &[u8]) -> Result<bool> {
    let rez = sqlx::query(r#"select 1 from contact where public_key = ?1"#)
        .bind(pubkey)
        .fetch_all(&con.con)
        .await?;
    if !rez.is_empty() {
        return Ok(true);
    }
    Ok(false)
}

pub async fn add(con: &Database, name: &str, pk: &[u8], server: u32) -> Result<u32> {
    Ok(sqlx::query_as::<_, NewContact>(r#"insert into contact (name, public_key, server)
                                          values (?1, ?2, ?3) on conflict do update set name = excluded.name
                                          returning id"#)
        .bind(name)
        .bind(pk)
        .bind(server)
        .fetch_one(&con.con)
        .await?
        .id
    )
}

pub async fn add_contact_list(con: &Database, first_reached: bool, pk: &[u8], server: u32) -> Result<NewPendingContact> {
    Ok(sqlx::query_as::<_, NewPendingContact>(r#"update contact set first_reached = ?1 where public_key = ?2 and server = ?3 returning id, name, avatar"#)
        .bind(first_reached)
        .bind(pk)
        .bind(server)
        .fetch_one(&con.con)
        .await?)
}

pub async fn update(con: &Database, id: u32, name: &str) -> Result<()> {
    if sqlx::query(r#"update contact set name = ?1 where id = ?2"#)
        .bind(name)
        .bind(id)
        .execute(&con.con)
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("Error updating contact in database"));
    }
    Ok(())
}

pub async fn remove(con: &Database, ids: &[u32]) -> Result<()> {
    let mut tx = con.con.begin().await?;

    for id in ids {
        sqlx::query(r#"delete from contact where id = ?1"#)
            .bind(id)
            .execute(&mut tx)
            .await?;
    }

    tx.commit().await?;
    Ok(())
}


pub async fn set_update_info(con: &Database, pk: &[u8], server_id: u32, height: u32, name: &str, avatar: &Option<Vec<u8>>) -> Result<IsDirectContact> {
    let ret = sqlx::query_as::<_, IsDirectContact>(r#"update contact set height = ?1, name = ?2, avatar = ?3 where public_key = ?4 and server = ?5 returning id, (ratchet is not null) as "is_direct_contact""#)
        .bind(height)
        .bind(name)
        .bind(avatar)
        .bind(pk)
        .bind(server_id)
        .fetch_one(&con.con)
        .await?;
    Ok(ret)
}

pub async fn get_contact_update_height(con: &Database, pk: &[u8], server_id: u32) -> Result<u32> {
    let rez = sqlx::query_as::<_, ContactInfoHeight>(r#"select height from contact where public_key = ?1 and server = ?2"#)
        .bind(pk)
        .bind(server_id)
        .fetch_one(&con.con)
        .await?;

    Ok(rez.height)
}

pub async fn get_contact_from_pk(con: &Database, pk: &[u8], server_id: u32) -> Result<u32> {
    let rez = sqlx::query_as::<_, ContactFind>(r#"select id from contact where public_key = ?1 and server = ?2"#)
        .bind(pk)
        .bind(server_id)
        .fetch_one(&con.con)
        .await?;

    Ok(rez.id)
}

pub async fn get_contact_pk_server(con: &Database, user_id: u32) -> Result<(PublicKey, u32)> {
    let rez = sqlx::query_as::<_, InnerPublicKey>(r#"select public_key, server from contact where contact.id = ?1"#)
        .bind(user_id)
        .fetch_one(&con.con)
        .await?;

    Ok((PublicKey::try_from_slice(&rez.public_key)?, rez.server))
}

pub async fn get_contact_ratchet(con: &Database, server_id: u32, pk: &[u8]) -> Result<Vec<u8>> {
    let rez = sqlx::query_as::<_, Ratchet>(r#"select ratchet from contact where public_key = ?1 and server = ?2 and ratchet not null"#)
        .bind(pk)
        .bind(server_id)
        .fetch_one(&con.con)
        .await?;
    Ok(rez.ratchet)
}

pub async fn update_contact_ratchet(con: &Database, server_id: u32, pk: &[u8], ratchet: &[u8]) -> Result<()> {
    if sqlx::query(r#"update contact set ratchet = ?1 where server = ?2 and public_key = ?3"#)
        .bind(ratchet)
        .bind(server_id)
        .bind(pk)
        .execute(&con.con)
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("Error updating ephemeral peer public keys in database"));
    }
    Ok(())
}

pub async fn get_contact_server(con: &Database, user_id: u32) -> Result<String> {
    let rez = sqlx::query_as::<_, Server>(r#"select server from server where id = (select server from contact where id = ?1)"#)
        .bind(user_id)
        .fetch_one(&con.con)
        .await?;

    Ok(rez.server)
}

pub async fn users_using_server_count(con: &Database, server: &str) -> Result<u32> {
    let rez = sqlx::query_as::<_, UserCount>(r#"select count(*) as "count" from contact where server = (select id from server where server = ?1)"#)
        .bind(server)
        .fetch_one(&con.con)
        .await?;

    Ok(rez.count)
}
