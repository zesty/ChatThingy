use super::Database;
use anyhow::Result;
use nero_client_api::ProfileUpdate;

#[derive(sqlx::FromRow)]
pub struct ProfileUpdateInfo {
    pub name: String,
    pub avatar: Vec<u8>,
    pub height: u32
}

#[derive(sqlx::FromRow)]
struct ProfileSecret {
    pub height: u32,
    pub nonce: Vec<u8>,
    pub secret: Vec<u8>
}

#[derive(sqlx::FromRow)]
struct PendingTask {
    pub id: u32,
    pub content: Vec<u8>
}


pub async fn get_profile_info(con: &Database) -> Result<ProfileUpdateInfo> {
    let ret = sqlx::query_as::<_, ProfileUpdateInfo>(r#"select name, avatar, height from profile_info"#)
        .fetch_one(&con.con)
        .await?;

    Ok(ret)
}

pub async fn set_profile_info(con: &Database, name: &str, avatar: &[u8]) -> Result<()> {
    sqlx::query(r#"insert into profile_info (id, name, avatar, height) values (0, ?1, ?2, 0) on conflict(id) do update set name = excluded.name, avatar = excluded.avatar, height = height+1"#)
        .bind(name)
        .bind(avatar)
        .execute(&con.con)
        .await?;

    Ok(())
}

pub async fn update_profile_update_secret(con: &Database, server_id: u32, info: &ProfileUpdate) -> Result<()> {
    if sqlx::query(r#"insert into profile_update_secret values (?1, ?2, ?3, ?4) on conflict(id) do update set height = excluded.height, nonce = excluded.nonce, secret = excluded.secret"#)
        .bind(server_id)
        .bind(info.height)
        .bind(&info.secret)
        .bind(&info.nonce)
        .execute(&con.con)
        .await?
        .rows_affected() == 0 {
        return Err(anyhow::Error::msg("Error updating profile update secrets"));
    }
    Ok(())
}

pub async fn get_profile_update_secret(con: &Database, server_id: u32) -> Result<Option<ProfileUpdate>> {
    let ret = sqlx::query_as::<_, ProfileSecret>(r#"select height, nonce, secret from profile_update_secret where id = ?1"#)
        .bind(server_id)
        .fetch_one(&con.con)
        .await;

    match ret {
        Ok(v) => Ok(Some(ProfileUpdate { height: v.height, secret: v.secret, nonce: v.nonce })),
        Err(sqlx::Error::RowNotFound) => Ok(None),
        Err(e) => Err(anyhow::Error::new(e))
    }
}

pub async fn add_pending(con: &Database, msg: &[u8]) -> Result<()> {
    sqlx::query(r#"insert into pending_task (content) values (?1)"#)
        .bind(msg)
        .execute(&con.con)
        .await?;

    Ok(())
}

pub async fn get_pending(con: &Database) -> Result<(u32, Vec<u8>)> {
    let ret = sqlx::query_as::<_, PendingTask>(r#"select id, content from pending_task limit 1"#)
        .fetch_one(&con.con)
        .await?;

    Ok((ret.id, ret.content))
}

pub async fn delete_pending(con: &Database, id: u32) -> Result<()> {
    sqlx::query(r#"delete from pending_task where id = ?1"#)
        .bind(id)
        .execute(&con.con)
        .await?;

    Ok(())
}
