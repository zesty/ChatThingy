
fn main() {
    let args: Vec<_> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Run: nero-server-key-gen <network-type>");
        return;
    }

    match args[1].as_str() {
        "tor" => {
            let key = torut::onion::TorSecretKeyV3::generate();
            println!("Key: {}",
                     serde_json::to_string(&key)
                     .expect("Couldn't serialize key to string"));
            println!("Domain: \"{}\"", key.public().get_onion_address());
        },
        "lokinet" => {
            // TODO
        },
        _ => {}
    }
}
